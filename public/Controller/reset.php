<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\reset;

$reset = new reset();

$argsGet = [
	'token' => FILTER_SANITIZE_STRING,
];
$argsPost = [
	'newPassword' => FILTER_SANITIZE_STRING,
	'confirmNewPassword' => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $argsPost, false);
$GET = filter_input_array(INPUT_GET, $argsGet, false);

//we check token
if(isset($GET['token'])){
	try{
		//we search if token is valid for a user and check time
		$user = $userDAO->selectUserByToken($GET['token']);
		if($user != null){
			$request = strtotime ($user->getRequest());
	        $timeRequest = (mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y"))- $request);
	        if($timeRequest > 900){
	        	$user->setOpen(0);
	        	$userDAO->updateUser($user);
	            header("Location: index.php?page=error&code=504") ;
	        }
			if($user == null){
				header('Location: index.php?page=error&code=403');
			}
		}else{
			$ERROR = [
				'message' => 'Invalid Token'
			];
		}
	}catch(PDOException $e){
        log_error($e, $logger, 'reset', $session->getUserdata(), $backLink);
        die();
	}
}else{
	header('Location: index.php?page=error&code=403');
}

if(isset($POST['newPassword'])){
	$check = strcmp ($POST['newPassword'], $POST['confirmNewPassword']);
	if($check != 0){
		$ERROR = [
			'message' => $LANG['password_not_equal']
		];
	}else{
		$password = password_hash($POST['newPassword'], PASSWORD_DEFAULT);
		$user->setPassword($password);
        $user->setRequest(null);
        $user->setToken(null);
        $user->setOpen(0);
        try {
            $userDAO->updateUser($user);
        } catch (PDDOException $e) {
            log_error($e, $logger, 'reset', $session->getUserdata(), $backLink);
            die();
        }
        header('Location: index.php');
	}
}

$base = new base();

$base->base_header();

$base->open_body($backLink);

$base->table('center', $from);

if(isset($ERROR)){
	$base->displayError($ERROR);
}

if($user != null){
	$reset->resetForm($user);
}

$base->close_body();

$base->base_footer();
