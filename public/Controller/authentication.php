<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/03/2019
 * Time: 16:17
 */

use App\HTML\authentification;
use App\HTML\base;
use App\HTML\login;

$args = [
    'tokenSession' => FILTER_SANITIZE_STRING,
];

$argsGET = [
    'idSession' => FILTER_VALIDATE_INT,
    'error' => FILTER_VALIDATE_INT,
    'redirect' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);
$GET = filter_input_array(INPUT_GET, $argsGET, false);

$base = new base();
$authHTML = new authentification();
$login = new login();

$base->base_header(['authentication_session']);

$base->openFlexBody($backLink,'middle');
$base->table("flex", $from);

//if error into get / error login Moodle
if(isset($GET['error'])){
    $ERROR = [
        'message' => 'Error: Login / Password'
    ];
}


try {
    if (isset($POST['tokenSession']) && $POST['tokenSession'] != null) {
        //check in session table for public token
        $arraysult = $sessionDAO->selectSessionsByFilter("token", $POST['tokenSession'], 1);

        if (empty($arraysult)) {
            //check in acces keys table for private token
            $sessionQamphiId = $accessKeysDAO->selectSessionByToken($POST['tokenSession']);
            $sessionQamphi = $sessionDAO->selectSessionsByFilter("id", $sessionQamphiId, 1)[0];
        } else {
            $sessionQamphi = $arraysult[0];
        }

    } else if (isset($GET['idSession'])) {
        $sessionQamphi = $sessionDAO->selectSessionsByFilter("id", $GET['idSession'], 1);
        if ($sessionQamphi != null) {
            $sessionQamphi = $sessionQamphi[0];
        } else {
            header("Location: ?page=sessions&error=nosession");
        }
        if ($sessionQamphi->getAccess() == 'private') {
            header("Location: ?page=error&code=403");
        }
    } else if ($sessionQamphi == null) {
        header("Location: ?page=sessions&error=nosession");
    } else {
        header("Location: ?page=error&code=402");
    }
} catch (PDOException $e) {
    log_error($e, $logger, 'authentication', $session->getUserdata(), $backLink);
    die();
}

if ($sessionQamphi != null) {
    if (!isset($GET['redirect'])) {
        //persist user participation
        $statsDAO->notify('session_join');
        $sessionDAO->incrementParticipants($sessionQamphi->getId());
        //delete access key if private session
        if ($sessionQamphi->getAccess() === 'private') {
            $accessKeysDAO->deleteByToken($POST['tokenSession']);
        }

        if ($sessionQamphi->getAuth() == 'anon') {
            header('Location: ?page=participate&idSession=' . $sessionQamphi->getId() . '&anon&redirect');
            exit;
        }
        //redirect with GET to prevent user to increment participation by refreshing page
        header("Location: ?page=authentication&idSession=" . $sessionQamphi->getId()."&redirect");
    }
} else {
    header("Location: ?page=sessions&error=nosession");
}

//display errors
if(isset($ERROR)){
    $base->displayError($ERROR);
}

try {//check session status
    if ($auth->isAnon() || $session->getStatus() == "IDLE") {
        $username = 'ANON';
        if (!isset($POST['token'])) {
            $token = $session->generateToken('login_admin');
        } else {
            $token = $POST['token'];
        }
    } else {
        $token = $session->generateToken('login_admin');
        $username = $session->getValue('username');
    }
} catch (PDOException $e) {
    log_error($e, $logger, 'authentication', $session->getUserdata(), $backLink);
    die();
}
$authHTML->form_auth($username, $sessionQamphi, $moodle);

//check if moodle is activate / display or not form
if($moodle == "on"){
    echo $login->loginMoodleForm($token, $sessionQamphi->getId());
}


$base->table("end");
$base->close_body();

$base->base_footer();
