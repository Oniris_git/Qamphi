<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\about;

$base = new base();
$about = new about();

$base->base_header();

$base->open_body($backLink);

$base->table("without-btn");

$about->aboutTitle($from, $config->getVersion());

$about->contentAbout();

$about->licenceAbout();

$base->table("end");

$base->close_body();

$base->base_footer();
