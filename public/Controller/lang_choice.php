<?php

use App\HTML\base;
use App\HTML\lang_choice;

$args = [
    'from' => FILTER_SANITIZE_STRING,
    'language' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

if (isset($POST)) {
    try {
        $session->setUserPreference('lang', $POST['language']);
    } catch (PDOException $e) {
        log_error($e, $logger, 'confirm', $session->getUserdata(), $backLink);
        die();
    }
    header('Location: '.$POST['from']);
}

$base = new base();
$langChoice = new lang_choice();

$base->base_header();
$base->open_body($backLink, 'large');
$base->table("without-btn");
$langChoice->langMenu($allLangs, $from);

$base->table("end");
$base->close_body();
$base->base_footer();