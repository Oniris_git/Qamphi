<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\main;

$base = new base();
$main = new main();

if(isset($session) && $auth->isAnon() != true && $session->getStatus() != 'IDLE'){
    $role = $session->getValue('role');
}else{
    $role = 'ANON';
}

$base->base_header(['main_index']);

$main->menu($backLink);

$base->footer($role);

$base->base_footer();
