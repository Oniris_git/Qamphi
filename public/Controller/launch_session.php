<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/03/2019
 * Time: 16:50
 */

use App\HTML\base;
use App\HTML\launch_session;
use App\HTML\main;

include "ControllerFunctions/launch_sessionFunctions.php";

//HTML objects instantiation
$base = new base();
$launch = new launch_session();
$main = new main();

if(isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE' ||  $session->getValue('role') != 'Admin' && $session->getValue('role') != 'Teacher'){
    header("Location: ?page=error&code=403");
}

$args = [
    'order' => FILTER_VALIDATE_INT,
    'idSession' => FILTER_VALIDATE_INT,
    'request' => FILTER_SANITIZE_STRING,
    'sessionStatId' => FILTER_VALIDATE_INT,
];

$POST = filter_input_array(INPUT_POST, $args, false);

if(isset($POST['idSession']) ){
    try {
        $actualSession = $sessionDAO->selectSessionsByFilter('id', $POST['idSession'])[0];
        $listQuestions = $questionDAO->selectQuestionBySession($POST['idSession']);
    } catch (PDOException $e) {
        log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
        die();
    }
    if($listQuestions == null || $listQuestions[0]->getContent() == ''){
        $ERROR = [
            'message' => $LANG['no_questions']
        ];
    }else{
        try {
            checkList($listQuestions, $questionDAO, $proposalDAO);
        } catch (PDOException $e) {
            log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
            die();
        }
        $countQuestion = count($listQuestions);
    }

}else{
    header("Location: index.php?page=main");
}

//get question and its proposals
if(isset($POST['order'])){
    try{
        if($POST['order']){
            $sessionDAO->updateByField($POST['idSession'], "active", 1);
        }
        if($listQuestions == null || $listQuestions[0]->getContent() == ""){
            $ERROR = [
                'message' => $LANG['no_questions']
            ];
        }else{
            $order = ($POST['order']) - 1;
            $actualSession->setCurrent($order+1);
            $sessionDAO->updateByField($actualSession->getId(),'current',$order+1);
            $questionToDisplay = $listQuestions[$order];
            $listProposition = $proposalDAO->selectProposalNotNullByQuestion($questionToDisplay->getId());
            $questionsList = [$questionToDisplay];
        }

    }catch (PDOException $e){
        log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
        die();
    }
}

if (isset($_POST['progression'])) {
    try {
        echo json_encode([
            'responses' => $responseDAO->countResponsesByQuestion($questionToDisplay->getId()),
            'total' => $actualSession->getParticipants()
        ]);
    } catch (PDOException $e) {
        log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
        die();
    }
    die();
}

if (isset($POST['sessionStatId'])) {
    try{
        $questionsList = $questionDAO->selectQuestionBySession($POST['sessionStatId']);
    }catch (PDOException $e){
        log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
        die();
    }
}

$base->base_header(['launch_session']);

if(isset($POST['request']) && $POST['request']== 'stats'){
    $base->open_body($backLink);
}else{
    $base->openFlexBody($backLink, "large", '20');
}

//In case stats / to display number of responses
if(isset($POST['request']) && $listQuestions != null){
    try {
        switch ($POST['request']):
            case "response":
                $actualSession->setCurrent(-$actualSession->getCurrent());
                $sessionDAO->updateByField($actualSession->getId(), 'current', $actualSession->getCurrent());
                if (isset($POST['sessionStatId'])) {
                    $LANG['question_number'] = '';
                }
                foreach ($questionsList as $questionToDisplay) {
                    $questionType = get_class($questionToDisplay);
                    $listResponse = [];
                    $totalCount = 0;
                    $listResponse = $responseDAO->selectResponseByQuestion($questionToDisplay->getId());

                    if ($questionType::needsProposals()) {
                        $listProposition = $proposalDAO->selectProposalNotNullByQuestion($questionToDisplay->getId());
                        $questionToDisplay->setProposalsFromData($listResponse, $listProposition);
                    }
                    $base->table("flex", $from);
                    if (isset($ERROR)) {
                        $base->displayError($ERROR);
                    }
                    $session = $sessionDAO->selectSessionsByFilter("id", $questionToDisplay->getSessionId())[0];
                    $countResponse = sizeof($listResponse);
                    $questionToDisplay->setParticipants($countResponse);
                    $questionToDisplay->displayDetailedStats($session->getParticipants(), $listResponse);

                    $page = "stats";
                    if (!isset($POST['sessionStatId'])) {
                        $launch->navigationReview($questionToDisplay, $countQuestion, $page);
                    }
                }
                if ($actualSession->getCurrent() < 0 && abs($actualSession->getCurrent()) == (int)$questionDAO->countQuestionBySession($actualSession->getId())) {
                    $sessionDAO->updateByField($actualSession->getId(), "active", 0);
                }
                break;
            case "stats":
                $from = 'index.php?page=list_session';
                $actualSession->setCurrent(0);
                $sessionDAO->updateByField($actualSession->getId(), 'current', 0);
                checkList($listQuestions, $questionDAO, $proposalDAO);
                if ($listQuestions == null || $listQuestions[0]->getContent() == '') {
                    $ERROR = [
                        'message' => $LANG['no_questions']
                    ];
                    $main->returnBtn($from);
                    if (isset($ERROR)) {
                        $base->displayError($ERROR);
                    }
                    break;
                } else {
                    $base->table("start-without-btn", $from);
                    $count = count($questionDAO->selectQuestionBySession($POST['idSession']));
                    $main->returnBtn($from, $POST['idSession'], $count);
                    if ($actualSession->getParticipants() == 0) {
                        $ERROR = [
                            'message' => "Aucune participation pour cette session"
                        ];
                    }
                    if (isset($ERROR)) {
                        $base->displayError($ERROR);
                    }
                    if ($actualSession->getParticipants() != 0) {
                        $launch->statSwitch($actualSession->getId());
                    }
                    $launch->openstats();
                    foreach ($listQuestions as $question) {
                        if (get_class($question)::needsProposals()) {
                            $tableau = statsInfo($sessionDAO, $proposalDAO, $responseDAO, $listQuestions);
                            $listProposition = $proposalDAO->selectProposalByQuestion($question->getId());
                            $question->setProposals($listProposition);
                        } else {
                            $tableau['listUsers'] = [];
                        }
                        $listResponse = $responseDAO->selectResponseByQuestion($question->getId());
                        $question->statsFromData($actualSession, $listResponse);
                        if ($actualSession->getParticipants() != 0) {
                            $question->displayStats($listResponse);
                        }
                    }
                    $launch->closestats();
                }

                $launch->statPage($tableau['listUsers'], $from, $actualSession);

                //we disactive session to not disturb view with too much sessions
                $sessionDAO->updateByField($actualSession->getId(), "active", 0);
                break;
        endswitch;
    } catch (PDOException $e) {
        log_error($e, $logger, 'launch_session', $session->getUserdata(), $backLink);
        die();
    }
}else if($listQuestions == null || $listQuestions[0]->getContent() == ""){
    $base->table("flex", $from);
    if(isset($ERROR)){
        if($ERROR['message'] == $LANG['no_questions']){
           $main->returnBtn($from);
        }
        $base->displayError($ERROR);
    }
}else{
    //if request does not exist it is the first time on page
    if($questionToDisplay->getOrder() == '1'){
        $base->table("flex", $from);
    }else{
        $base->table("flex-btn", $from);
    }
    if(isset($ERROR)){
        $base->displayError($ERROR);
    }
    if (get_class($questionToDisplay)::needsProposals()) {
        $questionToDisplay->setProposals($listProposition);
    }
    $questionToDisplay->display();
}

$launch->session_id($actualSession->getId());
$base->table("end");
$base->close_body();
$base->base_footer(['charts', 'launch_session']);
