<?php

use App\DAL\QuestionDAO;
use App\HTML\base;
use App\HTML\confirm;

global $LANG;

$base = new base();
$confirm = new confirm();

$base->base_header(['confirm', 'participate']);
$base->open_form_container();
if(isset($_GET['idSession'])){
    $idSession = $_GET['idSession'];
} else {
    $idSession = $_POST['idSession'];
}

try {
    $listQuestion = $questionDAO->selectQuestionBySession($idSession);
    $countQuestion = count($listQuestion);
    $session = $sessionDAO->selectSessionsByFilter('id', $idSession)[0];
    $waiting = isset($_GET['waiting']) ? true : false;
} catch (PDOException $e) {
    log_error($e, $logger, 'confirm', $session->getUserdata(), $backLink);
    die();
}
if (isset($_GET['waiting'])) {
    $confirm->display($LANG['wait']);
//if user post response to late
}elseif(isset($_GET['late'])) {
    $confirm->display($LANG['late']);
//if user post several responses for an unique choice question by dev tools
} elseif (isset($_GET['tooMuch'])) {
    $confirm->display('Trop de réponses pour cette question');
} elseif (isset($_GET['noResponse'])) {
    $confirm->display($LANG['no_response']);
} else {

    try {//get user responses to question he just answered
        $questionAnswered = $questionDAO->selectQuestionByOrder($idSession, $session->getCurrent());
        $recap = $responseDAO->selectResponseByUser($_GET['username'], $questionAnswered->getId());
    } catch (PDOException $e) {
        log_error($e, $logger, 'confirm', $session->getUserdata(), $backLink);
        die();
    }

    $questionAnswered->displaySummary($recap);


}
//next or end button
if(abs($session->getCurrent())+1 <= $countQuestion){
    $confirm->nextQuestion($waiting);
}else{
    $confirm->endSession();
}

$base->close_form_container();
$base->base_footer();

