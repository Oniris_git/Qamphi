<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\auth;
use App\HTML\base;
use App\HTML\list_session;

include "ControllerFunctions/list_sessionFunctions.php";


if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE' || isset($session) && $session->getValue('role') != 'Admin' && $session->getValue('role') != 'Teacher') {
    header("Location: ?page=error&code=403");
}
$base = new base();
$list = new list_session();

//instantiation of variables to loop
$i = 0;
$f = 0;
$data = [];

$argsGet = [
    'spage' => FILTER_VALIDATE_INT,
    'shared' => FILTER_SANITIZE_STRING,
    'sort' => FILTER_SANITIZE_STRING,
    'order' => FILTER_SANITIZE_STRING,
];
$GET = filter_input_array(INPUT_GET, $argsGet, false);

$args = [
    'action' => FILTER_SANITIZE_STRING,
    'idSwitchOff' => FILTER_SANITIZE_STRING,
    'idSwitchOn' => FILTER_SANITIZE_STRING,
    'deleteResponse' => FILTER_SANITIZE_STRING,
    'deleteSession' => FILTER_SANITIZE_STRING,
    "byPage" => FILTER_VALIDATE_INT,
    "public" => FILTER_SANITIZE_STRING,
    "private" => FILTER_SANITIZE_STRING,
    "mine" => FILTER_SANITIZE_STRING,
    "shared" => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $args, false);

if (isset($POST['action'])) {
    action($POST, $sessionDAO, $responseDAO, $session);
}

$base->base_header(['list_session']);

$base->openFlexBody($backLink, "XL", '15');
$base->table("flex", $from);
$list->editTitle($from);


//catch sessions of user in session
try {
    // page number in pagination
    $current_page = $GET['spage'] ?? 1;

    // sort preferences
    $sort_pref = array_key_exists('sort', $user_prefs) ? $user_prefs['sort'] : ['field' => 'id', 'order' => 'asc'];

    $sort = [
        'field' => $GET['sort'] ?? $sort_pref['field'],
        'order' => $GET['order'] ?? $sort_pref['order'],
    ];

    $session->setUserPreference('sort', $sort);


    // filters preferences
    $filters = array_key_exists('filters', $user_prefs) ? $user_prefs['filters'] : [
        'visibility' => ['public', 'private'],
        'owner' => ['mine', 'shared'],
        'by_page' => 5,
    ];


    $session->setUserPreference('filters', $filters);

    // count and select sessions
    $sessions_count = $sessionDAO->countSessionsByParameters($session->getValue('id'), $filters);
    $sessionByUser = $sessionDAO->selectSessionsByPage($session->getValue('id'), $current_page, $sort, $filters);


    $total_pages = ceil($sessions_count / $filters['by_page']);

    $authorisations = [];
    foreach ($sessionByUser as $sessionU) {

        $authorisations[$sessionU->getId()]['owner'] = $userDAO->selectUserById($sessionU->getUserId())->getUsername();

        $authorisations[$sessionU->getId()]['owner_id'] = $sessionU->getUserId();
        if ($sessionU->getUserId() === $session->getValue('id')) {
            $authorisations[$sessionU->getId()]['rights'] = 'write';
        } else {
            $authorisations[$sessionU->getId()]['rights'] = $authorisationsDAO->selectRights($sessionU->getId(), $session->getValue('id'));
        }

        if ($sessionU->getTitle() == "") {

            $sessionDAO->updateSession($sessionU->getId(), $LANG['empty_title']);

            header("Location: ?page=list_session");
            exit;
        }

        $data[$sessionU->getId()] = $questionDAO->countQuestionBySession($sessionU->getId());

        $i++;
    }
} catch (PDOException $e) {
    log_error($e, $logger, 'list_session', $session->getUserdata(), $backLink);
    die();
}
// display options handling
if (isset($POST['byPage'])) {

    // sessions visibility preferences
    $visibility = [];

    if (isset($POST['public'])) {
        $visibility[] = 'public';
    }
    if (isset($POST['private'])) {
        $visibility[] = 'private';
    }

    // sessions owner preferences
    $owner = [];

    if (isset($POST['mine'])) {
        $owner[] = 'mine';
    }
    if (isset($POST['shared'])) {
        $owner[] = 'shared';
    }

    // sessions by page preferences
    $by_page = $user_prefs['filters']['by_page'];
    if ($POST['byPage'] > 0) {
        $by_page = $POST['byPage'];
    }

    $session->setUserPreference('filters', [
        'visibility' => $visibility,
        'owner' => $owner,
        'by_page' => $by_page
    ]);

    die();
}

$list->sessionList($sessionByUser, $data, $current_page, (int)$total_pages, $authorisations, $session->getValue('id'), $sort, $filters);
$base->table("end");
$list->filtersModal($filters);
$base->close_body();
$base->footer($session->getUserData()->getRole());
$base->base_footer(['list_session']);


