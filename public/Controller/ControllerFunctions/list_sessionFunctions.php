<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 12/03/2019
 * Time: 16:09
 * @param $POST
 * @param $sessionDAO
 * @param $responseDAO
 * @return array
 */

function action($POST, $sessionDAO, $responseDAO, $session){
    //$session = new Session();
    switch ($POST['action']){
        case "switch-off":
            try{
                $sessionDAO->updateByField($POST['idSwitchOff'], "active", 0);
            }catch (PDOException $e){
                return $ERROR = [
                    'message' => $e->getMessage()
                ];
            }
            break;
        case "switch-on":
            try{
                $sessionDAO->updateByField($POST['idSwitchOn'], "active", 1);
            }catch (PDOException $e){
                return $ERROR = [
                    'message' => $e->getMessage()
                ];
            }
            break;
        case "deleteResponse":
            try{
                $sessionByUser = $sessionDAO->selectSessionsByFilter("user_id", $session->getValue('id'));
                $shared = $sessionDAO->sharedSharedSessionsByUser($session->getValue('id'));
                $sessionByUser = array_merge($sessionByUser, $shared);
                $sessionIds = [];
                foreach ($sessionByUser as $userSession) {
                    $sessionIds[] = $userSession->getId();                    
                }
                if (in_array($POST['deleteResponse'],$sessionIds)) {
                    $responseDAO->deleteResponsesBySession($POST['deleteResponse']);
                    $sessionDAO->updateByField($POST['deleteResponse'], "participants", 0);
                } else {
                    header("Location: ?page=error&code=403");
                }
            }catch (PDOException $e){
                return $ERROR = [
                    'message' => $e->getMessage()
                ];
            }
            break;
        case "deleteSession":
            try{
                $sessionByUser = $sessionDAO->selectSessionsByFilter("user_id", $session->getValue('id'));
                $shared = $sessionDAO->sharedWritingSessionsByUser($session->getValue('id'));
                $sessionByUser = array_merge($sessionByUser, $shared);
                $sessionIds = [];
                foreach ($sessionByUser as $userSession) {
                    $sessionIds[] = $userSession->getId();                    
                }
                if (in_array($POST['deleteSession'],$sessionIds)) {
                    $sessionDAO->deleteSession($POST['deleteSession']);
                } else {
                    header("Location: ?page=error&code=403");
                }
            }catch (PDOException $e){
                return $ERROR = [
                    'message' => $e->getMessage()
                ];
            }
            break;
    }
}
