<?php

use PHPMailer\PHPMailer\PHPMailer;

function sendMailSMTP($dataSMTP, $user, $subject, $body){
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);
	try {
	    $mail->isSMTP();
	    $mail->Host       = $dataSMTP['host'];
	    $mail->SMTPAuth   = true;
	    $mail->Username   = $dataSMTP['username'];
	    $mail->Password   = $dataSMTP['password'];
		$mail->SMTPSecure = 'ssl';
		if ($dataSMTP['certs'] == 'on') {
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
		}
//		$mail->SMTPDebug  = 4;
	    $mail->Port       = $dataSMTP['port'];
	    $mail->setFrom($dataSMTP['username'], 'Qamphi');
	    $mail->addAddress($user->getEmail(), $user->getUsername());
	    $mail->isHTML(true);
        $mail->CharSet = $mail::CHARSET_UTF8;
	    $mail->Subject = $subject;
        $mail->Body = $body;
//	    $mail->

	    $mail->send();
	} catch (Exception $e) {
	    throw new Exception($e->getMessage());
	}

}

function generateToken()
{
    $token = sha1(uniqid(microtime(), true));
    return $token;
}
