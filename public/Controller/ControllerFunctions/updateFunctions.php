<?php

function updateDB($pdo) {
    $fileSql = __DIR__.'/../../install/install.sql';

    $query = file_get_contents($fileSql);
    try {
        $pdo->query($query);
    } catch (PDOException $e) {
        dump($e);die();
    }
    return true;
}