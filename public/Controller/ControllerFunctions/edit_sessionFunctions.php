<?php

use App\BO\Proposal;
use App\BO\Questions\Question;

/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 12/03/2019
 * Time: 16:42
 * @param $questionsPOST
 * @param $questionDAO
 * @param $proposalDAO
 * @param $sessionId
 * @return string
 */

//if post / values traitment
function postTreatment($questionsPOST = null, $questionDAO, $proposalDAO, $sessionId, $POST, $session, $sessionDAO, $accessKeysDAO, $responseDAO, $authorisationsDAO, $FILES){
    //variable of question loop    
    $qLoop = 1;
    if($questionsPOST != null){
        foreach ($questionsPOST as $question){
            if (!empty($question)):
            try{
                //question object
                $questionToObject = [
                    'session_id' => $sessionId,
                    'q_order' => $qLoop,
                    'q_text' => $question["Q".$qLoop],
                    'type_id' => $question["Q".$qLoop."type"],
                ];
                $questionObject = Question::instanciate($questionToObject);
                if($questionObject->getContent() == ''){
                    $questionObject->setContent($LANG['empty_title']);
                }
                    //update object     
                    $check = $questionDAO->selectQuestionByOrder($sessionId,$questionObject->getOrder());   
                    if ($check != null) {                      
                        $questionDAO->updateQuestion($sessionId, $questionObject);  
                        $questionId = $check->getId();                         
                    } else {
                        $questionId = $questionDAO->insert($sessionId, $questionObject->getOrder(),$questionObject->getContent(), $questionObject->getType());
                    }
                    if (get_class($questionObject)::needsProposals()) {
                        //check if proposal are not empty
                        $propoEmpty = 0;
                        $deletedProposals = [];
                        //we loop  times by proposal / $pLoop proposal loop
                        for( $pLoop=1 ; $pLoop <= 5 ; $pLoop++ ){
                            //we create proposal object for each question
                            if (!isset($question["Q".$qLoop."C".$pLoop])) {
                                $question["Q".$qLoop."C".$pLoop] = 0;
                            }
                            $proposalObject = [
                                'question_id' => $questionId,
                                'p_order' => $pLoop,
                                'p_text' => $question["Q".$qLoop."P".$pLoop],
                                'correct' => $question["Q".$qLoop."C".$pLoop]
                            ];
                            $proposal = new Proposal($proposalObject);
                            if($proposal->getContent() === ""){
                                $propoEmpty++;
                            }
                            //update proposal
                            $checkProposal = $proposalDAO->selectByOrder($proposal->getQuestionId(), $proposal->getOrder());                      
                            if (empty($checkProposal) && $proposal->getContent() !='') {
                                $proposalDAO->insert($questionId, $proposal->getOrder(), $proposal->getContent(), $proposal->getCorrect());
                            } 
                            if (!empty($checkProposal) && $proposal->getContent() !='') {
                                $proposalDAO->updateProposal($questionId, $proposal);
                            } 
                            if (!empty($checkProposal) && $proposal->getContent() === '') {
                                $deletedProposals[] = $checkProposal[0];
                            }
                        }
                        foreach ($deletedProposals as $proposalToDelete) {
                            $proposalDAO->deleteProposal($proposalToDelete->getId());
                        }
                        $updatedList = $proposalDAO->selectProposalNotEmpty($questionId);
                        foreach ($updatedList as $key => $value) {                        
                            $value->setOrder($key+1);              
                            $proposalDAO->update($value);
                        }
                    }
                    if (get_class($questionObject)::needsFile()) {
                        if (isset($FILES['upload_'.$questionObject->getOrder()])) {                            
                            $file = $FILES['upload_'.$questionObject->getOrder()];
                            if ($file['error'] === 0) {
                                if (is_file(__DIR__.'/../../tmp/question'.$questionId.'.png')) {
                                    $responseDAO->deleteResponsesByQuestion($questionId);
                                }
                                imagepng(imagecreatefromstring(file_get_contents($file['tmp_name'])), __DIR__.'/../../tmp/question'.$questionId.'.png');
                            }
                        }
                    }
                    $qLoop++;

            }catch (PDOException $e){
                return $e->getMessage();
            }
            endif;
        }

    }
    //it depends of user's choice
    switch ($POST['action']){
        case "save_quit":
            header("Location: index.php?page=list_session");
            break;
        case "save_stay":
            break;
        //insert empty question and  proposals
        case "save_new":
            try{
                $idQuestion = $questionDAO->insert($sessionId, $qLoop, "");
                for($i=1;$i<=5;$i++){
                    $proposalDAO->insert($idQuestion[0], $i, "", 0);
                }
            }catch (PDOException $e){
                return $e->getMessage();
            }
            break;
        case "deleteQuestion":
            try {
                $ids = $questionDAO->selectQuestionsByUser($session->getValue('id'));
                $shared = $questionDAO->selectSharedQuestionsByUser($session->getValue('id'));
                $ids = array_merge($ids, $shared);
                $id = $POST['idQuestion'];
                if (in_array($id, $ids)) {
                    $deleted = $questionDAO->selectQuestionById($id);
                    $questionDAO->deleteQuestionUpdateOrder($sessionId, $deleted->getOrder());                
                } else if ($id === '') {
                    header("Location: ?page=edit_session&id=".$sessionId);
                } else {
                    header("Location: ?page=error&code=403");
                }
            } catch (PDOException $e) {
                return $e->getMessage();
            }
            break;
        case "options":
            $auth="";
            if (isset($POST['anon'])) {
                $auth .= "anon ";            
            }           
            if (isset($POST['pseudo'])) {
                $auth .= "pseudo ";            
            }
            if (isset($POST['moodle'])) {
                $auth .= "moodle";            
            }
            $sessionDAO->setAuth($sessionId,str_replace(' ',',',trim($auth)));

            if (isset($POST['visibility']) && ($POST['visibility']==='private' || $POST['visibility']==='public')) {
                $currentSession = $sessionDAO->selectSessionsByFilter('id', $sessionId)[0];
                if ($POST['visibility']==='public') {
                    if ($currentSession->getAccess()==='private') {

                       $token = generateToken($sessionDAO, $accessKeysDAO);
                       $sessionDAO->updateByField($sessionId,'token',$token);
                       $accessKeysDAO->deleteKeysBySession($sessionId);
                    }                
                } else {
                    $sessionDAO->updateByField($sessionId,'auth','anon');
                    $sessionDAO->deleteToken($sessionId);
                    updateKeys($POST, $sessionId, $accessKeysDAO, $sessionDAO);
                    $sessionDAO->updateByField($sessionId,'access','private');
                    $sessionDAO->updateByField($sessionId,'auth','anon');
                    $sessionDAO->deleteToken($sessionId);
                }

                $sessionDAO->updateByField($sessionId,'access',$POST['visibility']);
            }
            break;
        case 'show_keys':
            $sessionDAO->updateByField($sessionId,'auth','anon');
            $sessionDAO->updateByField($sessionId,'access','private');
            $sessionDAO->deleteToken($sessionId);
            
            updateKeys($POST, $sessionId, $accessKeysDAO, $sessionDAO);
            $keys = $accessKeysDAO->selectTokensBySession($sessionId);
            echo json_encode($keys);
            exit;
            
            break;
        case 'exportCSV':
            updateKeys($POST, $sessionId, $accessKeysDAO, $sessionDAO);

            $sessionDAO->updateByField($sessionId,'auth','anon');
            $sessionDAO->updateByField($sessionId,'access','private');
            $sessionDAO->deleteToken($sessionId);

            $csvFile = __DIR__.'/../../tmp/session'.$sessionId.'.csv';    

            $sessionTokens = $accessKeysDAO->selectTokensBySession($sessionId);
            $file = fopen($csvFile,'w');

            foreach ($sessionTokens as $key) {  
                $key['token'] = "\"".$key['token']."\"";                
                fputcsv($file, $key);
            }

            fclose($file);
            break;    
        case 'deleteCSV':
            $csvFile = __DIR__.'/../../tmp/session'.$sessionId.'.csv';
            if (file_exists($csvFile)) {
                unlink($csvFile);
            }
            break;
        case 'add_guest':
            $authorisationsDAO->insert($sessionId, $POST['guest'], $POST['right']);
            break;
        case 'delete_guest':
            $authorisationsDAO->deleteBySessionGuest($sessionId, $POST['guest']);
            break;
        case 'update_rights':
            $authorisationsDAO->updateRights($sessionId, $POST['guest']);
            break;
    }
}

function changeOrderQuestion($idSession, $questionDAO, $POST){
    try{
        $listQuestions = $questionDAO->selectQuestionBySession($idSession);
    }catch (PDOException $e){
        return $e->getMessage();
    }

    switch ($POST['orderBy']){
        case 'up':
            try{
                $questionToUpGrade = $questionDAO->selectQuestionByOrder($idSession, $POST['order']);
                if ($questionToUpGrade->getOrder() > 1){
                    $newOrder = $questionToUpGrade->getOrder() - 1;
                    $questionToDownGrade = $questionDAO->selectQuestionByOrder($idSession, $newOrder);
                    $questionToUpGrade->setOrder($newOrder);
                    $questionToDownGrade->setOrder($newOrder + 1 );
                    $questionDAO->updateOrder($questionToDownGrade);
                    $questionDAO->updateOrder($questionToUpGrade);
                    return $questionToUpGrade->getId();
                }
            }catch (PDOException $e){
                return $e->getMessage();
            }
            break;
        case 'down':
            try{
                $questionToUpGrade = $questionDAO->selectQuestionByOrder($idSession, $POST['order']);
                if ($questionToUpGrade->getOrder() < count($listQuestions)){
                    $newOrder = $questionToUpGrade->getOrder() + 1;
                    $questionToDownGrade = $questionDAO->selectQuestionByOrder($idSession, $newOrder);
                    if($questionToDownGrade->getContent() !== ""){
                        $questionToUpGrade->setOrder($newOrder);
                        $questionToDownGrade->setOrder($newOrder - 1 );
                        $questionDAO->updateOrder($questionToDownGrade);
                        $questionDAO->updateOrder($questionToUpGrade);
                        return $questionToUpGrade->getId();
                    }else{
                        return false;
                    } 
                }
            }catch (PDOException $e){
                throw new PDOException($e->getMessage());  
            }
            break;
    }
}

//Generate random unique tokens (size default = 4)
function generateToken($sessionDAO, $accessKeysDAO, $size = 4) {
    $privateTokens = $accessKeysDAO->selectTokens();
    $publicTokens = $sessionDAO->allTokens();

    do {
        $token = bin2hex(random_bytes($size / 2));
    } while (in_array($token, $privateTokens) || in_array($token, $publicTokens));

    return $token;
}

//Update private keys for given session
//If user want more keys we insert. If he want less we delete
function updateKeys($POST, $sessionId, $accessKeysDAO, $sessionDAO) {
    $countKeys = $accessKeysDAO->countKeysBySession($sessionId);
    $keysWanted = $POST['keys'];
    if ($keysWanted < $countKeys) {
        $toDelete = $countKeys-$keysWanted;
        $accessKeysDAO->deleteSomeBySession($sessionId, $toDelete);
    } elseif ($keysWanted > $countKeys) {
        $toAdd = $keysWanted - $countKeys;
        $keys = [];
        for ($i=0; $i < $toAdd; $i++) { 
            $keys [] = generateToken($sessionDAO, $accessKeysDAO);
        }
        $accessKeysDAO->insertKeys($keys, $sessionId);
    }
}
