<?php

/**
 * @param array $configFile Configuration
 * @param array $daos DAOS instances of all tables
 * @return string export.sql file name with path
 */
function exportDatabase($configFile, $daos = []) {
    $date = date('d/m/Y');
    $db_name = $configFile['db_name'];
    $sqlString = "SET NAMES 'utf8mb4';\n\n--\n-- Base : $db_name \n-- Date : $date \n--\nSET FOREIGN_KEY_CHECKS = 0;\n\n";
    $config = $daos['configDAO']->selectConfig();

    $langDump = $daos['langDAO']->langArray();


    $tables = [];


    $tables[] = $daos['userDAO']->allUsers();
    $tables[] = $daos['sessionDAO']->allSessions();
    $tables[] = $daos['questionTypesDAO']->selectAllTypes();
    $tables[] = $daos['questionDAO']->allQuestions();
    $tables[] = $daos['proposalDAO']->allProposals();
    $tables[] = $daos['responseDAO']->allResponses();

    $sqlString = dumpArray($config, $sqlString, 'config');
    $sqlString = dumpArray($langDump, $sqlString, 'lang');

    foreach ($tables as $table) {
        $sqlString = dumpTable($table, $sqlString);
    }

    if ($daos['statsDAO']->tableExists('statistics')) {
        $statsDump = $daos['statsDAO']->stats_all_time();
        $sqlString = dumpArray($statsDump, $sqlString, 'statistics');
    }
    if ($daos['accessKeysDAO']->tableExists('access_keys')) {
        $accessKeys = $daos['accessKeysDAO']->allKeys();
        $sqlString = dumpArray($accessKeys, $sqlString, 'access_keys');
    }
    if ($daos['authorisationsDAO']->tableExists('authorisations')) {
        $authsDump = $daos['authorisationsDAO']->allAuthorisations();
        $sqlString = dumpArray($authsDump, $sqlString, 'authorisations');
    }

    $sqlString .= "\n\nSET FOREIGN_KEY_CHECKS = 1;";
    $filename = __DIR__."/../../tmp/".$db_name.date('Ymd').".sql";
    $file = fopen($filename, "w");
    fwrite($file, $sqlString);

    return realpath($filename);
}

function dumpTable($list, $sqlString) {
    if ($list != []) {
        if (gettype($list[0]) == 'object') {
            $class = get_class($list[0]);
            $table = getTableName($class);
            $attributes = array_keys($list[0]->getAttributes());
        } else {
            $table = $list['name'];
            unset($list['name']);
            $attributes = array_keys($list[0]);
        }
        $sqlString .= "\n\n--\n-- $table table\n--\n\nINSERT INTO `$table` (";
        foreach ($attributes as $attribute) {
            $sqlString .= "`$attribute`, ";
        }
        $sqlString = substr($sqlString, 0, -2);
        $sqlString .= ") VALUES";

        $i = 0;
        foreach ($list as $entry) {
            $sqlString .= "\n(";
            //$array = $entry->getAttributes();
            $array = gettype($entry) == 'object' ? $entry->getAttributes() : $entry;

            foreach ($array as $key => $value) {
                $sqlString .= quote($value) . ', ';
            }
            $sqlString = substr($sqlString, 0, -2);
            if ($i != count($list) - 1) {
                $sqlString .= "),";
            } else {
                $sqlString .= ");";
            }
            $i++;
        }
    }
    return $sqlString;
}


function dumpTableObject($list, $sqlString) {
    $class = get_class($list[0]);
    $table = getTableName($class);
    $sqlString .= "\n\n--\n-- $table tablenr--\n\nINSERT INTO `$table` (";
    foreach ($list[0]->getAttributes() as $key => $value) {
        $sqlString .= "`$key`, ";
    }
    $sqlString = substr($sqlString, 0, -2);
    $sqlString .= ") VALUES";

    $i = 0;
    foreach ($list as $object) {
        $sqlString .= "\r(";
        $objectArray = $object->getAttributes();
        foreach ($objectArray as $key => $value) {
            $sqlString .= quote($value).', ';
        }
        $sqlString = substr($sqlString, 0, -2);
        if ($i != count($list) -1) {
            $sqlString .= "),";
        } else {
            $sqlString .= ");";
        }
        $i++;
    }
    return $sqlString;
}

function quote($attr) {
    if ((string)(int) $attr == $attr) {
        return $attr;
    }
    if ($attr == null) {
        return 'null';
    }
    return '\''.str_replace('\'','\'\'',$attr).'\'';
}

function dumpArray($config, $sqlString, $table) {
    if ($config !== []) {
        switch ($table) {
            case 'config' :
                $rows = '(`id`, `type`, `name`, `value`)';
                break;
            case 'lang' :
                $rows = '(`id`, `name`, `content`)';
                break;
            case 'statistics' :
                $rows = '(`id`, `session_create`, `session_join`, `month`, `year`)';
                break;
            case 'access_keys' :
                $rows = '(`id`, `token`, `session_id`, `validity_date`)';
                break;
            case 'authorisations':
                $rows = '(`id`, `session_id`, `guest_id`, `rights`)';
                break;
            default: break;
        }


        $sqlString .= "\n\n--\n-- $table table\n--\n\nINSERT INTO `$table` $rows VALUES";

        $i = 0;
        foreach ($config as $array) {
            if (!($table == 'config' && $array['name'] == 'sql_version')) {
                $sqlString .= "\n(";
                foreach ($array as $key => $value) {
                    $sqlString .= quote($value) . ', ';
                }

                $sqlString = substr($sqlString, 0, -2);
                $sqlString .= "),";
                $i++;
            }
        }
        $sqlString = substr($sqlString, 0, -2);
        //if ($i != count($config) - 1) {
        $sqlString .= ");";
    }
    return $sqlString;
}

function getTableName($className) {
    $tableName = '';
    $className = str_replace('App\\BO\\','',$className);
    foreach (str_split($className) as $index => $char) {
        if (strtoupper($char) === $char) {
            if ($index == 0) {
                $tableName .= strtolower($char);
            } else {
                $tableName .= '_'.strtolower($char);
            }
        } else {
            $tableName .= $char;
        }
    }
    $tableName .= 's';
    if (strpos($tableName,'questions') !== false) {
        $tableName = 'questions';
    }
    return $tableName;
}