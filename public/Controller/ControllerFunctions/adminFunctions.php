<?php

use App\BO\User;

function checkImage($file){
    //directory
    $target_dir = "../public/image/";
    $target_file = $target_dir . basename($file["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else if ($check === false) {
        return $LANG['file_exist'];
    }else if ($file["size"] > 5000000) {
        return "Fichier trop volumineux";
    }else if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {
        return $LANG['file_type'];
    }
    // error
    if ($uploadOk == 0) {
        return $LANG['file_exist'];
    } else {
        if(!is_writable ("../public/image/")){
            return $LANG['permissions'];
        }else if (move_uploaded_file($file["tmp_name"], $target_file)) {
            return "true";
        }else{
            return $LANG['file_exist'];
        }
    }
}

function insertUser($POST, $userDAO, $moodleDAO = null){
    //if manual account
    if(!isset($POST['newAdd'])){
        //username format
        $upnValid = stristr($POST['username'], '.');
        //check if username exist
        $upnExist = $userDAO->userExistByName($POST['username']);
        $username = $POST['username'];
        if ($upnExist == true) {
            return $ERROR = [
                'message' => $LANG['id_exist']
            ];
        } elseif ($upnValid === 0) {
            return $ERROR = [
                'message' => $LANG['id_format']
            ];
        }
        $auth = 'manual';
        $upn = $POST['username'];
        $email = $POST['email'];
    //add moodle account
    }else{
        $upnExist = $userDAO->userExistByName($POST['newAdd']);
        $userMoodle = $moodleDAO->selectUserByName($POST['newAdd']);
        if ($upnExist == true) {
            return $ERROR = [
                'message' => $LANG['id_exist']
            ];
        }else{
            $username = $userMoodle[1]['username'];
            $auth = 'moodle';
            $email = $userMoodle[1]['email'];
        }
    }
    //table to create user object
    $userData = [
        'username' => $username,
        'email' => $email,
        'auth' => $auth,
        'role' => $POST['typeAdd'],
    ];
    $user = new User($userData);
    $dirname = str_replace($_SERVER['DOCUMENT_ROOT'],'',dirname(__DIR__.'/../../'));
    $qamphi = str_replace('/Controller','',$dirname);
    $domain = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$qamphi;
    $link = $domain;
    if ($auth == 'manual') {
        $token = generateToken();
        $user->setRequest(date("Y-m-d H:i:s"));
        $user->setToken($token);
        $user->setOpen(1);
        $link .= '/public/index.php?page=resetPw&token=' . $user->getToken();
    }
    try{
        $userDAO->insertUser($user);
        $config= App\Config::getInstance();
        $dataSMTP = [
            'host' => $config->getSMTP('host'),
            'port' => $config->getSMTP('port'),
            'username' => $config->getSMTP('username'),
            'password' => $config->getSMTP('password'),
            'certs' => $config->getSMTP('certs'),
        ];
        $body = '<p>'.$LANG['mail_new_account'].'</p><br>
        	    <a href = "'.$link.'">'.$LANG['mail_new_account_link'].'</a>';

        sendMailSMTP($dataSMTP, $user, $LANG['mail_new_account_subject'], $body);
        return true;
    } catch (PDOException $e){
        return $ERROR = [
            'message' => $e->getMessage()
        ];
    } catch (Exception $e) {
        return $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

//Moodle API access / not used
function webServiceMoodle($POST, $domain, $token){
    $serverUrl = $domain.'/webservice/rest/server.php?wstoken='
        .$token.'&wsfunction=core_user_get_users&moodlewsrestformat=json&criteria[1][key]=username&criteria[1][value]='
        .$POST['moodleSearch'];

    $curl = curl_init($serverUrl);
    //access to data and decode
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
    $post = json_decode(curl_exec($curl));
    curl_close($curl);
    if (isset($post->exception)) {
        return $ERROR = [
            'success' => false,
            'message' => 'Erreur de paramétrage de l\'API'
        ];
        exit;
    }else {
        $listUsers = [];
        $i = 1;
        foreach ($post->users as $user) {
            $listUsers[$i] = $user;
            $i++;
        }
        return $listUsers;
    }
}

//we extract information from config.php into table
function getConfigToArray(){
    //directory to file
    $filename = "../config/Config.php";
    $data = file($filename);
    $datasFormated = [];
    $i = 0;
    foreach($data as $key => $row){
        //we delete empty line
        if(strlen ($row) != 2 ){
            //we explode key and value
            if(strstr($row, "=>")){
                $tab = explode( '=>', $row);
                $j = 0;
                //for each value we delete space and other parasite caracters
                foreach ($tab as $value) {
                    $tab[$j] = trim($value, " \t\n\r\0\x0B");
                    $tab[$j] = str_replace(array("'", '"', ","), "", $tab[$j]);
                    $j++;
                }
                //we display as key => value
                $datasFormated[$tab[0]] = $tab[1];
                $i++;
            }
        }
    }
    return $datasFormated;
}

