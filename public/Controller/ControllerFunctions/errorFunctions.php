<?php


use App\HTML\base;
use App\HTML\error;

function log_error(Exception $e, $logger, $page, $backlink) {
    $error_message = $e->getMessage();
    $error_message .= ' -- Page : '.$page;

    $logger->error($error_message);

    $base = new base();
    $errorHTML = new error();

    $base->base_header();

    $base->openFlexBody($backlink, 'middle');

    $errorHTML->showError($LANG['error_occured'], $error_message);

    $base->close_body();

    $base->base_footer();
}