<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 22/03/2019
 * Time: 09:10
 */

function responseTreatment($listResponseObject){
    //to stock response numbers by proposal
    $tabCount = [0,0,0,0,0];
    foreach ($listResponseObject as $responseObject){
        $response = $responseObject->getResponse();
        //we explode responses in table
        $tabResponse = explode("-", $response);
        $i = 0;
        foreach ($tabResponse as $row){
            if ($row != ""){
                $tableau[$i] = [
                    'response' => $row,
                    'correct' => ""
                ];
                $i++;
            }
        }
        $tabLenght = count($tableau);
        $tabLenght = intval($tabLenght);
        //we increment table for each response valid
        for ($f=0;$f<$tabLenght;$f++){
            if($tableau[$f]['response'] != 0){
                $tabCount[$f] = $tabCount[$f] +1;
            }
        }
    }
    return $tabCount;
}

//we construct string response to compare both
function constructStringResponse($listProposition){
    if ($listProposition === null || empty($listProposition)){
        return '-0-0-0-0-0-';
    }
    $string = "";
    $i=1;
    foreach ($listProposition as $proposition){
        if($proposition->getCorrect() == 1){
            $string.="-".$i;
        }else{
            $string.="-0";
        }
        $i++;
    }
    $string.="-";
    for ($j=$i; $j < 6; $j++) { 
        $string .= '0-';
    }

    return $string;

}

//to know if user got response or partial
function traitementPartial($responseUser, $stringResponse){
    //false by default
    $result = false;
    $resultUser = [];
    $resultResponse = [];
    //we extract numbers of response
    $explodeUser = explode("-", $responseUser);
    $i=0;
    //we create table
    foreach ($explodeUser as $value){
        if($value != '' && $value != '0'){
            $resultUser[$i] = $value;
            $i++;
        }
    }

    //same way 
    $explodeResponse = explode("-", $stringResponse);
    $f=0;
    foreach ($explodeResponse as $value){
        if($value != '' && $value != '0'){
            $resultResponse[$f] = $value;
            $f++;
        }
    }
    //we compare tables
    foreach ($resultResponse as $response){
        if($resultUser != null && $resultUser != ''){
            foreach ($resultUser as $user){
                if($user == $response){
                    $result = true;
                }
            }
        }
    }
    return $result;
}

function statsInfo($sessionDAO, $proposalDAO, $responseDAO, $listQuestions){
    $i = 0;
    $listUsers = $sessionDAO->selectUserBySession($listQuestions[0]->getSessionId());
    $length = count($listUsers);
    for($j=0;$j< $length;$j++){
        $listUsers[$j]['score'] = 0;
    }
    foreach ($listQuestions as $question){
    //variables to display stats
        // $correct = 0;
        // $incorrect = 0;
        // $partial = 0;
        $listProposition = $proposalDAO->selectProposalByQuestion($question->getId());
        $stringResponse = constructStringResponse($listProposition);
        //if response exist
        if($stringResponse != '-0-0-0-0-0-'){
            $listResponse = $responseDAO->selectResponseByQuestion($question->getId());
            //foreach response we compare strings and we increment correct or incorrect
            foreach ($listResponse as $response){
                $trim = traitementPartial($response->getResponse(), $stringResponse);
                $check = strcmp (  $response->getResponse(), $stringResponse );
                if($check == 0) {
                    // $correct = $correct + 1;
                    //foreach correct we increment user score
                    for ($f = 0; $f < $length; $f++) {
                        if ($response->getUsername() == $listUsers[$f]['username']) {
                            $listUsers[$f]['score'] = $listUsers[$f]['score'] + 1;
                        }
                    }
                }
            }
        }
        $i++;
    }
    
    $newListUsers = rankingUsers($listUsers);

    $tableau['listUsers'] = $newListUsers;
    return $tableau;
}

function checkList($listQuestions, $questionDAO, $proposalDAO){
    foreach ($listQuestions as $question) {
        $loop = 1;
        if($question->getContent() == ""){
            $questionDAO->deleteQuestion($question);
        }
    }
}

function rankingUsers($listUsers){
    $newListUsers = [];

    for($i = 0 ; $i < sizeof($listUsers); $i++){
        $newListUsers[$listUsers[$i][0]] = $listUsers[$i]['score']; 
    }
    arsort($newListUsers,SORT_NUMERIC);

    return $newListUsers;
}
