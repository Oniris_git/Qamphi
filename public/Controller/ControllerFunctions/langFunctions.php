<?php 

/**
 * Generate langfile
 * Langfile initialize $LANG
 * @param array lang as key => trad
 * @param string lang to update (xx_XX format)
 */
function writeLangFile($array, $lang) {
    $path = "../config/lang/".$lang.".php";
    $file = fopen($path,'w');
    fwrite($file, '<?php
    $LANG=[
');
    foreach($array as $key => $value) {
        fwrite($file, "\t\t\"$key\" =>     \"$value\",\r\n");
    }
    fwrite($file,"];");
    fclose($file);
}

/**
 * remove prefixes (gen_, ttl_, btn_, err_, abt_ from $LANG)
 * @param array $LANG
 */
function filterLang($array) {
    foreach($array as $key=>$value) {
        unset($array[$key]);
        $key = substr($key,4);
        $array[$key] = $value;
    }
    return $array;
}

function getAboutTrads($currentLang) {
    if ($currentLang != 'fr_FR' && $currentLang != 'en_EN') {
        require '../config/lang/en_EN.php';
    }

    $abouts = [];
    foreach ($LANG as $key => $trad) {
        if (strpos($key, 'abt_')) {
            $abouts[$key] = $trad;
        }
    }
    require '../config/lang/'.$currentLang.'.php';
    return $abouts;
}