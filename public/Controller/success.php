<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\home;
use App\HTML\success;

$base = new base();
$success = new success();

$argGet = [
    'state' => FILTER_SANITIZE_STRING,
];

$GET = filter_input_array(INPUT_GET, $argGet, false);

if(isset($GET['state'])){
   $title =  $LANG['end_session'];
}else{
    $title = null;
}

$base->base_header();

$base->openFlexBody($backLink, 'little');

$base->table('flex', $from);

$success->success($title);

$base->close_body();

$base->base_footer();
