<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\BO\User;
use App\DAL\MoodleAccountDAO;
use App\HTML\auth;
use App\HTML\base;

include __DIR__."/../ControllerFunctions/adminFunctions.php";
include __DIR__."/../ControllerFunctions/loginFunctions.php";

if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin'){
    header("Location: ?page=error&code=403");
}

$base = new base();
$auth = new auth();

$listAdminUsers = [];

$args = [
    'username' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_STRING,
    'confirmPassword' => FILTER_SANITIZE_STRING,
    'moodleSearch' => FILTER_SANITIZE_STRING,
    'newAdd' => FILTER_SANITIZE_STRING,
    'deleteUser' => FILTER_VALIDATE_INT,
    'typeAdd' => FILTER_SANITIZE_STRING,
    'email' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

if($moodle == "on"){
    $moodleDAO = new MoodleAccountDAO($connectorMoodle);
}

//if posts exist to create user
if(isset($POST['username']) && isset($POST['confirmPassword']) && isset($POST['password']) || isset($POST['newAdd'])){
    //in case of error returns for function we put result into $ERROR
    //insert moodle account
    if(isset($POST['newAdd'])){
        try{
            $user = $userDAO->selectUserByUsername($POST['newAdd']);

                if($user != null){
                    $ERROR =$userDAO->changeProfile($user->getId(), $POST['typeAdd']);
                }else{
                    $ERROR = insertUser($POST, $userDAO, $moodleDAO);
                }

        }catch(PDOException $e){
            $ERROR = [
                'message' => $e->getMessage()
            ];
        }
    //insert manual account
    }else{
        try{
            $user = $userDAO->selectUserByUsername($POST['username']);
                if($user != null){
                    $ERROR =$userDAO->changeProfile($user->getId(), $POST['typeAdd']);
                }else{
                    $ERROR = insertUser($POST, $userDAO);
                }
        }catch(PDOException $e){
            $ERROR = [
                'message' => $e->getMessage()
            ];
        }
    }
    //if variable $ERROR == true the result have no errors
    if($ERROR === true){
        $ERROR = [];
        $VALID = [$LANG['account_created']];
    }
    //in case of moodle add we load just a part of the page in Ajax
    if(isset($POST['newAdd'])){
        $result = $userDAO->selectUsersByType($POST['typeAdd']);
        $countSessions = [];
        $questionsByUser = [];
        foreach ($result as $teacher) {
            try {
                $countSessions[$teacher->getId()] = $userDAO->countSessionsByUser($teacher->getId());
                $questionsByUser[$teacher->getId()] = $userDAO->countQuestionsByUser($teacher->getId());
            } catch (PDOException $e) {
                $base->displayError([$e->getMessage()]);
            }
        }
        $auth->tabAjax($result, $POST['typeAdd'], $countSessions, $questionsByUser);
        exit;
    }
//in case of user request for a search in moodle
}elseif(isset($POST['moodleSearch'])){
    try {
        $result = $moodleDAO->selectUserByName($POST['moodleSearch']);
    } catch (PDOException $e) {
        $base->displayError([$e->getMessage()]);
    }
    if(isset($result['message'])){
        echo $result['message'];
    }else{
        echo $auth->tabMoodle($result, $POST['typeAdd']);
    }
    exit;
//if user request to delete one
}elseif (isset($POST['deleteUser'])){
    try{
        //check if user is not principal admin
        try {
            $user = $userDAO->selectUserById($POST['deleteUser']);
        } catch (PDOException $e) {
            $base->displayError([$e->getMessage()]);
        }
        if($user->getAuth() =='lock'){
            $ERROR = [
                'message' => $LANG['account_delete']
            ];
        }else{
            try {
                $userDAO->deleteUser($user);
            } catch (PDOException $e) {
                $base->displayError([$e->getMessage()]);
            }
        }
    }catch (PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

try{
    $listAdminUsers = $userDAO->selectUsersByType('admin');
    $listTeacherUsers = $userDAO->selectUsersByType('teacher');
    $countSessions = [];
    $questionsByUser = [];
    foreach ($listTeacherUsers as $teacher) {
        $countSessions[$teacher->getId()] = $userDAO->countSessionsByUser($teacher->getId());
        $questionsByUser[$teacher->getId()] = $userDAO->countQuestionsByUser($teacher->getId());
    }
    foreach ($listAdminUsers as $admin) {
        $countSessionsAdmin[$admin->getId()] = $userDAO->countSessionsByUser($admin->getId());
        $questionsByAdmin[$admin->getId()] = $userDAO->countQuestionsByUser($admin->getId());
    }
}catch(PDOException $e){
    $ERROR = [
        'message' => $e->getMessage()
    ];
}

$base->base_header();
$base->open_body($backLink, "large");
$base->table("without-btn");
$auth->authTitle();

if(isset($ERROR)){
    $base->displayError($ERROR);
}
if(isset($VALID)){
    $base->displayValid($VALID);
}

$auth->adminAccounts($listAdminUsers, $countSessionsAdmin, $questionsByAdmin, $moodle);
$auth->teacherAccounts($listTeacherUsers, $countSessions, $questionsByUser ,$moodle);

$base->table("end");
$base->close_body();
$base->base_footer();
$auth->footerAuth();

