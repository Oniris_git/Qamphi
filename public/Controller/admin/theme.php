<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 07/05/2019
 * Time: 16:55
 */

//color picker was integrate but we changed mind about it and now it is not include 
//but there are code lines to integrate it

use App\HTML\base;
use App\HTML\theme;

include __DIR__."/../ControllerFunctions/adminFunctions.php";

$base = new base();
$theme = new theme();

//check access
if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin'){
    header("Location: ?page=error&code=403");
}

$args = [
    'colorSelected' => FILTER_SANITIZE_STRING,
    'defaultColor' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

if (isset($POST['colorSelected'])){
    try{
        $color = $POST['colorSelected'];
        $configDAO->updateConfig("view", "color", $color);
        exit;
    }catch (PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}elseif (isset($POST['defaultColor'])) {
    try {
        $color = "white";
        $configDAO->updateConfig("view", "color", $color);
        //$mainColor = $configDAO->selectConfigByType("view", "color")['value'];
    } catch (PDOException $e) {
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}elseif (isset($_FILES['backgroundSelected'])){
    $file = checkImage($_FILES['backgroundSelected']);
    if($file != "true"){
        $ERROR = [
            'message' => $file
        ];
    }else{
        $target_dir = "../public/image/";
        $target_file = $target_dir . basename($_FILES['backgroundSelected']["name"]);
        try {
            $configDAO->updateConfig('view', 'background', $target_file);
        } catch (PDOException $e) {
            $ERROR = [
                'message' => $e->getMessage()
            ];
        }
    }
    //$mainColor = $configDAO->selectConfigByType("view", "color")['value'];
    try {
        $backLink = $configDAO->selectConfigByType("view", "background")['value'];
    } catch (PDOException $e) {
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}elseif (isset($_FILES['errorSelected'])){
    $formats = ['jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF'];
    foreach ($formats as $format) {
        if (file_exists(__DIR__."/../../image/error.".$format)) {
            $fileFormat = $format;
        }
    }
    $errorFile = $_FILES['errorSelected'];
    $errorFileFormat = explode('.', $errorFile['name'])[1];
    $errorFile['name'] = 'error.'.$errorFileFormat;
    $file = checkImage($errorFile);
    if($file != "true"){
        $ERROR = [
            'message' => $file
        ];
    } else {
        if ($errorFileFormat != $fileFormat) {
            unlink(__DIR__."/../../image/error.".$fileFormat);
        }
        header('Location: ?page=admin');
    }
}

$base = new base();

$base->base_header(['theme']);

$base->openFlexBody($backLink, 'large', '15');

$base->table("flex");

$theme->themeTitle();

$disabled = '';

if(!is_writable (__DIR__."/../../image")){
    $ERROR['message'] = $LANG['permissions'];
    $disabled = 'disabled';
}

if(isset($ERROR)){
    $base->displayError($ERROR);
}

$theme->backgroundForm($backLink, $disabled);

$base->table("end");

$base->close_body();

$base->base_footer();

//$theme->footerTheme();
