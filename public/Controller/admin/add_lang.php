<?php


use App\HTML\base;
use App\HTML\lang;

include __DIR__."/../ControllerFunctions/adminFunctions.php";

$base = new base();
$langHTML = new lang();

foreach (filterLang($LANG) as $key => $value){
    $args[$key] = FILTER_SANITIZE_STRING ;
}
$args['langName'] = FILTER_SANITIZE_STRING;
$args['langContent'] = FILTER_SANITIZE_STRING;

$POST = filter_input_array(INPUT_POST, $args, false);


foreach ($LANG as $name => $trad) {
    switch (substr($name,0,4)) {
        case 'ttl_' : $mainTitles[] = substr($name,4);
        break;
        case 'btn_' : $buttons[] = substr($name,4);
        break;
        case 'tab_' : $tab[] = substr($name,4);
        break;
        case 'err_' : $errors[] = substr($name,4);
        break;
        case 'gen_' : $general[] = substr($name,4);        
        break;
        case 'abt_' : $about[] = substr($name, 4);
        break;
        default : break;
    }
}

$LANG = filterLang($LANG);

if($POST != null){

    $attribute = 'value';

    //regex for lang ISO code name : xx_XX
    $langNameFormat = '#^[a-z]{2}_[A-Z]{2}$#';

    if (preg_match($langNameFormat, $POST['langName']) !== 1) {
        $ERROR['langNameFormat'] = $LANG['lang_name'];
    }

    if (in_array($POST['langName'], $langCodes)) {
        $ERROR['langExist'] = $LANG['lang_exists'];
    }

    if (in_array($POST['langContent'], $allLangs)) {
        $ERROR['langContentExist'] = $LANG['lang_content'];
    }

    if (isset($_FILES['flagFile'])) {
        $file = $_FILES['flagFile'];
        $checkFile = checkImage($file);
        if($checkFile != "true"){
            $ERROR['fileError'] = $checkFile;
        }

        $fileName = explode('.', $file['name'])[0];
        if ($fileName != $POST['langName']) {
            $ERROR['fileName'] = $LANG['file_name'];
            $target_dir = "../public/image/";
            $target_file = $target_dir . basename($file["name"]);
            if (file_exists($target_file)) {
                unlink($target_file);
            }
        }
    } else {
        $ERROR['fileRequired'] = $LANG['file_required'];
    }

    foreach ($POST as $key => $value){
        if ($POST[$key] === null || $POST[$key] === '') {
            $ERROR['requiredFields'] = $LANG['field_required'];
        }
    }

    //Generate lang file, add lang in DB
    if (!isset($ERROR)) {
        $TEMP = getAboutTrads($lang);
        $LANG = filterLang($LANG);
        foreach ($POST as $key => $value){
            if($POST[$key] != null && $key != 'langName' && $key != 'langContent'){
                $LANG[$key] = $value;
            }
            if ($POST[$key] === null || $POST[$key] === '') {
                $ERROR['requiredFields'] = $LANG['field_required'];
            }
        }
        foreach ($LANG as $name => $value) {
            unset($LANG[$name]);
            if (in_array($name, $mainTitles)) {
                $name = 'ttl_'.$name;
            }
            elseif (in_array($name, $buttons)) {
                $name = 'btn_'.$name;
            }
            elseif (in_array($name, $tab)) {
                $name = 'tab_'.$name;
            }
            elseif (in_array($name, $errors)) {
                $name = 'err_'.$name;
            } 
            elseif (in_array($name, $about)) {
                $name = 'abt_'.$name;
            } else {
                $name = 'gen_'.$name;
            }        
            if (in_array($name, $about)) {     
                $LANG[$name]=$TEMP[$name];
            } else {
                $LANG[$name]=$value;
            }
        }
        writeLangFile($LANG, $POST['langName']);
        try {
            $langDAO->addLang($POST['langName'], $POST['langContent']);
            $configDAO->updateConfig('general', 'lang', $POST['langName']);
        } catch (PDOException $e) {
            log_error($e, $logger, 'add_lang', $session->getUserdata(), $backLink);
            die();
        }
        $session->setUserPreference('lang', $POST['langName']);
        header('Location: ?page=success');
    }
} else {
    $attribute = 'placeholder';
}

$base->base_header();
$base->open_body($backLink, 'large');
$base->table("without-btn");

$disabled = '';

if(!is_writable (__DIR__."/../../../config/lang")){
    $ERROR['message'] = $LANG['permissions'];
    $disabled = 'disabled';
}

if(isset($ERROR)){
    $base->displayError($ERROR);
}
$langHTML->newLangForm();


//If errors : attribute = 'value' and fields are prefilled with user previous choice

$langHTML->general($general, $attribute, $POST);
$langHTML->mainTitles($mainTitles, $attribute, $POST);
$langHTML->buttons($buttons, $attribute, $POST);
$langHTML->tab($tab, $attribute, $POST);
$langHTML->errors($errors, $attribute, $POST, $disabled);
$base->table("end");
$base->close_body();
$base->base_footer();