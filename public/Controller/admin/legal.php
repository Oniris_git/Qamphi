<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\legal;

if(isset($_POST['html'])){
    try{
        if (isset($session) && $auth->isAnon() == false && $session->getValue('role') == 'Admin'){
            $configDAO->updateLegal($_POST['html']);
        exit;
        } else {
            header("Location: ?page=error&code=403");
        }
    }catch (PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
        exit;
    }
}else{
    try{
        $legalContent = $configDAO->selectConfigByType('view','legal');
    }catch (PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

$base = new base();
$legal = new legal();

$base->base_header();
$base->open_body($backLink, 'middle');
$base->table("without-btn");

if(isset($ERROR)){
    $base->displayError($ERROR);
}

$legal->legalTitle($from);
if(isset($session) && $auth->isAnon() == false && $session->getStatus() != 'IDLE' && $session->getValue('role') == 'Admin'){
    $legal->legalEditor();
    $legal->contenEditor($legalContent);
}else{
    $legal->displayLegal($legalContent);
}

$base->table("end");
$base->close_body();
$base->base_footer(['legal']);

