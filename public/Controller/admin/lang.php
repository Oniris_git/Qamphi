<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\base;
use App\HTML\lang;

if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin'){
    header("Location: ?page=error&code=403");
}
foreach (filterLang($LANG) as $key => $value){
    $args[$key] = FILTER_SANITIZE_STRING ;
}
$args['language'] = FILTER_SANITIZE_STRING;

$POST = filter_input_array(INPUT_POST, $args, false);

if (isset($POST['language'])) {
    try {
        $configDAO->updateConfig('general', 'lang', $POST['language']);
        $session->setUserPreference('lang', $POST['language']);
    } catch (PDOException $e) {
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
    unset($POST['language']);
}

try {
    $actualLang = $configDAO->selectConfigByType('general', 'lang')['value'];
} catch (PDOException $e) {
    $ERROR = [
        'message' => $e->getMessage()
    ];
}

if(array_key_exists('lang', $user_prefs) && $actualLang != $user_prefs['lang']) {
    require __DIR__.'/../../../config/lang/'.$actualLang.'.php';
}

foreach ($LANG as $name => $trad) {
    switch (substr($name,0,4)) {
        case 'ttl_' : $mainTitles[] = substr($name,4);
        break;
        case 'btn_' : $buttons[] = substr($name,4);
        break;
        case 'tab_' : $tab[] = substr($name,4);
        break;
        case 'err_' : $errors[] = substr($name,4);
        break;
        case 'gen_' : $general[] = substr($name,4);
        break;
        case 'abt_' : $about[] = substr($name, 4);
        break;
        default : break;
    }
}


if($POST != null){
    $TEMP = getAboutTrads($actualLang);
    $LANG = filterLang($LANG);
    foreach ($POST as $key => $value){
        if($POST[$key] != null){
            $LANG[$key] = $value;
        }
    }
    foreach ($LANG as $name => $value) {
        unset($LANG[$name]);
        if (in_array($name, $mainTitles)) {
            $name = 'ttl_'.$name;
        }
        elseif (in_array($name, $buttons)) {
            $name = 'btn_'.$name;
        }
        elseif (in_array($name, $tab)) {
            $name = 'tab_'.$name;
        }
        elseif (in_array($name, $errors)) {
            $name = 'err_'.$name;
        }
        elseif (in_array($name, $about)) {
            $name = 'abt_'.$name;
        } else {
            $name = 'gen_'.$name;
        }
        if (in_array($name, $about)) {
            $LANG[$name]=$TEMP[$name];
        } else {
            $LANG[$name]=$value;
        }
    }
    writeLangFile($LANG, $actualLang);
    $VALID['update'] = $LANG['ttl_update_ok'];
}

$LANG=filterLang($LANG);

$base = new base();
$langHTML = new lang();

$base->base_header(['lang']);
$base->open_body($backLink, 'large');
$base->table("without-btn");

$langHTML->langChoice($actualLang, $allLangs);
$langHTML->langTitle();

$disabled = '';

if(!is_writable (__DIR__."/../../../config/lang")){
    $ERROR['message'] = $LANG['permissions'];
    $disabled = 'disabled';
}

if(isset($ERROR)){
    $base->displayError($ERROR);
}

if(isset($VALID)){
    $base->displayValid($VALID);
}

$langHTML->general($general, 'value');
$langHTML->mainTitles($mainTitles, 'value');
$langHTML->buttons($buttons, 'value');
$langHTML->tab($tab, 'value');
$langHTML->errors($errors, 'value', null, $disabled);
$base->table("end");
$base->close_body();
$base->base_footer(['lang']);

