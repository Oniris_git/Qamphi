<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:41
 */

use App\HTML\admin;
use App\HTML\base;

$base = new base();
$admin = new admin();

if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin'){
    header("Location: ?page=error&code=403");
}

$base->base_header(['main_index']);

$base->openFlexBody($backLink, "large");

$admin->adminMenu();

$base->close_body();

$base->footer($session->getUserData()->getRole());

$base->base_footer();
