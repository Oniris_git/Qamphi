<?php

use App\HTML\database;
use App\HTML\base;
use App\Install\installFunctions;
use PHPMailer\PHPMailer\Exception;

include __DIR__."/../ControllerFunctions/adminFunctions.php";
include __DIR__."/../ControllerFunctions/databaseFunctions.php";

if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin'){
    header("Location: ?page=error&code=403");
}

$base = new base();
$db = new database();
$install = new installHTML();
$installFunctions = new installFunctions();

$args = [
    'serverNameMoodle' => FILTER_SANITIZE_STRING,
    'loginBDDMoodle' => FILTER_SANITIZE_STRING,
    'passwordBDDMoodle' => FILTER_SANITIZE_STRING,
    'bddNameMoodle' => FILTER_SANITIZE_STRING,
    'typeBDDMoodle' => FILTER_SANITIZE_STRING,
    'hostSMTP' => FILTER_SANITIZE_STRING,
    'portSMTP' => FILTER_VALIDATE_INT,
    'usernameSMTP' => FILTER_SANITIZE_STRING,
    'passwordSMTP' => FILTER_SANITIZE_STRING,
    'certs' => FILTER_SANITIZE_STRING,
    'domain' => FILTER_SANITIZE_STRING,
    'radioMoodle' => FILTER_SANITIZE_STRING,
    'environment' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

$argsGet = [
    'dump' => FILTER_SANITIZE_STRING,
];
$GET = filter_input_array(INPUT_GET, $argsGet, false);

$configFile = getConfigToArray();

try{
    $configSMTP = [
        'host' => $config->getSMTP('host'),
        'port' => $config->getSMTP('port'),
        'username' => $config->getSMTP('username'),
        'password' => $config->getSMTP('password'),
        'certs' => $config->getSMTP('certs'),
    ];
    $domain = $configDAO->selectConfigByType('general', 'domain')['value'];
}catch(PDOException $e){
    $ERROR = [
        'message' => $e->getMessage()
    ];
}


//if user want to disable moodle
if(isset($POST['radioMoodle'])){
    try{
        $configDAO->updateConfig('auth', 'moodle', $POST['radioMoodle']);
        $SUCCESS = [
            'message' => $LANG['toast_valid']
        ];
        $moodle = $configDAO->selectConfigByType('auth', 'moodle')['value'];
    }catch(PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

//if user wants to change environment
if (isset($POST['environment'])) {
    try {
        $configDAO->updateConfig('general', 'environment', $POST['environment']);
    } catch (PDOException $e) {
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

// if (isset($POST['language'])) {
//     $configDAO->updateConfig('general', 'lang', $POST['language']);
// }

if(isset($POST['serverNameMoodle'])){
    $BDDMoodle = [
        'db_host' => $POST['serverNameMoodle'],
        'db_name' => str_replace(' ','',$POST['bddNameMoodle']),
        'db_user' => $POST['loginBDDMoodle'],
        'db_password' => $POST['passwordBDDMoodle'],
        'db_type' => $POST['typeBDDMoodle'],
    ];
    $testDB = $installFunctions->testDB($BDDMoodle);
    if ($testDB === true) {
        try{
            // $installFunctions->testDB($BDDMoodle);
            $configDAO->updateConfig('auth', 'moodle', "on");
            $moodle = $configDAO->selectConfigByType('auth', 'moodle')['value'];
            $BDD = [
                'db_host' => $configFile['db_host'],
                'db_name' => $configFile['db_name'],
                'db_user' => $configFile['db_user'],
                'db_password' => $configFile['db_password'],
                'db_type' => $configFile['db_type'],
            ];
            $installFunctions->createConfigFile($BDD, $BDDMoodle);
            $configFile = getConfigToArray();
            $SUCCESS = [
                'message' => $LANG['toast_valid']
            ];
        }catch(PDOException $e){
            $ERROR = [
                'message' => $e->getMessage()
            ];
        }
    } else {
        $ERROR['moodle'] = $testDB;
    }
}

if(isset($POST['hostSMTP'])){
    try{
        $configSMTP = [
            'host' => $POST['hostSMTP'],
            'port' => $POST['portSMTP'],
            'username' => $POST['usernameSMTP'],
            'password' => $POST['passwordSMTP'],
            'certs' => isset($POST['certs']) ? $POST['certs'] : 'off'
        ];
        $installFunctions->testSMTP($configSMTP);
        // foreach ($configSMTP as $key => $value) {
        //     $configDAO->updateConfig('smtp', $key, $value);
        // }
        $installFunctions->createSMTPconfigFile($configSMTP);
        $SUCCESS = [
            'message' => $LANG['toast_valid']
        ];
    }catch(Exception $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

if(isset($POST['domain'])){
    try{
        $configDAO->updateConfig('general', 'domain', $POST['domain']);
        $domain = $configDAO->selectConfigByType('general', 'domain')['value'];
    }catch(PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

if (isset($GET['dump'])) {
    $filename = exportDatabase($configFile, [
        'configDAO' => $configDAO,
        'langDAO' => $langDAO,
        'accessKeysDAO' => $accessKeysDAO,
        'statsDAO' => $statsDAO,
        'authorisationsDAO' => $authorisationsDAO,
        'userDAO' => $userDAO,
        'sessionDAO' => $sessionDAO,
        'questionTypesDAO' => $questionTypesDAO,
        'questionDAO' => $questionDAO,
        'proposalDAO' => $proposalDAO,
        'responseDAO' => $responseDAO,
    ]);

    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . basename($filename) . "\"");
    readfile($filename);

    die();
}

$environment = $configDAO->selectConfigByType('general','environment')['value'];

$base->base_header(['configuration']);
$base->open_body($backLink);
$base->table("without-btn");
$db->dbTitle();

$disabled = '';

if(!is_writable (__DIR__."/../../../config")){
    $ERROR['message'] = $LANG['permissions'];
    $disabled = 'disabled';
}

if(isset($ERROR)){
    $base->displayError($ERROR);
}elseif(isset($SUCCESS)){
    $base->displayValid($SUCCESS);
}

$db->environmentChoice($environment);
$db->export($disabled);

if (!isset($configFile['db_hostMoodle'])) {
    $configFile['db_hostMoodle'] = '';
}
if (!isset($configFile['db_userMoodle'])) {
    $configFile['db_userMoodle'] = '';
}
if (!isset($configFile['db_passwordMoodle'])) {
    $configFile['db_passwordMoodle'] = '';
}
if (!isset($configFile['db_nameMoodle'])) {
    $configFile['db_nameMoodle'] = 'moodle';
}
if (!isset($configFile['db_typeMoodle'])) {
    $configFile['db_typeMoodle'] = 'mysql';
}

$db->moodleDB($moodle, $configFile, $install);
$db->SMTPAccess($install, $configSMTP, $domain);
$install->validForm($disabled,'');

$base->table("end");
$base->close_body();
$base->base_footer(['database']);

echo"</html>";
