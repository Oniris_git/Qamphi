<?php

use App\HTML\admin\update;
use App\HTML\base;
use App\Install\installFunctions;

include __DIR__."/../ControllerFunctions/databaseFunctions.php";
include __DIR__."/../ControllerFunctions/adminFunctions.php";
include __DIR__."/../ControllerFunctions/updateFunctions.php";

$argsGet = [
    'dump' => FILTER_SANITIZE_STRING,
    'update' => FILTER_SANITIZE_STRING,
];
$GET = filter_input_array(INPUT_GET, $argsGet, false);

if (isset($GET['dump'])) {
    try {
        $filename = exportDatabase(getConfigToArray(), [
            'configDAO' => $configDAO,
            'langDAO' => $langDAO,
            'accessKeysDAO' => $accessKeysDAO,
            'statsDAO' => $statsDAO,
            'authorisationsDAO' => $authorisationsDAO,
            'userDAO' => $userDAO,
            'sessionDAO' => $sessionDAO,
            'questionTypesDAO' => $questionTypesDAO,
            'questionDAO' => $questionDAO,
            'proposalDAO' => $proposalDAO,
            'responseDAO' => $responseDAO,
        ]);
    } catch (PDOException $e) {
        $ERROR[] = $e->getMessage();
    }

    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . basename($filename) . "\"");
    readfile($filename);

    die();
}

if (isset($GET['update'])) {
    try {
        $filename = exportDatabase(getConfigToArray(), [
            'configDAO' => $configDAO,
            'langDAO' => $langDAO,
            'accessKeysDAO' => $accessKeysDAO,
            'statsDAO' => $statsDAO,
            'authorisationsDAO' => $authorisationsDAO,
            'userDAO' => $userDAO,
            'sessionDAO' => $sessionDAO,
            'questionTypesDAO' => $questionTypesDAO,
            'questionDAO' => $questionDAO,
            'proposalDAO' => $proposalDAO,
            'responseDAO' => $responseDAO,
        ]);
    } catch (Exception $e) {
        $ERROR[] = $e->getMessage();
    }

    $pdo = new PDO($connector['db_type'].":host=".$connector['db_host'].';dbname='.$connector['db_name'],
                    $connector['db_user'],
                    $connector['db_password']);

    try {
        updateDB($pdo);
        $configDAO->truncate('config');
        $langDAO->truncate('lang');
        $questionTypesDAO->truncate('question_types');
        $import = file_get_contents($filename);
        $pdo->exec($import);
        $configDAO->insertConfig('general', 'sql_version', $code_version);
    } catch (PDOException $e) {
        $ERROR[] = $e->getMessage();
    }

    header('Location: ?page=success');
    die();
}

$base = new base();
$update = new update();

$disabled = '';

$base->base_header();
$base->open_body($backLink);
$base->table("without-btn");
$update->updateTitle();

if(!is_writable (__DIR__."/../../../config")){
    $ERROR['message'] = $LANG['permissions'];
    $disabled = 'disabled';
}

if(isset($ERROR)){
    $base->displayError($ERROR);
}elseif(isset($SUCCESS)){
    $base->displayValid($SUCCESS);
}

$update->export($disabled);
$update->update($disabled);
$base->table("end");
$base->close_body();
$base->footer($session->getUserData()->getRole());
$base->base_footer();