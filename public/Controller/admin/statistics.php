<?php

use App\HTML\base;
use App\HTML\statistics;

$args = [
    'chart' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);
$base = new base();
$stats = new statistics();

$month = date('m');
$year = date('Y');

try {
    $month_stats = $statsDAO->stats_this_month();
    $all_stats = $statsDAO->all_stats();
} catch (PDOException $e) {
    $ERROR = [
        'message' => $e->getMessage()
    ];
}
$actions = [];
foreach ($all_stats as $stat) {
    foreach ($stat as $key => $value) {
        if ($key !== 'id'
            && $key !== 'month'
            && $key !== 'year'
            && array_search($key, $actions) === false) {
                $actions[$key]['datasets']['label'] = $LANG[$key];
                $actions[$key]['labels'][] = $LANG['month_'.$stat['month']];
                $actions[$key]['datasets']['data'][] = $stat[$key];
        }
    }
}

if (isset($POST['chart'])) {
    echo json_encode($actions);
    die();
}

$base->base_header();
$base->open_body($backLink, "large");
$base->table("without-btn");
$stats->statsTitle();
$stats->month_summary($month_stats);
$stats->all_stats($actions);
$base->table("end");
$base->close_body();
$base->base_footer(['charts']);