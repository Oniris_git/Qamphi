<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 15:09
 */

use App\BO\Proposal;
use App\BO\Questions\Question;
use App\HTML\base;
use App\HTML\edit_session;
use App\BO\Questions\QuestionTypeException;

include "ControllerFunctions/edit_sessionFunctions.php";

if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
    header("Location: ?page=error&code=403");
}elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin' && $session->getValue('role') != 'Teacher'){
    header("Location: ?page=error&code=403");
}

$vespulaSession = $session;

$questionsPOST = [];

$argsGet = [
    'id' => FILTER_VALIDATE_INT,
];
$GET = filter_input_array(INPUT_GET, $argsGet, false);
$argsPost = [
    'title' => FILTER_SANITIZE_STRING,
    'action' => FILTER_SANITIZE_STRING,
    'orderBy' => FILTER_SANITIZE_STRING,
    'idQuestion' => FILTER_SANITIZE_STRING,
    'order' => FILTER_VALIDATE_INT,
    'newSession' =>FILTER_SANITIZE_STRING,
    'tokenSession' =>FILTER_VALIDATE_INT,
    'anon' =>FILTER_SANITIZE_STRING,
    'pseudo' =>FILTER_SANITIZE_STRING,
    'moodle' =>FILTER_SANITIZE_STRING,
    'visibility' =>FILTER_SANITIZE_STRING,
    'keys' => FILTER_VALIDATE_INT,
    'total' => FILTER_VALIDATE_INT,
    'newQuestion' => FILTER_VALIDATE_INT,
    'type_id' => FILTER_VALIDATE_INT,
    'guest' => FILTER_VALIDATE_INT,
    'right' => FILTER_SANITIZE_STRING,
    'authorisation' => FILTER_VALIDATE_INT,
    'search' => FILTER_SANITIZE_STRING,
    'update_guests' => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $argsPost, false);
//If new session / redirect if id does not exist
if(isset($POST['newSession'])){
    try{
        $statsDAO->notify('session_create');;
        //insert new session (with 1 question and 5 proposals)
        if ($moodle === "on") {
            $auth = "anon,pseudo,moodle";
        } else {
            $auth = "anon,pseudo";
        }
        $id = $sessionDAO->insertSession($session->getValue('id'),'',$auth);
        $idSession = $id[0];
        $key = generateToken($sessionDAO, $accessKeysDAO);
        $sessionDAO->updateByField($idSession, 'token', $key);
    }catch (PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}else if(!isset($GET['id'])){
    header("Location: index.php?page=home");
}else{
    $idSession = $GET['id'];
}

try {
    $sessionByUser = $sessionDAO->selectSessionsByFilter("user_id", $session->getValue('id'));
    $shared = $sessionDAO->sharedWritingSessionsByUser($session->getValue('id'));
} catch (PDOException $e){
    log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
    die();
}

$sessionByUser = array_merge($sessionByUser, $shared);
$sessionIds = [];
foreach ($sessionByUser as $userSession) {
    $sessionIds[] = $userSession->getId();
}

if (!in_array($idSession, $sessionIds)) {
    header("Location: ?page=error&code=403");
}

//Check number of question to know how many post we got / parse int
try{
    $count = $questionDAO->countQuestionBySession($idSession);
    $countInt = intval($count);
}catch(PDOException $e){
    log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
    die();
}

if ($countInt === 0 && !isset($POST['newSession'])) {
    $ERROR = [
        $LANG['no_questions']
    ];
}

//if user change order of questions
if(isset($POST['orderBy'])){
    try{
        $idAutofocus = changeOrderQuestion($GET['id'],$questionDAO, $POST);
        if($idAutofocus == false){
            echo json_encode($idAutofocus);
            exit;
        }
    }catch (PDOException $e){
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }
}
//if user changed session title
if(isset($POST['title'])){
    try{
        $sessionDAO->updateSession($GET['id'], $POST['title']);
    }catch (PDOException $e){
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }
}

if (isset($POST['search'])) {
    $session_edit = new edit_session();
    $users = $authorisationsDAO->searchUserByNameNotGuest($POST['search'], $idSession);
    $output = $session_edit->open_guests_table($session->getValue('id'));
    foreach ($users as $user) {
        if ($user->getId()!= $session->getValue('id')) {
            $output .= $session_edit->add_guest($user, $GET['id']);
        }
    }
    $output .= $session_edit->close_guest_table();
    echo $output;
    die();
}

if (isset($POST['update_guests'])) {
    try {
        $guests = $authorisationsDAO->guestsBySession($idSession);
    } catch (PDOException $e) {
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }
    $session_edit = new edit_session();
    echo $session_edit->guests_list($guests, $idSession);
    die();
}

//Ajax call to new question form
if (isset($POST['newQuestion'])) {
    $questionArray = [
        'session_id' => $idSession,
        'q_order' => $countInt + 1,
        'q_text' => '',
        'type_id' => $POST['newQuestion']
    ];
    try {
        $questionForm = Question::instanciate($questionArray);
    } catch (QuestionTypeException $e) {
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }
    if (get_class($questionForm)::needsProposals()) {
            $questionForm->setProposals();
    }
    $questionForm->teacherForm($idSession, $countInt);
    die();
}

if (isset($POST['type_id'])) {

    try {
        $type = $questionTypesDAO->selectTypeById($POST['type_id']);
    } catch (PDOException $e) {
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }

    if ($type === null) {
        $output = $LANG['save_quit'];
    } else {
        $output = $LANG['add_'.$type->getName().'_question'];
    }

    echo $output;
    die();
}

if(isset($POST['tokenSession'])){
    try{
        $sessionDAO->updateToken($GET['id'], $POST['tokenSession']);
    }catch (PDOException $e){
        log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
        die();
    }
}

//traitment if user sent a request
if(isset($POST['action'])){
        //foreach question we filter the post inputs
        if (isset($POST['total']) && $POST['total'] >=0 && $POST['action']) {
            $countInt = $POST['total'];
        } else {
            $countInt = $countInt + 1;
        }
        for($i = 1 ;$i <= $countInt ; $i++){
            $argsQuestionsPost[$i] = [
                "Q".$i => FILTER_SANITIZE_STRING,
                "Q".$i."type" => FILTER_VALIDATE_INT,
                "QID".$i => FILTER_SANITIZE_STRING,
                "Q".$i."P1" => FILTER_SANITIZE_STRING,
                "Q".$i."C1" => FILTER_VALIDATE_INT,
                "Q".$i."P2" => FILTER_SANITIZE_STRING,
                "Q".$i."C2" => FILTER_VALIDATE_INT,
                "Q".$i."P3" => FILTER_SANITIZE_STRING,
                "Q".$i."C3" => FILTER_VALIDATE_INT,
                "Q".$i."P4" => FILTER_SANITIZE_STRING,
                "Q".$i."C4" => FILTER_VALIDATE_INT,
                "Q".$i."P5" => FILTER_SANITIZE_STRING,
                "Q".$i."C5" => FILTER_VALIDATE_INT,
            ];
            $questionsPOST[$i] = filter_input_array(INPUT_POST, $argsQuestionsPost[$i], false);
        }
        try{
            $files = $_FILES ?? null;
            postTreatment($questionsPOST, $questionDAO, $proposalDAO, $GET['id'], $POST, $vespulaSession, $sessionDAO, $accessKeysDAO, $responseDAO, $authorisationsDAO, $files);
        }catch (PDOException $e){
            log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
            die();
        }catch (QuestionTypeException $e) {
            log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
            die();
        }
}

//catch session to edit
try{
    $session = $sessionDAO->selectSessionsByFilter("id", $idSession)[0];
    $listQuestions = $questionDAO->selectQuestionBySession($session->getId());
    $count = count($listQuestions);
}catch (PDOException $e){
    log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
    die();
}

$base = new base();
$session_edit = new edit_session();

$base->base_header(['edit_session']);
//$session_edit->openForm($idSession);
$base->open_body($backLink);
$base->table('start-fluid', $from);

if(isset($ERROR)){
    $base->displayError($ERROR);
}

$session_edit->options_modal($session, $moodle, $accessKeysDAO->countKeysBySession($session->getId()), $authorisationsDAO->guestsBySession($idSession));

$session_edit->openForm($idSession);
$session_edit->sessionEdit($session, $accessKeysDAO->countKeysBySession($session->getId()));


//loop to display each question and each proposal by question
foreach ($listQuestions as $question) {
    if (get_class($question)::needsProposals()) {
        try {
            $listProposal = $proposalDAO->selectProposalByQuestion($question->getId());
            $question->setProposals($listProposal);
        } catch (PDOException $e) {
            log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
            die();
        }
    }
    $question->teacherForm($idSession, $count);
}

try {
    $session_edit->buttonAddQuestion($idSession, $questionTypes);
} catch (QuestionTypeException $e) {
    log_error($e, $logger, 'edit_session', $session->getUserdata(), $backLink);
    die();
}

$session_edit->buttonEdit_save_quit($idSession);
$base->table("end");
$base->close_body();
if(!isset($POST['orderBy'])) { //Strange bug where footer is duplicated when user change question order
    $base->footer($vespulaSession->getUserData()->getRole());
}
$base->base_footer(['edit_session']);
