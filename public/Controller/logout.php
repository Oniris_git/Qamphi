<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 17/05/2019
 * Time: 17:49
 */

if(isset($USER)){
    header('Location: http://moodle/login/logout.php?sesskey='.$USER->sesskey);
    exit;
}else{
    $auth->logout();
}


header("Location: ?page=main");