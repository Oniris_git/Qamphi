<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 11:22
 */

use App\HTML\base;
use App\HTML\login;
use PHPMailer\PHPMailer\PHPMailer;
use Vespula\Auth\Exception;
//use PHPMailer\PHPMailer\Exception;
use App\Session\Session;
use League\Container\Container;
use Vespula\Auth\Auth;
use App\DAL\MoodleAccountDAO;


require '../vendor/phpmailer/phpmailer/src/Exception.php';
require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/SMTP.php';
include "ControllerFunctions/loginFunctions.php";

global $LANG;
ob_start();

$base = new base();
$login = new login();

$args = [
    'username' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_STRING,
    'token' => FILTER_SANITIZE_STRING,
    'idSession' => FILTER_VALIDATE_INT,
    'emailReset' => FILTER_VALIDATE_EMAIL
];

$argsGet = [
    'idSession' => FILTER_VALIDATE_INT,
];

if ($auth->isValid()) {
    $auth->logout();
}

$POST = filter_input_array(INPUT_POST, $args, false);
$GET = filter_input_array(INPUT_GET, $argsGet, false);

if(isset($GET['idSession'])){
    $idSession = $GET['idSession'];
}else{
    $idSession = null;
}

//if get error / it is an error in credential
if(isset($GET['error'])){
    $ERROR = [
        'message' => "Error: Login/Password"
    ];
}

//user sent a request to reset password
if(isset($POST['emailReset']) && $POST['emailReset'] != null){
    try {
        $user = $userDAO->selectUserByUsername($POST['emailReset']);
    } catch (PDOException $e) {
        log_error($e, $logger, 'login', $session->getUserdata(), $backLink);
        die();
    }
    if($user != null && $user->getAuth() == "moodle"){
        $ERROR = [
            'message' => "En cas d'oubli de mot de passe pour un compte moodle, merci de prendre contact avec le service informatique"
        ];
    }else if(filter_var($POST['emailReset']) && $user != null){
        //we catch smtp params
        $dataSMTP = [
            'host' => $config->getSMTP('host'),
            'port' => $config->getSMTP('port'),
            'username' => $config->getSMTP('username'),
            'password' => $config->getSMTP('password'),
            'certs' => $config->getSMTP('certs'),
        ];
        //we set user to allow user to update password
        $token = generateToken();
        $user->setRequest(date("Y-m-d H:i:s"));
        $user->setToken($token);
        $user->setOpen(1);
        $userDAO->updateUser($user);
        $dirname = str_replace($_SERVER['DOCUMENT_ROOT'],'',dirname(__DIR__));
        $qamphi = str_replace('/public','',$dirname);
        $domain = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$qamphi;

        //we send mail to user
        try{
            $body = '<p>'.$LANG['mail_password_reset_body'].'</p><br>
        	    <a href = "'.$domain.'/public/index.php?page=resetPw&token='.$user->getToken().'">'.$LANG['mail_password_reset_link'].'</a>';

            sendMailSMTP($dataSMTP, $user, $LANG['mail_password_reset_subject'], $body);
            $VALID = [
                'message' => $LANG['email_send'].$POST['emailReset']
            ];
        }catch(Exception $e){
            $ERROR = [
                'message' => $e->getMessage()
            ];
        } catch (PDOException $e) {
            log_error($e, $logger, 'login', $session->getUserdata(), $backLink);
            die();
        }
    }else{
        $ERROR = [
            'message' => $LANG['email_error']
        ];
    }
//if user try to login
}else if(isset($POST['emailReset']) && $POST['emailReset'] == false && !isset($POST['username']) && !isset($POST['password'])){
    $ERROR = [
            'message' => $LANG['email_error']
        ];
}else if(isset($POST['username']) && isset($POST['password'])){
    $credentials = [
        'username' => $POST['username'],
        'password' => $POST['password'],
    ];
    //we check token
    if (!$session->verifyToken('login_admin', $POST['token'])) {
        $ERROR = [
            'message' => "Invalid Token"
        ];
    }else{
        //we try to log with values
        try{
            $result = $auth->login($credentials);
            $userQamphi = $userDAO->selectUserByUsername($credentials['username']);
            if($userQamphi != null){
                $session->setValue('id', $userQamphi->getId());
                $session->setValue('role', $userQamphi->getRole());
            }
            if ($userQamphi != null && $userQamphi->getRole() === 'undefined') {
                $ERROR = [
                    'message' => 'Error : Account disabled'
                ];
                $auth->logout();
            }
        }catch (\Vespula\Auth\Exception $e){
            if($e->getMessage() == 'false' && $moodle != "off"){

            }else{
                $ERROR = [
                    'message' => 'Error : User / Password'
                ];
            }
        } catch (PDOException $e) {
            log_error($e, $logger, 'login', $session->getUserdata(), $backLink);
            die();
        }
        if(!$auth->isValid() && $moodle == 'on'){
            $container = new Container();
            $container->add('session', new Session());
            $container->add('adapter', function () use ($connectorMoodle) {
                $moodle = new MoodleAccountDAO($connectorMoodle);
                return $moodle;
            });
            $session = $container->get('session');
            $adapter = $container->get('adapter');
            $auth = new Auth($adapter, $session);
            try{
                $result = $auth->login($credentials);
                $user = $userDAO->selectUserByUsername($credentials['username']);
                if($user != null && $user->getRole() != 'undefined'){
                    $session->setValue('id', $user->getId());
                    $session->setValue('role', $user->getRole());
                } else {
                    $ERROR = [
                        'message' => 'Votre compte Moodle n\'est pas relié à QAmphi !'
                    ];
                    $auth->logout();
                }
            }catch(Exception $e){
                $ERROR = [
                    'message' => $e->getMessage()
                ];
            } catch (PDOException $e) {
                log_error($e, $logger, 'login', $session->getUserdata(), $backLink);
                die();
            }
        }

    }
    //if user come from participate he got idSession in post
    try {
        if (isset($POST['idSession']) && $POST['idSession'] != null && $auth->isValid() == true) {
            $session = $sessionDAO->selectSessionsByFilter("id", $POST['idSession'])[0];
            header("Location: ?page=participate&username=" . $POST['username'] . "&tokenSession=" . $session->getToken() . "&qOrder=1");
            die();
            //if this user does not enter rights parameters
        } elseif (isset($POST['idSession']) && $POST['idSession'] != null && $auth->isValid() == false) {
            $session = $sessionDAO->selectSessionsByFilter("id", $POST['idSession'])[0];
            header("Location: ?page=authentication&tokenSession=" . $session->getToken() . "&error=login");
            die();
        }
    } catch (PDOException $e) {
        log_error($e, $logger, 'login', $session->getUserdata(), $backLink);
        die();
    }
}
if($auth->isValid()){
    if (strpos($from,'page=login') || !strpos($from,'page')) {
        if($session->getValue('role') == "Admin"){
            $from = "?page=admin";
        }else{
            $from = "?page=list_session";
        }
    }
    header('Location: '.$from);
    die();
}else{
    $token = $session->generateToken('login_admin');
}

$base->base_header(['login']);

$base->openFlexBody($backLink, "little");

$login->titleLogin();

if(isset($ERROR)){
    $base->displayError($ERROR);
}
if(isset($VALID)){
    $base->displayValid($VALID);
}

$login->login($token, $idSession, $moodle);

$base->close_body();

$base->base_footer();
