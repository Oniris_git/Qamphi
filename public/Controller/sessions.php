<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/03/2019
 * Time: 16:50
 */

use App\HTML\base;
use App\HTML\sessions;

$base = new base();
$sessions = new sessions();

if(isset($_GET['error'])){
    $ERROR = [
        'message' => $LANG['error_key']
    ];
}

$args = [
    'idSession' => FILTER_VALIDATE_INT,
];

$POST = filter_input_array(INPUT_POST, $args, false);

$base->base_header();

$base->openFlexBody($backLink, "large");
$base->table("flex", $from);

if(isset($POST['idSession'])){
    if (isset($session) && $auth->isAnon() == true || $session->getStatus() == 'IDLE'){
        header("Location: ?page=error&code=403");
    }elseif(isset($session) && $auth->isAnon() == false && $session->getValue('role') != 'Admin' && $session->getValue('role') != 'Teacher'){
        header("Location: ?page=error&code=403");
    }
    // $statsDAO->notify('session_played');
    try {
        $sessionDAO->updateByField($POST['idSession'], "active", 1);
        $sessionDAO->updateByField($POST['idSession'], "current", 0);
        $sessionToLaunch = $sessionDAO->selectSessionsByFilter("id", $POST['idSession']);
        $listQuestions = $questionDAO->selectQuestionBySession($POST['idSession']);
    } catch (PDOException $e) {
        log_error($e, $logger, 'sessions', $session->getUserdata(), $backLink);
        die();
    }
    if($listQuestions == null){
        $ERROR = [
            'message' => $LANG['no_questions']
        ];
    }
    if(isset($ERROR)){
        $base->displayError($ERROR);
    }
    $message = $sessionToLaunch[0]->getAccess() == 'public' ? $LANG['following_access_key'] : $LANG['private_access_key'];
    $sessions->sessionTitle($from, $message);
    $sessions->sessionToken($sessionToLaunch[0]);
}else{
    if(isset($ERROR)){
        $base->displayError($ERROR);
    }
    $sessions->sessionTitle($from, $LANG['access_key']);
    $sessions->sessionAccess();
}

$base->table("end");
$base->close_body();

$base->base_footer();
