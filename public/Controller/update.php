<?php

use App\HTML\base;
use App\HTML\update;

$update = new update();
$base = new base();

$base->base_header();

$base->openFlexBody($backLink, "little");

$update->updateNeeded();
$base->close_body();
$base->footer($session->getUserData()->getRole());
$base->base_footer();