<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 18/03/2019
 * Time: 15:52
 */

use App\BO\Response;
use App\HTML\base;
use App\HTML\participate;
use App\DAL\MoodleAccountDAO;

$base = new base();
$participate = new participate();

$moodleDAO = new MoodleAccountDAO($connectorMoodle);

$base->base_header(['confirm', 'participate']);
$base->open_form_container();
$argsGet = [
    'tokenSession' => FILTER_SANITIZE_STRING,
    'idSession' => FILTER_VALIDATE_INT,
    'qOrder' => FILTER_VALIDATE_INT,
    'username' => FILTER_SANITIZE_STRING,
    'anon' => FILTER_SANITIZE_STRING,
];
$GET = filter_input_array(INPUT_GET, $argsGet, false);

//TODO : implement get_expected_post() in question objects => return array of post args needed by question type
$args = [
    'idSession' => FILTER_VALIDATE_INT,
    'username' => FILTER_SANITIZE_STRING,
    'order' => FILTER_VALIDATE_INT,
    'idQuestion' => FILTER_VALIDATE_INT,
    'nextQuestion' => FILTER_VALIDATE_INT,
    'P1' => FILTER_VALIDATE_INT,
    'P2' => FILTER_VALIDATE_INT,
    'P3' => FILTER_VALIDATE_INT,
    'P4' => FILTER_VALIDATE_INT,
    'P5' => FILTER_VALIDATE_INT,
    'response' => FILTER_SANITIZE_STRING,
    'left' => FILTER_VALIDATE_FLOAT,
    'top' => FILTER_VALIDATE_FLOAT,
    'typeAuth' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $args, false);

if(!isset($POST['idSession']) && !isset($GET['idSession']) && !isset($GET['tokenSession'])){
    header("Location: index.php?page=sessions");
}


function validUsername($POST, $responseDAO){
    if ($POST == "anon"){
        do {
            $username = "Anon".rand(10000,99999);
            $check = $responseDAO->selectListResponsesByUser($username);
        } while ($check !== []);
        $_SESSION['username'] = $username;
    }else{
        $username = $POST;
        $_SESSION['username'] = $username;
    }
    return $username;
}


//catch session id
try {
    if(isset($GET['idSession'])){
        $idSession = $GET['idSession'];
    }else if(isset($GET['tokenSession'])){
        $sessionByToken = $sessionDAO->selectSessionsByFilter("token", $GET['tokenSession'], 1)[0];
        $idSession = $sessionByToken->getId();
    }else{
        $idSession = $POST['idSession'];
    }
}catch(PDOException $e){
    log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
    die();
}


//catch username by post or gestion / it depends of the origin of the user
if (isset($GET['anon'])) {
    try {
        $username = validUsername('anon', $responseDAO);
    } catch (PDOException $e) {
        log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
        die();
    }
}elseif(isset($POST['username'])){
    if (isset($POST['typeAuth']) && $POST['typeAuth'] == "moodle") {
        try {
            $user_exists = $moodleDAO->authenticate(["username" => $POST['username'], "password" => $POST['password']]);
        }catch (Exception $e) {
            header("Location: index.php?page=authentication&idSession=".$idSession."&error");
            exit();
        }
    }

    $username = validUsername($POST['username'], $responseDAO);

}elseif (isset($GET['username'])){
    try {
        $username = validUsername($GET['username'], $responseDAO);
    } catch (PDOException $e) {
        log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
        die();
    }
}elseif(isset($session) && $auth->isAnon() == false ){
    $username = $session->getValue('username');
}else{
    header("Location: ?page=error&code=403");
}

if(isset($POST['order']) && $POST['order'] == 0){
    //$sessionDAO->incrementCounter($POST['idSession']);
    header("Location: ?page=confirm&idSession=".$idSession."&nextQuestion=1&username=".$POST['username']."&waiting");
}


        //we select questions
        try{
            $listQuestion = $questionDAO->selectQuestionBySession($idSession);
            $countQuestion = count($listQuestion);
        }catch(PDOException $e){
            log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
            die();
        }

        $currentSession = $sessionDAO->selectSessionsByFilter('id', $idSession)[0];

        if (!isset($currentSession)) {
            header("Location: ?page=error&code=402");
        }

        $qOrder = $currentSession -> getCurrent();
        if ($qOrder <= 0) {
            header("Location: ?page=confirm&waiting&idSession=".$idSession."&username=".$username);
        }

        //if user navigate in questions
        if(isset($GET['idSession']) && isset($GET['qOrder'])){
            try{
                //this variable is used to know if user has already respond or not
                $firstShow = true;
                $questionToDisplay = $questionDAO->selectQuestionByOrder($GET['idSession'], $GET['qOrder']);
                //if user rewrite url with incorrect qOrder
                if (!isset($questionToDisplay)) {
                    header("Location: ?page=error&code=402");
                }
                $listResponseByUser = $responseDAO->selectListResponsesByUser($username);
                //check if user already respond to question / user can't change his response
                foreach ($listResponseByUser as $response){
                    if($response->getQuestionId()== $questionToDisplay->getId()){
                        $firstShow = false;
                    }
                }
                if($firstShow == false){
                    $responseToDisplay = $responseDAO->selectResponseByUser($username, $questionToDisplay->getId());
                    //$responseArray = explode("-", $responseToDisplay->getResponse());
                }
            }catch(PDOException $e){
                log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
                die();
            }

        }else{
            $firstShow = true;
            try{
                // //if user post response
                if(isset($POST['nextQuestion'])){
                    //if the question user is answering to is session current
                    if ($POST['nextQuestion']-1 == $currentSession->getCurrent()) {
                        $currentQuestion = $questionDAO->selectQuestionByOrder($currentSession->getId(), $currentSession->getCurrent());
                        try {
                            $reponse = $currentQuestion->constructResponse($POST);
                        } catch (Exception $e) {
                            header("Location: ?page=confirm&idSession=".$POST['idSession']."&username=".$POST['username']."&".$e->getMessage());
                            exit();
                        }
                        //construct response object
                        $reponse = [
                            'question_id' => $POST['idQuestion'],
                            'response' => $reponse,
                            'username' => $username
                        ];
                        $reponseObject = new Response($reponse);
                        $check = $responseDAO->selectResponseByUser($username, $POST['idQuestion']);
                        if ($check === null) {
                            $result = $responseDAO->insert($reponseObject);
                        }
                        header("Location: ?page=confirm&idSession=".$POST['idSession']."&username=".$POST['username']);
                    }
                    else {
                        header("Location: ?page=confirm&idSession=".$POST['idSession']."&username=".$POST['username']."&late");
                    }
                }else{
                    $qOrder = $currentSession -> getCurrent();
                    $questionToDisplay = $questionDAO->selectQuestionByOrder($currentSession->getId(), $qOrder);
                    $listResponseByUser = $responseDAO->selectListResponsesByUser($username);
                    //check if user already respond to question / user can't change his response
                    foreach ($listResponseByUser as $response){
                        if($response->getQuestionId()== $questionToDisplay->getId()){
                            $firstShow = false;
                        }
                    }
                    if ($qOrder <= 0 || $firstShow == false) {
                        header("Location: ?page=confirm&waiting&idSession=".$idSession."&username=".$username);
                    }
                    $questionToDisplay = $questionDAO->selectQuestionByOrder($idSession, $qOrder);

                    $questionToDisplay = $questionDAO->selectQuestionByOrder($idSession, $qOrder);


                }
            }catch (PDOException $e){
                log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
                die();
            }
        }

        if(isset($ERROR)){
            $base->displayError($ERROR);
        }

        if(isset($GET['qOrder'])){
            $qOrder = $GET['qOrder'];
        }else{
            $qOrder = $currentSession->getCurrent();
        }
        //redirect to correct question
        if ($qOrder < 0 && $currentSession->getCurrent() > 0) {
            $qOrder = $currentSession->getCurrent();
        }
        //if user has not respond to question we display question / we display response
        if($firstShow == true && $qOrder==$currentSession->getCurrent()){
            try{

                $questionToDisplay->displayHeader($countQuestion, $username);
                if (get_class($questionToDisplay)::needsProposals()) {
                    $listProposal = $proposalDAO->selectProposalNotEmpty($questionToDisplay->getId());
                    $questionToDisplay->setProposals($listProposal);
                }
                $questionToDisplay->displayForm($username);

            }catch(PDOException $e){
                log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
                die();
            }
        }else{
            try{
                $questionToDisplay->displayHeader($countQuestion, $username);
                $responseToDisplay = isset($responseToDisplay) ? $responseToDisplay : new Response([
                    'question_id' => $questionToDisplay->getId(),
                    'response' => '-0-0-0-0-0-',
                    'username' => $username
                ]);
                $questionToDisplay->displayResponse($responseToDisplay);
            }catch(PDOException $e){
                log_error($e, $logger, 'participate', $session->getUserdata(), $backLink);
                die();
            }
        }

$base->close_form_container();
$base->base_footer(['participate']);

