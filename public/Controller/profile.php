<?php

use App\HTML\base;
use App\HTML\profile;

$base = new base();
$profile = new profile();

$argsPost = [
    'newPassword' => FILTER_SANITIZE_STRING,
    'confirmNewPassword' => FILTER_SANITIZE_STRING,
    'email' => FILTER_VALIDATE_EMAIL,
    'currentPassword' => FILTER_SANITIZE_STRING,
];
$POST = filter_input_array(INPUT_POST, $argsPost, false);

$user = $session->getUserData();
$current_password = $user->getPassword();

if(isset($POST["currentPassword"]) && $user->getAuth() == 'manual'){
    $user->setEmail($POST['email']);
    if (trim($POST['newPassword']) !== "") {
        $check = strcmp ($POST['newPassword'], $POST['confirmNewPassword']);
        if($check != 0){
            $ERROR = [
                'message' => $LANG['password_not_equal']
            ];
        }else{
            $password = password_hash($POST['newPassword'], PASSWORD_DEFAULT);
            $user->setPassword($password);
            $user->setRequest(null);
            $user->setToken(null);
            $user->setOpen(0);
        }
    }

    if (password_verify($POST['currentPassword'], $current_password)) {
        try {
            $userDAO->updateUser($user);
        } catch (PDDOException $e) {
            log_error($e, $logger, 'reset', $session->getUserdata(), $backLink);
            die();
        }
        header('Location: index.php');
    } else {
        $ERROR = [
            'message' => 'Mauvais mot de passe'
        ];
    }
}

$base->base_header();
$base->open_body($backLink);
$base->table("without-btn");
$profile->profile_title();
if(isset($ERROR)){
    $base->displayError($ERROR);
}
if($user->getAuth() == 'moodle'){
    $base->displayWarning([
        $LANG['moodle_account']
    ]);
}
$profile->profile_form($user);
$base->close_body();