<?php

use App\HTML\base;
use App\HTML\error;

$base = new base();
$errorHTML = new error();

$args = [
    'code' => FILTER_VALIDATE_INT,
    'message' => FILTER_SANITIZE_STRING,
];


$GET = filter_input_array(INPUT_GET, $args, false);

$base->base_header();

$base->openFlexBody($backLink, 'middle');

if (isset($GET['code'])){
    switch($GET['code'])
    {
        case '400':
            $error = 'Erreur HTTP';
            break;
        case '401':
            $error =  'Mauvaise combinaison identifiant / mot de passe';
            break;
        case '402':
            $error =  'Erreur dans la requête';
            break;
        case '403':
            $error =  'Accès refusé';
            break;
        case '404':
            $error =  'Page introuvable';
            break;
        case '405':
            $error =  'Methode refusée';
            break;
        case '500':
            $error =  'Erreur interne';
            break;
        case '501':
            $error =  'Le serveur ne supporte pas le service';
            break;
        case '502':
            $error =  'Mauvaise passerelle';
            break;
        case '503':
            $error =  'Le service n\'est pas disponible';
            break;
        case '504':
            $error =  'La réponse est trop longue';
            break;
        case '505':
            $error =  'Version HTTP non supportée';
            break;
        default:
            $error =  $LANG['error'];
    }
}else{
    $error =  $LANG['error'];
}

$errorHTML->showError($error);


$base->close_body();

$base->base_footer();
