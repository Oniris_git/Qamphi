//text editor
function commande(nom, argument) {
    if (typeof argument === 'undefined') {
        argument = '';
    }
    switch (nom) {
        case "createLink":
            argument = prompt("Quelle est l'adresse du lien ?");
            break;
        case "insertImage":
            argument = prompt("Quelle est l'adresse de l'image ?");
            break;
    }
    // Exec command
    document.execCommand(nom, false, argument);
}

//function to update legal
function result(callback){
    var xhr = new getXMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 400 || xhr.status === 500){
            show_toast(1);
        }
    };
    var html = document.getElementById("editor").innerHTML;
    xhr.open("POST", "?page=legal", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("html=" + html);
}

//callback function
function sendHTML(sData){
    show_toast(0);
}
