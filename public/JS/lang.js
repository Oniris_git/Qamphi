//function to change arrow in lang file admin
function hideArrow(name){
    var nameTarget = '#arrow'+name;
    var target = $(nameTarget);
    if(target.attr('class') == 'fas fa-caret-down' || target.attr('class') == 'fas fa-caret-down collapsed'){
        target.removeClass('fas fa-caret-down').addClass('fas fa-caret-right');
        target.css('margin-top', '1rem');
    }else{
        target.removeClass('fas fa-caret-right').addClass('fas fa-caret-down');
        target.css('margin-top', '0');
    }
}

// Submit lang form on input change
function changeLang() {

    var form = document.getElementById('formLang')
    form.submit()

}