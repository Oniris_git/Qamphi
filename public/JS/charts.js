$(document).ready(function() {
    urlParams = new URLSearchParams(window.location.search);
    page = urlParams.get('page')
    switch (page) {
        case 'statistics':
            stats_page()
            break;
        case 'launch_session':
            poll_page()
            break;
        default:
            break;
    }


})

function poll_page() {
    canvas = document.getElementsByTagName('canvas')
    var chartColors = [
        'rgba(255, 0, 0, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(54, 162, 235, 1)',
    ]
    ;
    for (let i = 0; i < canvas.length; i++) {
        dataid = canvas[i].id.replace('chart', 'data')
        data = JSON.parse(document.getElementById(dataid).innerHTML)        
        datas = []
        datas[0] = dataid.replace('-data', '')
        datasetsdata = []
        labels = []
        colors = []
        j = 0
        Object.keys(data).forEach(key => {
            labels.push(key)
            datasetsdata.push(data[key])
            colors.push(chartColors[j])
            j++
          });
        datas[1] = {
            datasets: [{
                data: datasetsdata,
                backgroundColor: colors
            }],
            labels: labels
        }
        options =  {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'left',
                labels: {
                    fontSize: 40,
                    fontColor: 'black',
                    fontFamily: 'open',
                    padding: 20
                }
            },
        }, 
        chart = build_canva(datas, 'pie', options)
    }
}


function stats_page() {
    $.ajax({
        url : './../public/index.php?page=statistics',
        type : 'POST',
        data : 'chart',
        dataType : 'json',
        success: function(data) {     
            data = Object.entries(data)   
            datas = []
            data.forEach(element => {
                datas[0] = element[0]
                datas[1] = {
                    labels: element[1].labels,
                    datasets: [{
                        label: element[1].datasets.label,
                        data: element[1].datasets.data,
                        fill: false,  
                        borderColor: '#000000',
                    }],                    
                } 
                options =  {
                    elements: {
                        line: {
                            tension: 0
                        }
                    },
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                        }]
                    },
                },         
                chart = build_canva(datas, 'line', options)
            });        
        }
    })
}

function build_canva(datas, charttype, options) {
var ctx = document.getElementById(datas[0]+'-chart').getContext('2d');
        var chart = new Chart(ctx, {
            type: charttype,
            data: datas[1],
            options: options
        });
        return chart
}