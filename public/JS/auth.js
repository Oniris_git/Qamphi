/**
 *
 * @returns {xhr object}
 * use for ajax
 */
function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

//Ajax function to get input and send to php file
function userSearch(callback, type){
    var input = '#inputUsername'+type;
    var inputValue = $(input).val();
    if(inputValue.length >= 3){
        var xhr = getXMLHttpRequest();

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
                callback(xhr.responseText);
            }else if(xhr.status === 400 || xhr.status === 500){
                show_toast(1);
            }
        }
        xhr.open("POST", "?page=auth", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("moodleSearch=" + inputValue + "&typeAdd=" + type);
    }
}

//callback for user search / admin
function readUsersAdmin(sData){
    $('#moodleadminUsers').remove();
    document.getElementById('targetUsersAdmin').innerHTML = sData;
}

//callback for user search / teacher
function readUsersTeacher(sData){
    $('#moodleteacherUsers').remove();
    document.getElementById('targetUsersTeacher').innerHTML = sData;
}

//Ajax to add manual account
function newAdd(callback, username, type){
    var xhr = getXMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 400 || xhr.status === 500){
            show_toast(1);
        }
    };
    xhr.open("POST", "?page=auth", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("newAdd=" + username + "&typeAdd=" + type);

}

//callback admin list and display toast
function readAdmin(sData) {
    $('#tableListadmin').remove();
    show_toast(0);
    document.getElementById('targetListadmin').innerHTML = sData;
}

//callback teacher list
function readTeacher(sData) {
    $('#tableListteacher').remove();
    show_toast(0);
    document.getElementById('targetListteacher').innerHTML = sData;
}

//to display modal buttons
function displayButtons(type) {
    var name = ".accountType"+type;
    var target = $(name)[0].style;
    var target1 = $(name)[1].style;
    if(target.visibility == 'hidden'){
        target.visibility = 'visible';
        target1.visibility = 'visible';
    }else{
        target.visibility = 'hidden';
        target1.visibility = 'hidden';
    }
}

//set autofocus on first input
//call this function onclick of modal display button
function addAutofocus() {
    var modal = event.target.getAttribute("data-target")
    $(modal).on('shown.bs.modal', function () {
        $(modal).find('input').first().trigger('focus')
    })
}

function confirmDelete() {
   let message = document.getElementById('confirm_delete_user').value
   return confirm(message)
}