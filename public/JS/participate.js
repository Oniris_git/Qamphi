//function to change color and value of proposal in use
//encomment when unique choice questions are implemented
function validResponse(id , typeQuestion ){
    var pid = "P"+id;
    var btn = pid+"Btn";
    var element = document.getElementById(pid);
    if (typeQuestion == 2 || typeQuestion == 5) {
        for (let i = 1; i < 6; i++) {
            let tempValue = document.getElementById("P"+i)
            let tempBtn = document.getElementById("P"+i+"Btn")
            if (tempValue && tempValue != 0) {
                tempValue.value = 0
                tempBtn.style.backgroundColor = "#fff";
                tempBtn.style.color = "#444";
            }
        }
    }
    if(element.value == 0){
        element.value = id;
        document.getElementById(btn).style.backgroundColor = "#444";
        document.getElementById(btn).style.color = "#fff";
    }else{
        element.value = 0;
        document.getElementById(btn).style.backgroundColor = "#fff";
        document.getElementById(btn).style.color = "#444";
    }
}

//function to check if the user posted at least one response
function checkResponses() {
    // if (document.getElementById('P1') === null && document.getElementById('pos_x') === null) {
    //     return true
    // }
    let countResponses = 0
    for (let i = 1; i<6; i++) {
        let button = document.getElementById('P'+i)
        if (button !== null && button.value !=="0") {
            countResponses++
        }
    }
    var message = document.getElementById('warning_no_response').value
    pos_x = document.getElementById('pos_x')
    wcinput = document.getElementById('wc-input')
    if (wcinput !== null && wcinput.value === '') {
        return confirm(message)
    } else if (pos_x !== null && pos_x.value === '') {
        return confirm(message)
    } else if (pos_x === null && wcinput === null && countResponses === 0) {
        return confirm(message)
    } else {
        return true
    }
}

// Get user click position for clickmap questions
function get_click_position(e) {
    // e = Mouse click event.
    var el = e.target
    var rect = e.target.getBoundingClientRect();

    var x = e.clientX - rect.left; //x position within the element.
    var y = e.clientY - rect.top;  //y position within the element.
    div = document.createElement('div')
    div.id = 'tmp'
    div.style.width='1px'
    div.style.height='1px'
    pointer = document.createElement('i')
    pointer.classList.add('fas')
    pointer.classList.add('fa-2x')
    pointer.classList.add('fa-map-marker-alt')
    pointer.classList.add('icon-pointer')
    div.style.position = 'absolute'
    pointer.id='pointer'
    tmp = document.getElementById('tmp')
    if (tmp !== null && tmp !== undefined) {
        tmp.parentNode.removeChild(tmp)
    }
    div.appendChild(pointer)
    div.style.zIndex = 10
    e.target.parentNode.appendChild(div)
    width = $('#pointer').width()
    height = $('#pointer').height()

    pointer.style.zIndex = 8
    e.target.parentNode.style.position = 'relative'
    e.target.parentNode.style.zIndex = 0


    img_height = $('#img_question').height()
    img_width = $('#img_question').width()

    var container = document.getElementsByClassName('form-container')[0]

    percent_left = (x/img_width)*100
    percent_top = (y/img_height)*100

    div.style.left = percent_left+'%'
    div.style.top = percent_top+'%'

    document.getElementById('pos_x').value = percent_left
    document.getElementById('pos_y').value = percent_top
}