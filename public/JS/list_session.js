function savePreferences() {
    var form = document.getElementById('preferences_form')
    var formData = new FormData(form)

    $.ajax({
        url : './../public/index.php?page=list_session',
        type : 'POST',
        data : formData,
        processData: false,
        contentType: false,
        success: function() {
            window.location.reload()
        },
        error: function() {
            show_toast(1)
        }
    })
}