//prevent form submit by pressing enter key
document.addEventListener('keypress', function (e) {
    if (e.keyCode === 13 || e.which === 13) {
        e.preventDefault();
        return false;
    }
});

var keys = document.getElementById('session_access_keys').value

//---------------------------
// QUESTIONS GESTION FUNCTION
//---------------------------

//display new question form
function addQuestion(typeId, idSession) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+idSession,
        type : 'POST',
        data : 'newQuestion='+typeId,
        dataType : 'html',
        success: function(data) {
            block = document.getElementById('allBlock')
            $(data).appendTo(block)
        }
    })
    window.scrollTo(0,document.body.scrollHeight);
}

//dinamically update correct proposal icon if proposal input is empty or not
function enable() {
    var propId = event.target.id.split(/[A-Z]/)
    var propIcon = document.getElementById("iconeCheck"+propId[1]+"id"+(propId[2]-1))

    if (event.target.value != "") {
        if(propIcon.classList.contains('disabled-check')) {
            propIcon.classList.remove('disabled-check')
        }
        if (propIcon.classList.contains('fa-times')) {
            propIcon.style.color = '#ff5353'
        }

    } else if (event.target.value == "" ) {
        if (!propIcon.classList.contains('disabled-check')) {
            propIcon.classList.add('disabled-check')
        }
        if (propIcon.classList.contains('fa-check')) {
            propIcon.classList.remove('fa-check')
            propIcon.classList.add('fa-times')
            let btn = document.getElementById('check'+propId[1]+"id"+(propId[2]-1))
            btn.value = 0;
        }
    }
}

// Switch between unique/multiple choices questions
function changetype(){
    order = event.target.id.replace('allow_multiple_responses', '')
    type_input = document.getElementsByName('Q'+order+'type')[0]
    if (type_input.value == 1) {
        type_input.value = 2
    } else {
        type_input.value = 1
    }
    propChecks = document.querySelectorAll('[id^="iconeCheck'+order+'"]')
    firstCorrect = false
    for (let i = 0; i < propChecks.length; i++) {
        if (propChecks[i].classList.contains('fa-check')) {
            firstCorrect = propChecks[i]
            break
        }
    }
    if (firstCorrect !== false) {
        firstCorrect = firstCorrect.id.slice(-1)
        changeValue(order, Number(firstCorrect))
        changeValue(order, Number(firstCorrect))
    }
}

//function to change value and icon in proposal
function changeValue(idQ, id){
    var idToSelect = "check"+idQ+"id"+id;
    var iconeToSelect = "iconeCheck"+idQ+"id"+id;
    var check = document.getElementById(idToSelect);

    var prop = document.getElementById("Q"+idQ+"P"+(id+1))
    if (prop.value === "") {
        return false
    }

    if(check.value == 0){
        check.value = 1;
        document.getElementById(iconeToSelect).className = "fas fa-check";
        document.getElementById(iconeToSelect).style.color = "#32c032";
        document.getElementById(iconeToSelect).style.fontSize = "1.5em";
    }else {
        check.value = 0;
        document.getElementById(iconeToSelect).className = "fas fa-times";
        document.getElementById(iconeToSelect).style.color = "#ff5353";
        document.getElementById(iconeToSelect).style.fontSize = "1.5em";
    }
    questionType = document.getElementsByName('Q'+idQ+'type')[0]
    if (questionType.value == 2) {
        for (let i = 0; i < 5; i++) {
            tmpId = "check"+idQ+"id"+i
            tmpIcon = "iconeCheck"+idQ+"id"+i
            let tmpProp = document.getElementById("Q"+idQ+"P"+(i+1))
            if (tmpId != idToSelect && tmpProp.value != "" ) {
                document.getElementById(tmpId).value = 0;
                document.getElementById(tmpIcon).className = "fas fa-times";
                document.getElementById(tmpIcon).style.color = "#ff5353";
                document.getElementById(tmpIcon).style.fontSize = "1.5em";
            }
        }
    }
}


//function to change question order
function requestUp(idSession, qOrder, callback){
    order = qOrder
    var xhr = getXMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            console.log("error 500");
        }else if(xhr.status === 404){
            console.log("error 400");
        }
    };
    qOrder = "QID"+ (qOrder -1);
    //document.getElementById(qOrder).focus();
    var orderBy = "up";
    xhr.open("POST", "index.php?page=edit_session&id="+idSession+"#"+ qOrder, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("orderBy=" + orderBy + "&order=" + order);
}

//function to change question order
function requestDown(idSession, qOrder, callback){
    order = qOrder
    var xhr = getXMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
            callback(xhr.responseText);
        }else if(xhr.status === 500){
            console.log("error 500");
        }else if(xhr.status === 404){
            console.log("error 400");
        }
    };
    qOrder = "Q"+ (qOrder +1)+"P3";
    //document.getElementById(qOrder).focus();
    var orderBy = "down";
    xhr.open("POST", "index.php?page=edit_session&id="+idSession+"#"+ qOrder, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("orderBy=" + orderBy + "&order=" + order);
}

function displayTypeButtons() {
    let btn = document.querySelectorAll('[id^="questionTypeBtn"]')
    for (let i = 0; i < btn.length; i++) {
        target = btn[i].style
        if(target.visibility == 'hidden'){
            target.visibility = 'visible';
        }else{
            target.visibility = 'hidden';
        }
    }

}

//save previous questions and add form in DOM - AJAX
function saveAndAdd(idSession, typeId) {
    let questions = document.querySelectorAll('[id^="QID"]')
    let lastId = questions.length
    let lastsProposals = document.querySelectorAll('[id^="Q' + lastId + 'P"]')

    if (typeof document.getElementsByName('Q'+lastId+'type')[0] == "undefined") {
        var lastType = -1
    } else {
        var lastType = document.getElementsByName('Q'+lastId+'type')[0].value
    }
    let notEmpty = 0
    for (let i = 0; i < lastsProposals.length; i++) {
        if (lastsProposals[i].value != '') {
            notEmpty++
        }
    }

    //if (notEmpty >= 2 || lastId == 0 || lastType > 1) {
    if (lastId == 0 || !((lastType <= 2 || lastType == 5) && notEmpty < 2)) {
        var form = document.getElementById('formSession')
        var formData = new FormData(form)
        formData.append('action', 'save_stay')
        formData.append('total', lastId)
        $.ajax({
            url : './../public/index.php?page=edit_session&id='+idSession,
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success: function() {
                updateProposals()
                addQuestion(typeId, idSession)
                updateArrows(idSession)
            }
        })
    } else {
        alert('Vous devez renseigner au moins 2 propositions')
    }

}

function updateArrows(idSession) {
    let arrows = document.getElementsByClassName('order-arrow')
    let questions = document.querySelectorAll('[id^="QID"]')

    for (var i = 0, len = arrows.length; i < len; i++) {
        let order = arrows[i].closest('#questionBlock').getAttribute('name')
        arrows[i].style = ''
        if (arrows[i].classList.contains('fa-caret-up')) {
            if (order == 1) {
                arrows[i].style = 'color: transparent'
            } else {
                arrows[i].parentNode.onclick = function(){requestUp(idSession, order, readData)}
            }
        } else {
            if (order == questions.length) {
                arrows[i].style = 'color: transparent'
            } else {
                arrows[i].parentNode.onclick = function(){requestDown(idSession, order, readData)}
            }
        }
    }

}

//update proposals order
function updateProposals() {
    let questions = document.querySelectorAll('[id^="QID"]')
    for (let i = 1; i <= questions.length; i++) {
        let proposals = document.querySelectorAll('[id^="Q' + i + 'P"]')
        let values = []

        for (let j = 0; j < proposals.length; j++) {
            if (proposals[j].value != '') {
                values.push(proposals[j].value)
                proposals[j].value = ''
            }
        }

        for (let j = 0; j < proposals.length; j++) {
            if (values[0] != null) {
                proposals[j].value = values[0]
            }
            values.shift()
        }

    }
}

//-----------------------
// Question display infos
//-----------------------

function question_info(type_id, session_id) {

    $.ajax({
        url : './../public/index.php?page=edit_session&id='+session_id,
        type : 'POST',
        data : 'type_id='+type_id,
        dataType : 'html',
        success: function(data) {
            container = document.getElementById('question-info-container')
            container.innerHTML = data
        }
    })

}

function empty_question_info() {
    container = document.getElementById('question-info-container')
    container.innerHTML = ''
}

//--------------------------
// GUESTS HANDLING FUNCTIONS
//--------------------------

function search_user(session_id) {
    input = document.getElementById('search_user_input')
    search = input.value

    if (search.length >= 3) {
        $.ajax({
            url : './../public/index.php?page=edit_session&id='+session_id,
            type : 'POST',
            data : 'search='+search,
            dataType : 'html',
            success: function(data) {
                container = document.getElementById('search-container')
                container.innerHTML = data
            }
        })
    }
}

function add_guest(guest, session_id) {
    data = 'action=add_guest&guest='+guest

    form = document.getElementById('sessions_options_form')
    right = document.querySelector('input[name="right-'+guest+'"]:checked').value
    data = data+'&right='+right


    $.ajax({
        url : './../public/index.php?page=edit_session&id='+session_id,
        type : 'POST',
        data : data,
        dataType : 'html',
        success: function(data) {
            tr = document.getElementById('tr-'+guest)
            tr.remove()
            show_toast(0)
            update_guests(session_id)
            empty_search_container()
        }
    })
}

function delete_guest(guest, session_id) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+session_id,
        type : 'POST',
        data : 'action=delete_guest&guest='+guest,
        dataType : 'html',
        success: function(data) {
            show_toast(0)
            update_guests(session_id)
            search_user(session_id)
        }
    })
}

function update_guests(session_id) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+session_id,
        type : 'POST',
        data : 'update_guests',
        dataType : 'html',
        success: function(data) {
            container = document.getElementById('guests_container')
            container.innerHTML = data
        }
    })
}

function update_rights(guest, session_id) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+session_id,
        type : 'POST',
        data : 'action=update_rights&guest='+guest,
        dataType : 'html',
        success: function(data) {
            show_toast(0)
        }
    })
}

function empty_search_container() {
    input = document.getElementById('search_user_input')
    input.value = ''
    container = document.getElementById('search-container')
    container.innerHTML = '';
}



//-----------------------
// OPTIONS MODAL FUNCTION
//-----------------------

//add private keys parameters options if private is checked
function changeModal(sessionId) {
    radio = event.target


    if (radio.value=='private') {

        createVisibilityPreferences(sessionId, keys);
        let auths = document.getElementById('auths')
        auths.style.display = 'none'

    } else {
        modal = document.getElementById('visibility-container')
        div = document.getElementById('visibility-group')
        modal.removeChild(div)
        let auths = document.getElementById('auths')
        auths.style.display = 'block'
    }
}

//insert visibility preferences DOM elements
function createVisibilityPreferences (sessionId, keys) {

    modal = document.getElementById('visibility-container')

    keyTrad=document.getElementsByName('keyTrad')[0]
    csvTrad=document.getElementsByName('csvTrad')[0]

    visgroup = document.getElementById('visibility-group')

    if (visgroup !== null) {
        visgroup.parentNode.removeChild(visgroup)
    }

    div = document.createElement('div')
    div.id = 'visibility-group'
    div.classList.add('form-inline')

    label = document.createElement('label')
    label.htmlFor = 'accessKeys'
    label.innerHTML = keyTrad.value+' :'
    label.classList.add('col-4')
    div.appendChild(label)

    input = document.createElement('input')
    input.type = 'number'
    input.classList.add('form-control')
    input.classList.add('col-4')
    keys = document.getElementById('session_access_keys')
    input.value = keys.value
    input.id = 'accessKeys'
    input.name = 'keys'
    div.appendChild(input)

    csv = document.createElement('button')
    csv.id = 'generateCSV'
    csv.innerHTML = csvTrad.value
    csv. classList.add('btn')
    csv. classList.add('btn-outline-secondary')
    csv. classList.add('col-4')
    csv.onclick = function (){exportToCSV(sessionId, input.value)};
    div.appendChild(csv)

    keysDiv = document.createElement('div')
    keysDiv.id = 'keys-group'
    div.appendChild(keysDiv)

    iframe = document.createElement('i')
    iframe.classList.add('fas')
    iframe.classList.add('fa-caret-right')
    iframe.style.cursor = 'pointer'
    iframe.innerHTML = " "+document.getElementsByName('showKeysTrad')[0].value
    iframe.onclick = function(){showKeys(sessionId)}
    keysDiv.appendChild(iframe)

    modal.appendChild(div)
}

//function to check if at least one auth mode is checked
function checkAuths(e) {
    var anon = document.getElementById("anon")
    var pseudo = document.getElementById("pseudo")
    var moodle = document.getElementById("moodle")

    auth = document.getElementsByName('authTrad')[0]

    if (!anon.checked && !pseudo.checked && !moodle.checked) {
        alert(auth.value)
        e.preventDefault()
    } else {
        return true
    }
}

//display or not visibility preferences on modal open
function initModal(sessionId) {
    private = document.getElementById('private')
    public = document.getElementById('public')
    div = document.getElementById('visibility-group')
    if (private.checked==true && div === null) {
        createVisibilityPreferences(sessionId, keys)
    }
}

//call PHP script to export to CSV (ajax)
function exportToCSV(sessionId, nmber) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+sessionId,
        type : 'POST',
        data : 'action=exportCSV&keys='+nmber,
        dataType : 'html',
        success: function() {
            $('#sessionPreferences').on('hide.bs.modal', function () {
                deleteCSV(sessionId)
            });
            keys = document.getElementById('session_access_keys').value = document.getElementById('accessKeys').value
            sessionId = window.location.search.replace('?page=edit_session&id=','')
            path = (document.location.origin+document.location.pathname).replace('index.php','tmp/session')

            window.location.href=path+sessionId+'.csv'
        }
    })
    event.preventDefault()
}

//function to delete csv file (called on modal close)
function deleteCSV(id) {
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+id,
        type : 'POST',
        data : 'action=deleteCSV'
    })
}

//save/get private access keys and display them - AJAX
function showKeys(id) {
    arrow = event.target
    group = document.getElementById('keys-group')
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+id,
        type : 'POST',
        data : 'action=show_keys&keys='+document.getElementById('accessKeys').value,
        dataType : 'json',
        success: function(data) {
            if (arrow.classList.contains('fa-caret-right')) {
                arrow.classList.remove('fa-caret-right')
                arrow.classList.add('fa-caret-down')
                ul = document.createElement('ul')
                ul.id = 'keys-list'
                group.appendChild(ul)
                for (let i = 0; i < data.length; i++) {
                    let key = data[i].token
                    let li = document.createElement('li')
                    li.innerHTML = key
                    ul.appendChild(li)
                }
            } else if(arrow.classList.contains('fa-caret-down')) {
                arrow.classList.remove('fa-caret-down')
                arrow.classList.add('fa-caret-right')
                group.removeChild(document.getElementById('keys-list'))
            }
            keys = document.getElementById('session_access_keys').value = document.getElementById('accessKeys').value
        }
    })
}

//warn if private -> public and private keys still exist
function warn() {

    confirmMsg = document.getElementsByName('confirmMsgKeys')[0]
    keys = document.getElementById('session_access_keys').value

    if (keys != 0) {
        return confirm('/!\\  ' + keys + confirmMsg.value)
    }
    return true
}

//save sessions preferences - AJAX
function saveOptions(idSession) {
    checkAuths()

    var form = document.getElementById('sessions_options_form')
    var formData = new FormData(form)
    formData.append('action', 'options')
    $.ajax({
        url : './../public/index.php?page=edit_session&id='+idSession,
        type : 'POST',
        data : formData,
        processData: false,
        contentType: false,
        success: function() {
            var private = document.getElementById('private').checked
            if (private) {
                keys = document.getElementById('session_access_keys').value = document.getElementById('accessKeys').value
            } else {
                keys = document.getElementById('session_access_keys').value = 0
            }
            show_toast(0)
        },
        error: function() {
            show_toast(1)
        }
    })
}