//function to disable moodle in install
function disableMoodle(){
    var radioButton = document.getElementById('radioMoodle');
    var moodle = $('.disableMoodle');
    if(radioButton.value == 'on'){
        radioButton.value = 'off';
        radioButton.checked = true;
        for(var i = 0; i < 4; i++){
            moodle[i].disabled=true;
        }
    }else{
        radioButton.value = 'on';
        radioButton.checked = false;
        for(var i = 0; i < 4; i++){
            moodle[i].disabled=false;
        }
    }

}