//color picker functions
//not used but easy to integrate

var colorPicker = new iro.ColorPicker("#color-picker-container", {
    borderWidth: 2,
    layout: [
        {
            component: iro.ui.Wheel,
            options: {
                borderColor: '#777777',
                borderWidth: 1
            }
        },
        {
            component: iro.ui.Slider,
            options: {
                borderColor: '#777777',
                borderWidth: 1
            }
        }
    ],
    width: 300,
    height: 300,
    color: "#fdfeff"
});


function onColorChange(color, changes) {
    var colorSelected = color.hexString;

    changeColor(colorSelected, readColor);
}

colorPicker.on('color:change', onColorChange);

function changeColor(colorSelected, callback){
    var xhr = getXMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)){
            callback(xhr.responseText);
        }
    };

    $(".colorContainer").css("background-color", colorSelected);
    xhr.open("POST", "?page=theme", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("colorSelected="+colorSelected);

}

function readColor(sData){
    //$("#cible").replaceWith(sData);
}


$('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
});
