$(function () {
  $('[data-toggle="popover"]').popover()
})


/**
 *
 * @returns {xhr object}
 * use for ajax
 */
function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Your browser does not support XMLHTTPRequest.");
        return null;
    }
    return xhr;
}

/**
 * @param sData
 * callback function to send html block to view
 */
function readData(sData) {
    if(sData == "false"){
        show_toast(2);
    }else{
        $('#global').remove();
        var target = document.getElementById("cible");
        target.innerHTML = sData;
    }

}

//Get question stats html response - AJAX
function modalQuestion(s_id, id) {
    $.ajax({
        url : './../public/index.php?page=launch_session',
        type : 'POST',
        data : 'idSession='+s_id+'&request=response&questionStatId='+id,
        dataType : 'html',
        success: function(data) {
            modal = document.getElementsByClassName('modal-body-'+id)[0]
            modal.innerHTML = data
        }
    })
}