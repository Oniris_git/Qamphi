$(document).ready(function() {
    getProgression()

});

function getProgression() {
    s_id = document.getElementById('session_id').value
    order = document.getElementById('question_order').value
    $.ajax({
        url : './../public/index.php?page=launch_session',
        type : 'POST',
        data : 'progression&idSession='+s_id+'&order='+order,
        dataType : 'json',
        success: function(data) {
            span1 = document.getElementById('nbr-responses')
            span2 = document.getElementById('nbr-participants')

            if (data.responses > data.total) {
                data.total = data.responses
            }

            span1.innerHTML = data.responses
            span2.innerHTML = data.total



            setTimeout(() => { getProgression() }, 1000);
        }
    })
}


// Switch between advanced and normal stats
function showAdvancedStats(sId) {
    var generals = document.getElementById('generalStats')
    var advanced = document.getElementById('advancedBlock')
    var score = document.getElementById('user-scores')
    switch (event.target.value) {
        case 'on':
            event.target.value = 'off'
            event.target.checked = false
            generals.style.display = 'none'
            if (score !== null) {
                score.style.display = 'none'
            }
            if (advanced !== null) {
                advanced.style.display = ''
            } else {
                getAdvancedStats(sId)
            }
            break;
        case 'off':
            event.target.value = 'on'
            event.target.checked = true
            generals.style.display = ''
            if (score !== null) {
                score.style.display = ''
            }
            if (advanced !== null) {
                advanced.style.display = 'none'
            }
            break;
        default:
            break;
    }
}

// Get questions stats and display them
function getAdvancedStats(sId) {
    block = document.getElementById('statsBlock')

    div = document.createElement('div')
    div.id = 'loading'
    div.classList.add('centerStats')
    block.prepend(div)

    icon = document.createElement('i')
    icon.classList.add('fas')
    icon.classList.add('fa-spinner')
    icon.classList.add('fa-pulse')
    icon.classList.add('fa-3x')
    div.appendChild(icon)

    $.ajax({
        url : './../public/index.php?page=launch_session',
        type : 'POST',
        data : 'idSession='+sId+'&request=response&sessionStatId='+sId,
        dataType : 'html',
        success: function(data) {
            advancedBlock = document.createElement('div')
            advancedBlock.id = 'advancedBlock'
            block.prepend(advancedBlock)

            advancedStats = document.createElement('div')
            advancedStats.id = 'advancedStats'
            advancedStats.innerHTML = data

            advancedBlock.appendChild(advancedStats)
            block.removeChild(div)
            poll_page()
        }
    })
}