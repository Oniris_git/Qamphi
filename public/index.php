<?php

require '../vendor/autoload.php';
require "../app/dependencies.php";
require "../HTML/base.php";

use App\DAL\AccessKeysDAO;
use App\DAL\SessionDAO;
use App\DAL\ProposalDAO;
use App\DAL\QuestionDAO;
use App\DAL\ResponseDAO;
use App\DAL\ConfigDAO;
use App\DAL\QuestionTypeDAO;
use App\DAL\StatisticsDAO;
use App\DAL\UserDAO;
use App\DAL\AuthorisationDAO;

require_once 'Controller/ControllerFunctions/errorFunctions.php';

//instanciation DAL
$sessionDAO = new SessionDAO($connector);
$proposalDAO = new ProposalDAO($connector);
$questionDAO = new QuestionDAO($connector);
$responseDAO = new ResponseDAO($connector);
$userDAO = new UserDAO($connector);
$configDAO = new ConfigDAO($connector);
$accessKeysDAO = new AccessKeysDAO($connector);
$questionTypesDAO = new QuestionTypeDAO($connector);
$statsDAO = new StatisticsDAO($connector);
$authorisationsDAO = new AuthorisationDAO($connector);

global $LANG;

$args = [
    "page" => FILTER_SANITIZE_STRING
];
$GET = filter_input_array(INPUT_GET, $args, false);

if (!isset($session)) {
    $page = 'logout';
} else {
    try {
        $questionTypes = $questionTypesDAO->selectAllTypes();
    } catch (PDOException $e) {
        log_error($e, $logger, 'index', $session->getUserdata(), $backLink);
        die();
    }
}

//used to redirect to page - 1
if(isset($_SERVER['HTTP_REFERER'])){
    $from = $_SERVER['HTTP_REFERER'];
}else{
    $from = '?page=main';
}

//rooting
if(isset($GET['page'])):
    $page = $GET['page'];
    try {
        if ($auth->isAnon() || $session->getUserdata() === null) {
            $canAccess = ['main', 'login', 'sessions', 'about','legal', 'lang_choice', 'authentication', 'participate', 'confirm', 'success', 'error', 'resetPw', 'logout', 'update'];
            if (!in_array($page, $canAccess)) {
                $from = $page;
                $page = 'login';
            }
        }
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        header('Location: index.php?page=login');
        die();
    }
    if ($page != 'lang' && $page != 'add_lang') {
        $LANG = filterLang($LANG);
    }

    // Check if update is needed

    $code_version = $config->getVersion();
    try {
        $sql_version = $configDAO->selectConfigByType('general', 'sql_version')['value'];
    } catch (PDOException $e) {
        log_error($e, $logger, 'index', $session->getUserdata(), $backLink);
        die();
    }

    if (($sql_version == null || $code_version != $sql_version)
        && ($page != 'login'
            && $page != 'logout'
            && $page != 'about'
            && $page != 'legal'
            && $page != 'lang_choice'
            && $page != 'main'))
        {
            $page = 'update';
        }

    switch ($page){
        case 'main':
            if ($auth->isAnon() == false
                && $session->getUserdata() !== null
                && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/admin.php";
                include "Controller/admin/admin.php";
                exit;
            } elseif ($auth->isAnon() == false
                && $session->getUserdata() !== null
                && $session->getValue('role') == 'Teacher'){
                $from = "";
                require "../HTML/list_session.php";
                include "Controller/list_session.php";
                exit;
            } else {
                require "../HTML/main.php";
                include "Controller/main.php";
                exit;
            }
        case 'login':
            require "../HTML/login.php";
            include "Controller/login.php";
            exit;
        case 'resetPw':
            $from = '?page=main';
            require "../HTML/reset.php";
            include "Controller/reset.php";
            exit;
        case 'success':
            $from = 'install/install.php';
            require "../HTML/success.php";
            include "Controller/success.php";
            exit;
        case 'logout':
            include "Controller/logout.php";
            exit;
        case 'profile':
            require "../HTML/profile.php";
            include "Controller/profile.php";
            exit;
        case 'about':
            $from = '?page=main';
            require "../HTML/about.php";
            include "Controller/about.php";
            exit;
        case 'list_session':
            if($auth->isAnon() == false && $session->getStatus() == 'IDLE' || $session->getValue('role') != 'Admin'){
                 $from = "";
            }else{
                $from = "?page=admin";
            }
            if( ($auth->isAnon() == false && $session->getValue('role') == 'Admin')
            || ($auth->isAnon() == false && $session->getValue('role') == 'Teacher')){
                require "../HTML/list_session.php";
                include "Controller/list_session.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case 'edit_session':
            $from = '?page=list_session';
            if(($auth->isAnon() == false && $session->getValue('role') == 'Admin')
            || ($auth->isAnon() == false && $session->getValue('role') == 'Teacher')){
                require "../HTML/edit_session.php";
                include "Controller/edit_session.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case 'authentication':
            $from = '?page=main';
            require "../HTML/authentication.php";
            require "../HTML/login.php";
            include "Controller/authentication.php";
            exit;
        case 'sessions':
            if(substr($from,-7) == "sessions"){
                if(($auth->isAnon() == false && $session->getValue('role') == 'Admin')
                || ($auth->isAnon() == false && $session->getValue('role') == 'Teacher')){
                    $from = "?page=list_session";
                }else{
                    $from = '?page=main';
                }
            }else if(isset($_GET['error'])){
                $from = "?page=main";
            }
            require "../HTML/sessions.php";
            include "Controller/sessions.php";
            exit;
        case 'participate':
            require "../HTML/participate.php";
            include "Controller/participate.php";
            exit;
        case'launch_session':
            if($auth->isAnon() == false && $session->getStatus() == 'IDLE'
            || $session->getValue('role') != 'Admin' || $session->getValue('role') != 'Teacher'){
                if(substr($from,-12) == "list_session"){
                    $from = "?page=list_session";
                }else if(substr($from,-14) == "launch_session"){
                    $from = $from;
                }else{
                    $from = "?page=sessions";
                }
            }else{
                $from = "?page=list_session";
            }
            require "../HTML/launch_session.php";
            require "../HTML/main.php";
            include "Controller/launch_session.php";
            exit;
        case'admin':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/admin.php";
                include "Controller/admin/admin.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'legal':
                require "../HTML/admin/legal.php";
                include "Controller/admin/legal.php";
                exit;
        case'install':
            require "install/installHTML.php";
            require "install/installFunctions.php";
            include "install/install.php";
            exit;
        case'theme':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/theme.php";
                include "Controller/admin/theme.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'lang':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/lang.php";
                include "Controller/admin/lang.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'add_lang':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/lang.php";
                include "Controller/admin/add_lang.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'database':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "install/installHTML.php";
                require "install/installFunctions.php";
                require "../HTML/admin/database.php";
                include "Controller/admin/database.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'auth':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/auth.php";
                include "Controller/admin/auth.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'statistics':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/statistics.php";
                include "Controller/admin/statistics.php";
                exit;
            } else {
                header("Location: ?page=error&code=403");
            }
        case'error':
            require "../HTML/error.php";
            include "Controller/error.php";
            exit;
        case'confirm':
            require "../HTML/confirm.php";
            include "Controller/confirm.php";
            exit;
        case'lang_choice':
            require "../HTML/lang_choice.php";
            include "Controller/lang_choice.php";
            exit;
        case 'update':
            if ($auth->isAnon() == false && $session->getValue('role') == 'Admin'){
                require "../HTML/admin/update.php";
                include "Controller/admin/update.php";
                exit;
            } else {
                require "../HTML/update.php";
                include "Controller/update.php";
                exit;
            }
            exit;
        default:
            require "../HTML/error.php";
            include "Controller/error.php";
            exit;
    }
else:
    $LANG = filterLang($LANG);
    require "../HTML/main.php";
    include "Controller/main.php";
    exit;
endif;

