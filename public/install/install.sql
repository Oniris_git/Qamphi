SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET NAMES 'utf8mb4';
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS = 0;


-- --------------------------------------------------------

--
-- `config` table structure
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(150) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

--
-- `config` data dumping
--

INSERT INTO `config` (`id`, `type`, `name`, `value`) VALUES
(1, 'view', 'legal', 'Ce texte représente les mentions légales de l&#39;application. Il sera à paramétrer par vos soins dans les réglages administration.'),
(2, 'view', 'background', '../public/image/bg2.jpg'),
(3, 'auth', 'moodle', '');

-- --------------------------------------------------------

--
-- `lang` table structure
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

--
-- `lang` data dumping
--

INSERT INTO `lang` (`id`, `name`, `content`) VALUES
(NULL, 'fr_FR', 'Français'),
(NULL, 'en_EN', 'English');


-- --------------------------------------------------------

--
-- `users` table structure
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(128) NOT NULL,
    `email` varchar(128) NOT NULL,
    `auth` varchar(10) NOT NULL,
    `role` varchar(10) NOT NULL,
    `password` varchar(255) DEFAULT NULL,
    `request` timestamp NULL DEFAULT NULL,
    `open` int(1) DEFAULT NULL,
    `token` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- `sessions` table structure
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(8) NOT NULL,
    `title` varchar(128) NOT NULL,
    `active` tinyint(1) NOT NULL,
    `participants` int(4) DEFAULT 0 COMMENT 'This field is incremented when user inquire session token and click next. Used to compare number of connected users and number of user who answered to question',
    `current` int(3) DEFAULT 0 COMMENT 'This field is set to its negative value when response is displayed',
    `token` varchar(4) DEFAULT NULL,
    `auth` varchar(20) DEFAULT 'anon,pseudo,moodle',
    `access` varchar(7) DEFAULT 'public',
    PRIMARY KEY (`id`),
    KEY `user_id` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


--
-- `question_types` table structure
--
DROP TABLE IF EXISTS `question_types`;
CREATE TABLE IF NOT EXISTS `question_types` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(128) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;


--
-- `question_types` data dumping
--
INSERT INTO `question_types` (`id`, `name`) VALUES
(1, 'multiple_choices'),
(2, 'unique_choice'),
(3, 'words_cloud'),
(4, 'click_map'),
(5, 'poll');


-- --------------------------------------------------------

--
-- `questions` table structure
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `q_order` int(2) NOT NULL,
  `q_text` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- `proposals` table structure
--

DROP TABLE IF EXISTS `proposals`;
CREATE TABLE IF NOT EXISTS `proposals` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `question_id` int(11) NOT NULL,
    `p_order` int(1) NOT NULL,
    `p_text` varchar(255),
    `correct` tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `question_id` (`question_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- `responses` table structures
--

DROP TABLE IF EXISTS `responses`;
CREATE TABLE IF NOT EXISTS `responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `response` varchar(256) NOT NULL,
  `username` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;



--
-- `access_keys` table structure
--
DROP TABLE IF EXISTS `access_keys`;
CREATE TABLE IF NOT EXISTS `access_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(10) NOT NULL ,
  `session_id` INT NOT NULL ,
  `validity_date` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;




--
-- `statistics` table structure
--
DROP TABLE IF EXISTS `statistics`;
CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_create` int(11) NOT NULL DEFAULT 0,
  `session_join` int(11) NOT NULL DEFAULT 0,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;


--
-- `authorisations` table structure
--
DROP TABLE IF EXISTS `authorisations`;
CREATE TABLE IF NOT EXISTS `authorisations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `guest_id` int(11) NOT NULL,
  `rights` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `guest_id` (`guest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


SET FOREIGN_KEY_CHECKS = 1;

--
-- Constraints
--

--
-- `sessions` table constraints
--
ALTER TABLE `sessions`
    ADD CONSTRAINT `session_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- `proposals` table constraints
--
ALTER TABLE `proposals`
  ADD CONSTRAINT `proposal_question_fk` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- `questions` table constraints
--
ALTER TABLE `questions`
  ADD CONSTRAINT `question_session_fk` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_type_fk` FOREIGN KEY (`type_id`) REFERENCES `question_types` (`id`);

--
-- `responses` table constraints
--
ALTER TABLE `responses`
  ADD CONSTRAINT `response_question_fk` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- `access_keys` table constraints
--
ALTER TABLE `access_keys`
  ADD CONSTRAINT `access_key_session_fk` FOREIGN KEY (`session_id`) REFERENCES `sessions`(`id`) ON DELETE CASCADE;

ALTER TABLE `authorisations`
  ADD CONSTRAINT `fk_guest_id` FOREIGN KEY (`guest_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sessauth_id` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

COMMIT;
