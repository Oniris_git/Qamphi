<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 07/05/2019
 * Time: 11:50
 */


namespace App\Install;

use App\BO\User;
use PDO;
use PDOException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class installFunctions
{

    function createDB($dataBDD){
        try{
            $requestDrop = 'DROP PROCEDURE IF EXISTS deleteUpdateOrder;';
            $requestCreate = 'CREATE PROCEDURE deleteUpdateOrder (IN s_id INT, IN deleted_q_order INT)
            BEGIN
                DELETE FROM questions WHERE session_id=s_id AND q_order=deleted_q_order;
                UPDATE questions
                SET q_order = q_order-1
                WHERE q_order > deleted_q_order
              AND session_id=s_id;
            END';

            $requestDrop2 = 'DROP PROCEDURE IF EXISTS proposalDelete;';
            $requestCreate2 = 'CREATE PROCEDURE proposalDelete (IN q_id INT, IN deleted_p_order INT)
            BEGIN
                DELETE FROM proposals WHERE question_id=q_id AND p_order=deleted_p_order;
                UPDATE proposals
                SET p_order = p_order-1
                WHERE p_order > deleted_p_order
              AND question_id=q_id;
            END';

            $bdd = new PDO($dataBDD['db_type'].":host=".$dataBDD['db_host'], $dataBDD['db_user'], $dataBDD['db_password']);

            //files sql to create BDD
            $fileSql = realpath(__DIR__.'/install.sql');
            $sqlContent = file_get_contents($fileSql);
            $dbName = $dataBDD['db_name'];
            $file = fopen($fileSql, 'r+');
            fwrite($file,
            "DROP DATABASE IF EXISTS $dbName;
            CREATE DATABASE $dbName
            CHARACTER SET utf8mb4
            COLLATE utf8mb4_unicode_ci;
            USE $dbName;
            ".$sqlContent);
            fclose($file);

            function executeQueryFile($fileSql, $bdd) {
                $query = file_get_contents($fileSql);
                $bdd->query($query);
            }
            executeQueryFile($fileSql, $bdd);
            $bdd->exec($requestDrop);
            $bdd->exec($requestCreate);
            $bdd->exec($requestDrop2);
            $bdd->exec($requestCreate2);

            $file = fopen($fileSql, 'w');
            fwrite($file, $sqlContent);
            fclose($file);

            return true;
            //if values are not valid throw exception to display
        }catch(Exception $e){
            return "Error :  ".$e->getMessage();
        }
    }

    function testDB($BDD){
        try{
            $bdd = new PDO($BDD['db_type'].':host='.$BDD['db_host'],$BDD['db_user'],$BDD['db_password']);
            return true;
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }

    function testDBMoodle($BDD){
        try{
            $bdd = new PDO($BDD['db_type'].':host='.$BDD['db_host'].';dbname='.$BDD['db_name'],$BDD['db_user'],$BDD['db_password']);
            return true;
        }catch(PDOException $e){
            return $e->getMessage();
        }
    }

    function createConfigFile($dataBDD, $dataBDDMoodle){
        $db = $dataBDD['db_name'];
        $host = $dataBDD['db_host'];
        $login = $dataBDD['db_user'];
        $pass = $dataBDD['db_password'];
        $type = $dataBDD['db_type'];
        $dbMoodle = $dataBDDMoodle['db_name'];
        $hostMoodle = $dataBDDMoodle['db_host'];
        $loginMoodle = $dataBDDMoodle['db_user'];
        $passMoodle = $dataBDDMoodle['db_password'];
        $typeMoodle = $dataBDDMoodle['db_type'];
        $file = fopen(__DIR__."/../../config/Config.php", "w");
        fwrite($file, "<?php\r\n
        return [\r\n
        \t\t\"db_type\" =>     \"$type\",\r\n
        \t\t\"db_name\" =>     \"$db\",\r\n
        \t\t\"db_host\" =>   \"$host\",\r\n
        \t\t\"db_user\" =>     \"$login\",\r\n
        \t\t\"db_password\" => \"$pass\",\r\n
        \t\t\"db_nameMoodle\" =>     \"$dbMoodle\",\r\n
        \t\t\"db_hostMoodle\" =>   \"$hostMoodle\",\r\n
        \t\t\"db_userMoodle\" =>     \"$loginMoodle\",\r\n
        \t\t\"db_passwordMoodle\" => \"$passMoodle\",\r\n
        \t\t\"db_typeMoodle\" => \"$typeMoodle\",\r\n
        ];\r\n
        ?>"
        );
        //Close configuration:
        fclose($file);
    }

    function createConfigFileLess($dataBDD){
        $db = $dataBDD['db_name'];
        $host = $dataBDD['db_host'];
        $login = $dataBDD['db_user'];
        $pass = $dataBDD['db_password'];
        $type = $dataBDD['db_type'];
        $file = fopen(__DIR__."/../../config/Config.php", "w");
        fwrite($file, "<?php\r\n
        return [\r\n
        \t\t\"db_type\" =>     \"$type\",\r\n
        \t\t\"db_name\" =>     \"$db\",\r\n
        \t\t\"db_host\" =>   \"$host\",\r\n
        \t\t\"db_user\" =>     \"$login\",\r\n
        \t\t\"db_password\" => \"$pass\",\r\n
        \t\t\"version\" => \"20.09\",\r\n
        \r\n
        ];\r\n
        ?>"
        );
        //Close configuration:
        fclose($file);
    }

    function createSMTPconfigFile($dataSMTP) {
        $host = $dataSMTP['host'];
        $port = $dataSMTP['port'];
        $username = $dataSMTP['username'];
        $password = $dataSMTP['password'];
        $certs = $dataSMTP['certs'];
        $file = fopen(__DIR__."/../../config/ConfigSMTP.php", "w");
        fwrite($file, "<?php\r\n
        return [\r\n
        \t\t\"host\" =>     \"$host\",\r\n
        \t\t\"port\" =>   \"$port\",\r\n
        \t\t\"username\" =>     \"$username\",\r\n
        \t\t\"password\" => \"$password\",\r\n
        \t\t\"certs\" => \"$certs\",\r\n
        ];\r\n
        ?>"
        );
    }

    function testAdmin($user) {
        //username format
        $upnValid = stristr($user['username'], '.');

        if ($upnValid === 0) {
            return 'L\'identifiant de l\'utilisateur doit être au format prenom.nom';

        }else if (!filter_var($user['email'])){
            return 'L\'email n\'est pas valide';
        }
        //password input comparison
        $compare = strcmp($user['confirmPassword'], $user['password']);
        //password size verify
        $length = strlen($user['password']);
        if ($length <= 7) {
            return 'Le mot de passe doit contenir un minimum de 8 caractères';
        } elseif ($compare != 0) {
            return 'Les deux mots de passe doivent être identiques';
        }
        return true;
    }

    function insertAdmin($user, $userDAO){
        //return $this->testAdmin($user);

        $username = $user['username'];
        $auth = 'lock';

        $hash = password_hash($user['password'], PASSWORD_DEFAULT);
        //array used to creat an User object
        $userData = [
            'upn' => $username,
            'email' => $user['email'],
            'username' => $user['username'],
            'auth' => $auth,
            'role' => 'Admin',
            'password' => $hash
        ];
        $user = new User($userData);
        try{
            $userDAO->insertUser($user);
            return true;
        }catch (PDOException $e){
            return $e->getMessage();
        }
    }

    function testSMTP($dataSMTP){
        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host       = $dataSMTP['host'];
            $mail->SMTPAuth   = true;
            //$mail->SMTPDebug = 4 ;
            $mail->Username   = $dataSMTP['username'];
            $mail->Password   = $dataSMTP['password'];
            $mail->SMTPSecure = 'ssl';
            if ($dataSMTP['certs'] == 'on') {
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
            }
            $mail->Port       = $dataSMTP['port'];
            $mail->setFrom($dataSMTP['username'], 'Mailer');
            $mail->addAddress($dataSMTP['username']);
            $mail->isHTML(true);
            $mail->Subject = 'Test SMTP Qamphi';
            $mail->Body    = 'SMTP works !';

            $mail->send();
        } catch (Exception $e) {
            throw new Exception("Error authenticate SMTP");
        }
    }

}
