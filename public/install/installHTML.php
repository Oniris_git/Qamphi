<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 07/05/2019
 * Time: 10:57
 */

class installHTML
{

    public function installTitle(){
        $result = '<div class="row">
                        <div class="col-8">
                            <h1>
                                <i class="fas fa-database" style="color: #625a5a"></i> Installation
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function dbForm($disabled, $data){

        $result = ' <form action="?page=install" method="post">
                        <div class="installContainer">
                            <div class="row">
                                <div class="col-12">
                                    <h2><strong>I- '.$LANG['database'].' Qamphi</strong></h2>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['server_name'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="serverName" placeholder="localhost" class="form-control" required '.$disabled.' value='.$data['db_host'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Login :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="loginBDD" class="form-control" required '.$disabled.' value='.$data['db_user'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['password'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="password" name="passwordBDD" class="form-control"  '.$disabled.'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['database'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="bddName" class="form-control" '.$disabled.' value='.$data['db_name'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Type :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <select name="bddType" class="form-control" '.$disabled.'>
                                        <option value="mysql"'.self::isselected($data, 'mysql').'>MySQL/MariaDB</option>
                                        <option value="oci"'.self::isselected($data, 'oci').'>Oracle</option>
                                        <option value="pgsql"'.self::isselected($data, 'pgsql').'>PostgreSQL</option>
                                    </select>
                                </div>
                            </div>
                        </div>';
        echo $result;
    }

    public function dbFormMoodle($data){

        $result = ' <form action="?page=install" method="post">
                        <div class="installContainer">
                            <div class="row switchAlign">                            
                            <div class="col-12 col-sm-8 col-md-7 col-lg-5 col-xl-3">                            
                                    <h2>
                                        <strong>II- '.$LANG['database'].' Moodle</strong>
                                        <a type="button" data-toggle="modal" data-target="#modalHelpMoodle">
                                            <i class="far fa-question-circle" style="font-size:1.2rem;"></i>
                                        </a>
                                    </h2>
                                </div>
                                <label class="switch">
                                <input id="radioMoodle" type="radio"';
                                $result .= 'name="radioMoodle" onclick="disableMoodle()">
                                <span class="slider round"></span>
                            </label>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['server_name'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" id="serverNameMoodle" name="serverNameMoodle" placeholder="localhost" class="form-control disableMoodle" required value='.$data['db_host'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Login :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="loginBDDMoodle" class="form-control disableMoodle" required value='.$data['db_user'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['password'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="password" name="passwordBDDMoodle" class="form-control disableMoodle">
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['database'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="bddNameMoodle" class="form-control disableMoodle" value='.$data['db_name'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                            <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                Type :
                            </div>
                            <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                <select name="bddTypeMoodle" class="form-control">
                                <option value="mysql"'.self::isselected($data, 'mysql').'>MySQL/MariaDB</option>
                                <option value="oci"'.self::isselected($data, 'oci').'>Oracle</option>
                                <option value="pgsql"'.self::isselected($data, 'pgsql').'>PostgreSQL</option>
                                </select>
                            </div>
                        </div>
                        </div>';
        $result .= $this->modalHelpMoodle();
        echo $result;
    }

    public function refresh($base, $ERROR, $lang) {
        echo '<div class="container" style="display :block; align-items: center">';
        $base->displayError($ERROR);
        echo '<a type="button" href="./install.php?lang='.$lang.'" >
            <i class="fas fa-redo" style="margin-left:47%" title="Reload page"></i>
        </a>
        </div>';
    }

    public function modalHelpMoodle(){

        $result = ' <div class="modal fade" id="modalHelpMoodle" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Moodle help</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                If you want to couple Moodle accounts with Qamphi enter the connection information in the fields.<br>
                                If not just check the radio button to disable fonctionnality. You can change later.
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>';

        return $result;
    }

    public function adminForm($disabled, $datas){
        $result = ' <form action="?page=install" method="post">
                        <div class="installContainer">
                            <div class="row">
                                <div class="col-12">
                                    <h2><strong>VI- '.$LANG['admin'].'</strong></h2>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Username :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="username" placeholder="firstname.name" class="form-control" required '.$disabled.' value='.$datas['username'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Email :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="email" name="email" class="form-control" required '.$disabled.'  value='.$datas['email'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['password'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="password" name="password" class="form-control" required '.$disabled.' placeholder="Minimum 8 characters">
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['confirm_password'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="password" name="confirmPassword" class="form-control" required '.$disabled.' placeholder="Minimum 8 characters">
                                </div>
                            </div>
                        </div>';
        echo $result;
    }

    public function SMTPForm($disabled, $data){
        $checked = $data['certs'] == 'on' ? ' checked' : '';
        $result = ' <form action="?page=install" method="post">
                        <div class="installContainer">
                            <div class="row">
                                <div class="col-12">
                                    <h2 style="display:inline"><strong>III- '.$LANG['smtp'].'</strong></h2>
                                    <a type="button" data-toggle="modal" data-target="#modalHelpSMTP">
                                        <i class="far fa-question-circle" style="font-size:1.2rem;"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Host :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="text" name="hostSMTP" placeholder="smtps.example.com" class="form-control" required '.$disabled.' value='.$data['host'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Port :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="number" name="portSMTP" placeholder="465" class="form-control" required '.$disabled.' value='.$data['port'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    Email :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="email" name="usernameSMTP" class="form-control" required placeholder="noreply@example.com" '.$disabled.' value='.$data['username'].'>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                    '.$LANG['password'].' :
                                </div>
                                <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                    <input type="password" name="passwordSMTP" class="form-control" required '.$disabled.'>
                                </div>
                            </div>
                            
                            
                            <div class="row justify-content-center">
                            <div class="col-6 col-lg-5 col-md-5 col-xl-5 margin_label">
                                '.$LANG['certs'].' :
                            </div>
                            <div class="col-6 col-lg-5 col-md-5 col-xl-6">
                                <input type="checkbox" name="certs" class="" '.$disabled.$checked.'>
                            </div>
                        </div>';
        $result.=                '</div>';
        $result .= $this->modalHelpSMTP();
        echo $result;
    }

    public function modalHelpSMTP(){

        $result = ' <div class="modal fade" id="modalHelpSMTP" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">SMTP help</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                SMTP data is essential to give the user the possibility to recover his password. <br>
                                You will find the documentation <a href="https://serversmtp.com/smtp-settings/">here</a>.
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>';

        return $result;
    }

    public function validForm($disabled, $lang){

            $result = ' <div class="row justify-content-center">
                            <div class="col-4">';
                if ($lang !=='') {
                    $result.=            '<input type="hidden" name="language" value="'.$lang.'">';
                }
            $result .=          '<button type="submit" class="btn btn-outline-success width100 btn_margin" '.$disabled.'>Ok</button>
                            </div>
                        </div>
                    </form>
                    <script src="../JS/global.js"></script>';

        echo $result;

    }

    public function linkToInstall($lang) {
        $result = ' <div class="row justify-content-center">
                        <div class="col-4">
                        <form method="POST" action="install.php">';
                    if ($lang !=='') {
                    $result.=            '<input type="hidden" name="language" value="'.$lang.'">';
                    }
                    $result .=          '<button type="submit" class="btn btn-outline-success width100 btn_margin" >Ok</button>
                                        </form>
                            </div>
                        </div>
                    </form>';

            echo $result;
        }

    public function environmentChoice($disabled, $value) {
        $result = '<form action="?page=install" method="post">
                    <div class="installContainer">
                        <h2>
                        <strong>'.$LANG['environment'].'</strong>
                        </h2>
                        <input type="radio" id="prod" name="environment" value="prod" '.$disabled.' ';
        if ($value == 'prod') {
            $result .= 'checked>
            ';
        } else {
            $result .= '>
            ';
        }
        $result .=     '<label for="prod">'.$LANG['prod'].'&nbsp;&nbsp;</label>
                        <input type="radio" id="dev" name="environment" value="dev" '.$disabled.' ';
        if ($value == 'dev') {
            $result .= 'checked>
            ';
        } else {
            $result .= '>
            ';
        }
        $result .=     '<label for="dev">'.$LANG['dev'].'</label>
                    </div>';

        echo $result;

    }

    public function headerInstall($css_files = []){
        header( 'content-type: text/html; charset=utf-8' );
        foreach ($css_files as $link){
            echo '<link rel="stylesheet" href="../css/css/'.$link.'.css">';
        }
        echo '	<link rel="stylesheet" href="../css/lib/bootstrap.min.css">
            <link rel="stylesheet" href="../css/css/app_global.css">
			<meta name="viewport" http-equiv="Content-Type" content="width=device-width, initial-scale=1.0, text/html; charset=utf-8"/> 
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
			<title>Qamphi</title>
			<link rel="shortcut icon" href="../image/favicon.ico">';
    }

    public function error($e) {
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p style="text-align: center"><strong>'.$e.'</strong></p>
     </div>';
    }

    //display base footer
    public function install_footer($radio){

        $retour = '	<footer>
                    <script src="../css/lib/jquery-3.4.1.min.js"></script>
                    <script src="../css/lib/bootstrap.bundle.js"></script>
                    <script src="../JS/global.js"></script>
                    <script src="../JS/database.js"></script>';
        if ($radio == "off") {
            $retour .=	'<script src="../JS/install.js"></script>';
        }

        $retour .=        '</footer>';
        echo $retour;
    }

    public function langInit($langs, $current) {
        $retour = '
        <form id="formLang" method="POST" action="lang.php">
        <div class="installContainer">
        <h2>
        <strong>'.$LANG['ttl_lang_choice'].'</strong>
        </h2>
        <select name="language" id="language" class="form-control col-md-3" onchange="changeLang()">';
        foreach ($langs as $name => $content) {
            if ($name == $current) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            $retour .= '<option value="'.$name.'" '.$selected.'>'.$content.'</option>';
        }
        $retour .= '</select>
        </div>
        </form>';

        echo $retour;
    }

    static function isselected($data, $dbtype) {
        return $data['db_type'] == $dbtype ? ' selected' : '';
    }

}
