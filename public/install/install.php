<?php

require '../../app/Autoloader.php';
require '../../vendor/autoload.php';

use App\Autoloader;
use App\HTML\base;
use App\Install\installFunctions;
use App\DAL\UserDAO;
use App\DAL\ConfigDAO;

//autoload classes
Autoloader::register();

$qamphi = str_replace($_SERVER['DOCUMENT_ROOT'],'',__DIR__);
$qamphi = str_replace('/public/install', '', $qamphi)   ;
$tmpUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$qamphi;


if (file_exists('../../config/Config.php') && file_exists('../../config/ConfigSMTP.php')) {
    header('Location: '.$tmpUrl.'/index.php');
    die();
}


//used classes
require '../../HTML/base.php';
require '../../public/install/installFunctions.php';
require '../../public/install/installHTML.php';

$base = new base();
$install = new installHTML();
$installFunction = new installFunctions();
$backLink = '../image/bg2.jpg';
$ERROR = [];

$argsGet = [
    'lang' => FILTER_SANITIZE_STRING,
];

$args = [
    'serverName' => FILTER_SANITIZE_STRING,
    'loginBDD' => FILTER_SANITIZE_STRING,
    'passwordBDD' => FILTER_SANITIZE_STRING,
    'serverNameMoodle' => FILTER_SANITIZE_STRING,
    'bddNameMoodle' => FILTER_SANITIZE_STRING,
    'bddName' => FILTER_SANITIZE_STRING,
    'bddType' => FILTER_SANITIZE_STRING,
    'bddTypeMoodle' => FILTER_SANITIZE_STRING,
    'loginBDDMoodle' => FILTER_SANITIZE_STRING,
    'passwordBDDMoodle' => FILTER_SANITIZE_STRING,
    'username' => FILTER_SANITIZE_STRING,
    'email' => FILTER_VALIDATE_EMAIL,
    'password' => FILTER_SANITIZE_STRING,
    'confirmPassword' => FILTER_SANITIZE_STRING,
    'radioMoodle' => FILTER_SANITIZE_STRING,
    'hostSMTP' => FILTER_SANITIZE_STRING,
    'portSMTP' => FILTER_VALIDATE_INT,
    'usernameSMTP' => FILTER_SANITIZE_STRING,
    'passwordSMTP' => FILTER_SANITIZE_STRING,
    'certs' => FILTER_SANITIZE_STRING,
    'domain' => FILTER_SANITIZE_STRING,
    'environment' => FILTER_SANITIZE_STRING,
    'language' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);


$GET = filter_input_array(INPUT_GET, $argsGet, false);

if (isset($POST['language'])) {
    $lang = $POST['language'];
} elseif (isset($GET['lang'])) {
    $lang = $GET['lang'];
}

require './../../config/lang/'.$lang.'.php';
foreach($LANG as $key=>$value) {
    unset($LANG[$key]);
    $key = substr($key,4);
    $LANG[$key] = $value;
}

if(isset($POST['serverName'])){

    $dataBDD = [
        'db_host' => $POST['serverName'],
        'db_name' => str_replace(' ','',$POST['bddName']),
        'db_user' => $POST['loginBDD'],
        'db_password' => $POST['passwordBDD'],
        'db_type' => $POST['bddType'],
    ];

    $dataSMTP = [
        'host' => $POST['hostSMTP'],
        'port' => $POST['portSMTP'],
        'username' => $POST['usernameSMTP'],
        'password' => $POST['passwordSMTP'],
        'certs' => isset($POST['certs']) ? $POST['certs'] : 'off'
    ];

    try{
        $resultDB = $installFunction->testDB($dataBDD);

        $valueBDD = $dataBDD;

        $resultAdmin = $installFunction->testAdmin($POST);


            $adminDatas = [
                'username' => $POST['username'],
                'email' => $POST['email'],
            ];
            $adminPwd = $POST['password'];


        if(isset($POST['radioMoodle']) && $POST['radioMoodle'] == "off"){
            $radioMoodle = "off";
        } else {
            $radioMoodle ="on";
            $dataBDDMoodle = [
                'db_host' => $POST['serverNameMoodle'],
                'db_name' => str_replace(' ','',$POST['bddNameMoodle']),
                'db_user' => $POST['loginBDDMoodle'],
                'db_password' => $POST['passwordBDDMoodle'],
                'db_type' => $POST['bddTypeMoodle'],
            ];
            $resultDBMoodle = $installFunction->testDBMoodle($dataBDDMoodle);
            $valueBDDMoodle = $dataBDDMoodle;
            if ($resultDBMoodle !== true) {
                $ERROR['MOODLE_ERROR'] = 'Moodle parameters error';
            }
        }


//        try{
//            $installFunction->testSMTP($dataSMTP);
//        }catch(PHPMailer\PHPMailer\Exception $e){
//            $ERROR['SMTP_ERROR'] = $e->getMessage();
//        }

        $SMTPValues = $dataSMTP;


        if ($resultDB !== true) {
            $ERROR['DB_ERROR'] = $resultDB;
        }

        if ($resultAdmin !== true) {
            $ERROR['ADMIN_ERROR'] = $resultAdmin;
        }
        if (empty($ERROR)) {
            $installDB = $installFunction->createDB($dataBDD);

            $configDAO = new ConfigDAO($dataBDD);
            $userDAO = new UserDAO($dataBDD);

            // config definition in DB
            $configDAO->insertConfig('general', 'environment', $POST['environment']);
            $configDAO->insertConfig('general', 'lang', $POST['language']);
            $configDAO->updateConfig('auth', 'moodle', $radioMoodle);

            //config file generation
            $installFunction->createSMTPconfigFile($dataSMTP);
            if($radioMoodle == "off"){
                $installFunction->createConfigFileLess($dataBDD);
            }else{
                $installFunction->createConfigFile($dataBDD, $dataBDDMoodle);
            }

            $code_version = require './../../config/Version.php';
            $configDAO->insertConfig('general', 'sql_version', $code_version);

            //admin creation
            $adminInsert = $installFunction->insertAdmin($POST, $userDAO);

            require './../../app/dependencies.php';

            try{
                $credentials['username'] = $adminDatas['username'];
                $credentials['password'] = $adminPwd;
                $result = $auth->login($credentials);
                $userQamphi = $userDAO->selectUserByUsername($credentials['username']);
                if($userQamphi != null){
                    $session->setValue('id', $userQamphi->getId());
                    $session->setValue('role', $userQamphi->getRole());
                }
            }catch (\Vespula\Auth\Exception $e){
                throw new \Vespula\Auth\Exception($e);
            }
            header('Location: '.$tmpUrl.'/public/index.php?page=success');
            exit;
        }

    }catch(PDOException $e){
        $ERROR = [
            'message' => $e->getMessage()
        ];
    }
}

if(!is_writable ("../../config") || !is_writable("./install.sql") || !is_writable("./../image") || !is_writable("./../tmp")){
    $ERROR['message'] = $LANG['permissions'];
}

$install->headerInstall(['main_install']);

$base->open_body($backLink);

$base->table("without-btn");

$install->installTitle();

$disabled = '';

if (!isset($valueBDD)) {
    $valueBDD = [
        'db_host' => '',
        'db_name' => 'qamphi',
        'db_user' => '',
        'db_type' => 'mysql',
    ];
}

if (!isset($valueBDDMoodle)) {
    $valueBDDMoodle = [
        'db_host' => '',
        'db_name' => 'moodle',
        'db_user' => '',
        'db_type' => 'mysql',
    ];
}

if (!isset($adminDatas)) {
    $adminDatas = [
        'username' => '',
        'email' => ''
    ];
}

if (!isset($SMTPValues)) {
    $SMTPValues = [
        'host' => '',
        'port' => '',
        'username' => '',
        'certs' => ''
    ];
}

if (!isset($radioMoodle)) {
    $radioMoodle = "off";
}

// if (isset($POST['domain'])) {
//     $domain = $POST['domain'];
// } else {
//     $domain = '';
// }

if (isset($POST['environment'])) {
    $environmentValue = $POST['environment'];
} else {
    $environmentValue = 'prod';
}


if(isset($ERROR) && !empty($ERROR)){
    if ($ERROR['message'] == $LANG['permissions']) {
        $install->refresh($base, $ERROR, $lang);
        $disabled = 'disabled';
    }
    // else {
    //     $base->displayError($ERROR);
    // }
}

$install->environmentChoice($disabled, $environmentValue);

if (isset($ERROR['DB_ERROR'])) {
    $install->error($ERROR['DB_ERROR']);
}
$install->dbForm($disabled, $valueBDD);

if (isset($ERROR['MOODLE_ERROR'])) {
    $install->error($ERROR['MOODLE_ERROR']);
}
$install->dbFormMoodle($valueBDDMoodle);

if (isset($ERROR['SMTP_ERROR'])) {
    $install->error($ERROR['SMTP_ERROR']);
}
$install->SMTPForm($disabled, $SMTPValues);

if (isset($ERROR['ADMIN_ERROR'])) {
    $install->error($ERROR['ADMIN_ERROR']);
}
$install->adminForm($disabled, $adminDatas);

$install->validForm($disabled, $lang);

$base->table("end");

$base->close_body();

$install->install_footer($radioMoodle);
