<?php

require '../../app/Autoloader.php';
require '../../vendor/autoload.php';

use App\Autoloader;
use App\HTML\base;
use App\Install\installFunctions;

//autoload classes
Autoloader::register();

if (file_exists('../../config/Config.php') && file_exists('../../config/ConfigSMTP.php')) {
    header('Location: ./../index.php');
}

require '../../HTML/base.php';
require '../../public/install/installFunctions.php';
require '../../public/install/installHTML.php';


$args['language'] = FILTER_SANITIZE_STRING;

$POST = filter_input_array(INPUT_POST, $args, false);

$lang = isset($POST['language']) ? $POST['language'] : 'fr_FR';
// dump($lang);
// die();

$base = new base();
$install = new installHTML();
$installFunction = new installFunctions();
$backLink = '../image/bg2.jpg';
$ERROR = [];

// if(!is_writable ("../../config") || !is_writable("./install.sql") || !is_writable("./../image") || !is_writable("./../tmp")){
//     $ERROR['message'] = $LANG['err_permissions'];
// }


require '../../config/lang/'.$lang.'.php';


$install->headerInstall(['lang_install']);
$base->open_body($backLink);
$base->table("without-btn");
$install->installTitle();
$disabled = '';
// if (isset($ERROR['message']) && $ERROR['message'] == $LANG['err_permissions']) {
//     $install->refresh($base, $ERROR);
//     $disabled = 'disabled';
// } 
$install->langInit([
    'fr_FR' => 'Français',
    'en_EN' => 'English'
], $lang);
$install->install_footer('');
$install->linkToInstall($lang);





