<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 06/05/2019
 * Time: 17:05
 */

namespace App\DAL;
use App\DAL\BDD;


class ConfigDAO extends BDD
{

    public function selectConfig() {
        $request = "SELECT * FROM config";
        $data = [];

        $result = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] = $row;
        }
        return $data;


    }


    /**
     * @param $content
     */
    public function updateLegal($content){
        $request = "UPDATE config SET value = :value WHERE type = :type AND name = :name";


        $stmt = $this->getPDO()->prepare($request);

        $stmt->execute([
            ':type' => 'view',
            ':name' => 'legal',
            ':value' => $content
        ]);

    }

    public function selectConfigByType($type, $name){

        $request = "SELECT * FROM config WHERE type = :type AND name = :name";


        $result = [];
        $prep = $this->getPDO()->prepare($request);
        $prep->execute([
            ':type' => $type,
            ':name' => $name,
        ]);
        $stmt = $prep->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($stmt as $row){
            return $row;
        }

    }

    public function updateConfig($type, $name, $content){
        $request = "UPDATE config SET value = :value WHERE type = :type AND name = :name";

        $stmt = $this->getPDO()->prepare($request);

        $stmt->execute([
            ':type' => $type,
            ':name' => $name,
            ':value' => $content
        ]);

    }

    public function insertConfig($type, $name, $value){

        $request = 'INSERT INTO `config`(`type`, `name`, `value`) VALUES (:type, :name, :value)';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':type' => $type,
            ':name' => $name,
            ':value' => $value
        ]);

    }

}