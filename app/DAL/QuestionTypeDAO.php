<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 16:27
 */

namespace App\DAL;

use App\BO\QuestionType;
use mysql_xdevapi\Exception;
use PDO;
use PDOException;

class QuestionTypeDAO extends BDD
{

    /**
     * @return array question types
     */
    public function selectAllTypes() {
        $request = 'SELECT * FROM question_types;';
        $data = [];
        $result = $this->getPDO()->query($request);
        foreach ($result as $row){
            $data[] = new QuestionType($row);
        }
        return $data;
    }

    public function selectTypeById($id) {
        $request = 'SELECT * from question_types WHERE id = :id';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            return new QuestionType($row);
        }
    }

}