<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 09:52
 */

namespace App\DAL;

use App\DAL\BDD;
use App\BO\User;

class AuthorisationDAO extends BDD
{

    /**
     * @return array
     */
    public function allAuthorisations() {
        $request = 'SELECT * FROM authorisations';
        $data = [];

        $result = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] = $row;
        }
        return $data;

    }

    public function insert($sessionid, $guest_id, $rights) {
        $request = 'INSERT INTO authorisations (session_id, guest_id, rights) VALUES (:session_id, :guest_id, :rights)';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $sessionid,
            ':guest_id' => $guest_id,
            ':rights' => $rights,
        ]);

    }

    /**
     * @param $id
     */
    public function delete($id){
        $request = 'DELETE FROM authorisations WHERE id = :id';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $id
        ]);

    }

    /**
     * @param $id
     */
    public function deleteBySessionGuest($session_id, $guest){
        $request = 'DELETE FROM authorisations WHERE session_id = :session_id AND guest_id = :guest';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $session_id,
            ':guest' => $guest
        ]);

    }

    public function guestsBySession($session_id) {
        $request = 'SELECT * FROM authorisations INNER JOIN sessions ON authorisations.session_id = sessions.id
                                                  INNER JOIN users ON authorisations.guest_id = users.id
                                                  WHERE authorisations.session_id = :session_id';


        $data = [];
        $i = 0;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $session_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i]['user'] =  new User($row);
            $data[$i]['auth'] =  $row['rights'];
            $i++;
        }
        return $data;

    }

    public function adminBySession($session_id) {
        $request = 'SELECT * FROM authorisations INNER JOIN sessions ON authorisations.session_id = sessions.id
                                                  INNER JOIN users ON authorisations.guest_id = users.id
                                                  WHERE authorisations.session_id = :session_id
                                                  AND rights = \'write\';';

        $data = [];
        $i = 0;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $session_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i]['user'] =  new User($row);
            $data[$i]['auth'] =  $row['rights'];
            $i++;
        }
        return $data;

    }

    public function searchUserByNameNotGuest($input, $session_id){

        $request = 'SELECT * FROM users WHERE username LIKE :input
                                          AND users.id NOT IN
                                          (SELECT guest_id FROM authorisations WHERE session_id = :session_id);';

        $data = [];
        $i=1;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':input' => "%$input%",
            ':session_id' => $session_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] =  new User($row);
        }
        return $data;

    }

    public function updateRights($idSession, $guest) {
        $request = 'UPDATE authorisations SET rights = (CASE WHEN rights = \'read\' THEN \'write\' ELSE \'read\' END)
                                                WHERE session_id = :session_id AND guest_id = :guest;';


        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $idSession,
            ':guest' => $guest
        ]);

    }

    public function selectRights($idSession, $guest) {
        $request = 'SELECT rights FROM authorisations WHERE session_id = :session_id AND guest_id = :guest;';


        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':session_id' => $idSession,
            ':guest' => $guest
        ]);
        return $stmt->fetchColumn();

    }
}