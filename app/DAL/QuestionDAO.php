<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:01
 */

namespace App\DAL;

use App\BO\Questions\Question;
use App\DAL\BDD;
use PDOException;

class QuestionDAO extends BDD
{


    /**
     * @return array Questions
     */
    public function allQuestions() {
        $request = 'SELECT * FROM questions';
        $results = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $questions = [];
        foreach ($results as $row) {
            $questions[] = Question::instanciate($row);
        }
        return $questions;
    }

    /**
     * @param $s_id
     * @return array
     */
    public function selectQuestionBySession($s_id){

        $requete = "SELECT * FROM questions WHERE session_id = :s_id ORDER BY q_order";
        $i=0;
        $data = [];

        $stmt=$this->getPDO() ->prepare($requete);
        $stmt->execute([
            ':s_id' => $s_id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            $data[$i] =  Question::instanciate($item);
            $i++;
        }
        return $data;

    }

    /**
     * @param int session id
     * @return int number of questions
     */
    public function countQuestionBySession($idSession){

        $request = "SELECT COUNT(*) FROM `questions` WHERE session_id = :s_id ";

        $result = $this->getPDO()->prepare($request);
        $result->execute([
            ':s_id' => $idSession
        ]);
        return $result->fetchColumn();

    }

    /**
     * @param $idQuestion
     * @return Question
     */
    public function selectQuestionById($idQuestion){

        $requete = 'SELECT * FROM questions WHERE id= :q_id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $idQuestion
        ]);
        foreach ($stmt as $row){
            return Question::instanciate($row);
        }

    }

    /**
     * @param int session id
     * @param int question order
     * @return Question
     */
    public function selectQuestionByOrder($idSession, $order){
        $request = "SELECT * FROM questions WHERE session_id = :s_id AND q_order = :q_order";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':s_id' => $idSession,
            ':q_order' => $order
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            return Question::instanciate($row);
        }
    }

    /**
     * @param $s_id
     * @param $q_order
     * @param $q_text
     * @return
     */
    public function insert($s_id, $q_order, $q_text, $typeId){

        $requete='INSERT INTO questions(session_id, q_order, q_text, type_id) VALUES(:session_id, :q_order, :q_text, :type_id)';

        $stmt = $this->getPDO()->prepare($requete);
        $result = $stmt->execute([
            ":session_id" => $s_id,
            ":q_order" => $q_order,
            ":q_text" => $q_text,
            ":type_id" => $typeId,
        ]);
        return $this->getPDO()->lastInsertId();

    }

    /**
     * @param $s_id
     * @param $question
     */
    public function updateQuestion($s_id, $question){

            $requete='UPDATE questions SET q_text= :q_text, type_id = :type_id WHERE session_id= :session_id AND q_order= :q_order';

            $stmt = $this->getPDO()->prepare($requete);
            $stmt->execute([
                ':q_text' => $question->getContent(),
                ':session_id' => $s_id,
                ':q_order' => $question->getOrder(),
                ':type_id' => $question->getType(),
            ]);

    }

    /**
     * @param Question
     */
    public function updateOrder($question){
        $requete='UPDATE questions SET q_order = :q_order WHERE id = :id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_order' => $question->getOrder(),
            ':id' => $question->getId(),
        ]);

    }

    /**
     * @param int question id
     * @param array values
     */
    public function updateQuestionById($id, array $params) {
        $requete = 'UPDATE questions SET :parametre = :valeur WHERE id = :id';

        foreach ($params as $key=>$value) {
            $stmt=$this->getPDO()->prepare($requete);
            $stmt->execute([
                ':parametre' => $key,
                ':valeur' => $value,
                ':id' => $id
            ]);
        }

    }

    //Update question order when deleting a question
    //Use this instead of classical DELETE FROM function
    public function deleteQuestionUpdateOrder($s_id,$order) {
        $request = "CALL deleteUpdateOrder($s_id, $order);";

        $this->getPDO()->query($request);

    }

    /**
     * @param int user id
     * Select all questions given user created
     */
    public function selectQuestionsByUser($u_id) {
        $request = "SELECT questions.id FROM questions 
                    INNER JOIN sessions on session_id = sessions.id
                    WHERE sessions.user_id = :u_id";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':u_id' => $u_id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $ids = [];
        foreach ($result as $id) {
            $ids[] = $id['id'];
        }
        return $ids;
    }

    public function selectSharedQuestionsByUser($u_id) {
        $request = "SELECT questions.id FROM questions 
                    INNER JOIN sessions on questions.session_id = sessions.id
                    INNER JOIN authorisations ON authorisations.session_id = sessions.id
                    WHERE guest_id = :u_id
                    AND rights = 'write';";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':u_id' => $u_id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $ids = [];
        foreach ($result as $id) {
            $ids[] = $id['id'];
        }
        return $ids;
    }

}
