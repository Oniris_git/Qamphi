<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:07
 */

namespace App\DAL;

use App\BO\Response;
use App\DAL\BDD;
use PDOException;

class ResponseDAO extends BDD
{


    /**
     * @return array Responses
     */
    public function allResponses() {
        $request = 'SELECT * FROM responses';
        $results = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $responses = [];
        foreach ($results as $row) {
            $responses[] = new Response($row);
        }
        return $responses;
    }

    /**
     * @param $q_id
     * @return array
     */
    public function selectResponseByQuestion($q_id){
        $requete = "SELECT * FROM responses WHERE question_id = :q_id";
        $data = [];
        $i=0;

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $q_id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i] = new Response($row);
            $i++;
        }
        return $data;
    }

    /**
     * @param $username
     * @param $questionId
     * @return Response
     */
    public function selectResponseByUser($username, $questionId){

        $request = "SELECT * FROM responses WHERE question_id =  :q_id AND username = :username";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':q_id' => $questionId,
            ':username' => $username
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            return new Response($row);
        }
    }

    /**
     * @param $username
     * @return array
     */
    public function selectListResponsesByUser($username){

        $request = "SELECT * FROM responses WHERE username = :username";
        $data = [];
        $i = 0;

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username'=>$username
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i] = new Response($row);
            $i++;
        }
        return $data;
    }


    /**
     * @param Response $response
     */
    public function insert(Response $response){
        $request = "INSERT INTO responses (question_id, response, username) VALUES (:question_id, :reponse, :username)";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':question_id' => $response->getQuestionId(),
            ':reponse' => $response->getResponse(),
            ':username' => $response->getUserName()
        ]);
}

    /**
     * @param $s_id
     * @return int
     */
    public function countResponse($s_id){

        $requete = 'SELECT count(*) AS COUNT FROM responses WHERE question_id IN(SELECT id FROM questions WHERE session_id = :s_id GROUP BY session_id)';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id'=>$s_id
        ]);
        return $stmt->fetchColumn();
    }

        /**
     * @param $s_id
     * @return int
     */
    public function countResponsesByQuestion($q_id){

        $requete = 'SELECT count(*) AS COUNT FROM responses WHERE question_id = :q_id;';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id'=>$q_id
        ]);
        return $stmt->fetchColumn();
    }

    /**
     * @param $s_id
     */
    public function deleteResponsesBySession($s_id){

        $requete='DELETE FROM responses WHERE question_id IN (SELECT id FROM questions WHERE session_id = :s_id)';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id'=>$s_id
        ]);
    }

    
    /**
     * @param $s_id
     */
    public function deleteResponsesByQuestion($q_id){

        $requete='DELETE FROM responses WHERE question_id = :q_id;';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id'=>$q_id
        ]);
    }

}