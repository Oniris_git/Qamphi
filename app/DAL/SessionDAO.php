<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 09:52
 */

namespace App\DAL;

use App\DAL\BDD;
use PDOException;
use App\BO\Session;

class SessionDAO extends BDD
{

    /**
     * @return array Sessions
     */
    public function allSessions() {
        $request = 'SELECT * FROM sessions';
        $results = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $sessions = [];
        foreach ($results as $row) {
            $sessions[] = new Session($row);
        }
        return $sessions;
    }

    /**
     * @return false|\PDOStatement|string
     */
    public function selectSessions($active){

        $requete = "SELECT * FROM sessions WHERE active = :active";
        $data = [];
        $i=0;

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':active'=>$active
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        if ($result){
            foreach ($result as $row){
                $data[$i]= new Session($row);
                $i++;
            }
            return $data;
        }else{
            return "Erreur dans la séléction des sessions";
        }
    }

    /**
     * @param $user_id
     * @return array
     */
    public function selectSessionsByFilter($filter, $value, $active = null){

        if($active != null){
            $ACTIVE = ' AND active = 1';
        }else{
            $ACTIVE = '';
        }

        $requete = 'SELECT * FROM sessions WHERE '.$filter.' = :value'.$ACTIVE;

        $result = [];
        $i = 0;

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':value'=>$value
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $result[$i] = new Session($row);
            $i++;
        }
        return $result;
    }

    /**
     * @param $s_id
     * @return array
     */
    public function selectUserBySession($s_id){

        $requete = 'SELECT R.username FROM responses R, sessions S, questions Q 
					WHERE S.id = Q.session_id AND Q.id = R.question_id AND S.id = :s_id 
					GROUP BY R.username';

        $data = [];
        $i=0;

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id'=>$s_id
        ]);
        $result=$stmt->fetchAll();
        foreach ($result as $row){
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }

    /**
     * @param $u_id
     * @return array
     */
    public function countSessionsByUser($u_id, $options = []){

        $requete = 'SELECT COUNT(*) FROM sessions WHERE user_id = :u_id;';


        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':u_id'=>$u_id
        ]);
        return $stmt->fetchColumn();
    }

    public function countSharedSessionsByUser($u_id) {
        $requete = 'SELECT COUNT(*) FROM sessions INNER JOIN authorisations ON session_id = sessions.id 
                        WHERE guest_id = :u_id;';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':u_id'=>$u_id
        ]);
        return $stmt->fetchColumn();

    }

    public function countSessionsByParameters($u_id, $filters) {
        $mine = '';
        $shared = '';

        $conditions = '';
        $requete = '';
        if (count($filters['visibility']) == 1) {
            $conditions .= ' AND access = \''.$filters['visibility'][0].'\'';
        }

        if (in_array('mine', $filters['owner'])){
            $requete = '(SELECT COUNT(*) FROM sessions WHERE user_id = :u_id';
            $requete .= $conditions.') ';
        }

        if (in_array('shared', $filters['owner'])) {
            $requete = '(SELECT COUNT(sessions.id) FROM sessions INNER JOIN authorisations ON session_id = sessions.id 
                        WHERE guest_id = :u_id';

            $requete .= $conditions.') ';
        }



        if (in_array('mine', $filters['owner'])
            && in_array('shared', $filters['owner'])) {
            $requete = 'SELECT COUNT(id) FROM (
                        (SELECT sessions.id 
                            FROM sessions WHERE user_id = :u_id'.$conditions.') 
                    UNION 
                    (SELECT sessions.id
                            FROM sessions INNER JOIN authorisations ON session_id = sessions.id 
                        WHERE guest_id = :u_id'.$conditions.')) AS list';
        }

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->bindValue(':u_id', $u_id, \PDO::PARAM_INT);

        $stmt->execute();
        return $stmt->fetchColumn();

    }

    public function selectSessionsByPage($u_id, $page, $sort, $filters) {

        $by_page = $filters['by_page'];

        $mine = '';
        $shared = '';

        $conditions = '';

        if (count($filters['visibility']) == 1) {
            $conditions .= ' AND access = \''.$filters['visibility'][0].'\'';
        }

        $discr = '';

        if (in_array('mine', $filters['owner'])){
            $mine .= '(SELECT id, 
                            user_id, 
                            title, 
                            active,
                            participants, 
                            current, 
                            token, 
                            auth, 
                            access, 
                            null ,
                            null, 
                            null , 
                            \'write\' FROM sessions WHERE user_id = :u_id';
            $mine .= $conditions.') ';
        }

        if (in_array('shared', $filters['owner'])) {
            $discr = 'sessions.';
            $shared .= '(SELECT sessions.id, 
                            user_id, 
                            title, 
                            active,
                            participants, 
                            current, 
                            token, 
                            auth, 
                            access,  
                            authorisations.id, 
                            session_id, 
                            guest_id,
                            rights FROM sessions INNER JOIN authorisations ON session_id = sessions.id 
                        WHERE guest_id = :u_id';

            $shared .= $conditions.') ';
        }



        if (in_array('mine', $filters['owner'])
            && in_array('shared', $filters['owner'])) {
            $mine .= 'UNION ';
            $discr = '';
        }

        $requete = $mine.$shared.' ORDER BY '.$discr.$sort['field'].' '.$sort['order'].' LIMIT :first, :by_page';

        $first = ($page*$by_page)-$by_page;
        $i = 0;

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->bindValue(':u_id', $u_id, \PDO::PARAM_INT);
        $stmt->bindValue(':first', $first, \PDO::PARAM_INT);
        $stmt->bindValue(':by_page', $by_page, \PDO::PARAM_INT);

        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $result[$i] = new Session($row);
            $i++;
        }
        return $result;
    }

    

    /**
     * @param $user_id
     * @param $title
     * @return int
     */
    public function insertSession($user_id, $title, $auth){

        $requete = 'INSERT INTO sessions(user_id, title, active, current,  auth, access) VALUES(:userId, :title, 0, 0, :auth, "public")';

        $stmt = $this->getPDO()->prepare($requete);
        $result = $stmt->execute([
            ":userId" => $user_id,
            ":title" => $title,
            ":auth" => $auth,
        ]);
        $ids = $this->getPDO()->query("SELECT max(id) FROM sessions");
        foreach ($ids as $id){
            return $id;
        }

    }

    /**
     * @param $s_id
     * @param $s_title
     */
    public function updateSession($s_id, $title){

        $requete='UPDATE sessions SET title= :title WHERE id= :s_id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':title' => $title,
            ':s_id' => $s_id
        ]);
    }

    /**
     * @param int session id
     * @param string field to update
     * @param string value
     */
    public function updateByField($s_id, $field, $value){
        $requete='UPDATE sessions SET '.$field.' = :value WHERE id= :s_id';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':value'=>$value,
            ':s_id' => $s_id
        ]);
    }

    /**
     * @param int session id
     * Delete session token
     */
    public function deleteToken($sessionId) {
        $requete='UPDATE sessions SET token = NULL WHERE id= :s_id';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id' => $sessionId
        ]);
    }

    /**
     * @param $s_id
     */
    public function deleteSession($s_id){

        $requete = 'DELETE FROM sessions WHERE id= :s_id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id' => $s_id
        ]);
    }

    /**
     * @param $s_id
     */
    public function countParticipantsBySession($s_id) {
        $request = 'SELECT COUNT(DISTINCT username) FROM responses
                    INNER JOIN questions ON question_id=questions.id
                    INNER JOIN sessions ON session_id=sessions.id
                    WHERE sessions.id= :s_id';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':s_id' => $s_id
        ]);
        return $stmt->fetchColumn();
    }


    /**
     * @param int session id     
     */
    public function incrementParticipants($sessionId){
        $requete = "UPDATE sessions SET participants = participants + 1 WHERE id = :s_id";

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':s_id' => $sessionId
        ]);
    }

    /**
     * @param int session id
     * @param string auth
     */
    public function setAuth($s_id, $auth) {
        $requete = "UPDATE sessions SET auth = :auth WHERE id = :s_id";

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':auth' => $auth,
            ':s_id' => $s_id,
        ]);
    }

    /**
     * @param int user id
     * Select last session given user created
     */
    public function lastSessionByUser($u_id){
        $request = 'SELECT MAX(id) FROM sessions WHERE user_id= :u_id';

        $stmt=$this->getPDO()->prepare($request);
        $stmt->execute([
            ':u_id' => $u_id
        ]);
        return $stmt->fetchColumn();
    }

    /**
     * @return array all tokens
     */
    public function allTokens() {
        $request = 'SELECT token FROM sessions;';
        $data = [];

        $result = $this->getPDO()->query($request);
        foreach ($result as $row){
            $data[] = $row['token'];
        }
        return $data;
    }

    public function sharedSharedSessionsByUser($user_id) {
        $request = 'SELECT * FROM sessions INNER JOIN authorisations ON session_id = sessions.id
                                            WHERE guest_id = :user_id;';

        $result = [];
        $i = 0;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':user_id' => $user_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $row['id'] = $row['session_id'];
            $result[$i] = new Session($row);
            $i++;
        }
        return $result;
    }

    public function sharedWritingSessionsByUser($user_id) {
        $request = 'SELECT * FROM sessions INNER JOIN authorisations ON session_id = sessions.id
                                            WHERE guest_id = :user_id
                                            AND rights = \'write\';';

        $result = [];
        $i = 0;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':user_id' => $user_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $row['id'] = $row['session_id'];
            $result[$i] = new Session($row);
            $i++;
        }
        return $result;
    }
}
