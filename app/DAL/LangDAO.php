<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 22/05/2019
 * Time: 11:13
 */

namespace App\DAL;


class LangDAO extends BDD
{

    /**
     * @return array all langs
     */
    public function selectAllLang(){
        $request = 'SELECT * FROM lang';
        $data = [];

        $result = $this->getPDO()->query($request);
        foreach ($result as $row){
            $data[$row['name']] = $row['content'];
        }
        return $data;

    }

    public function langArray() {
        $request = 'SELECT * FROM lang';
        $data = [];

        $result = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $data= [];
        foreach ($result as $row){
            $data[] = $row;
        }
        return $data;

    }

    /**
     * @param string name
     * @return array lang
     */
    public function selectLangByField($name){
        $request = 'SELECT * FROM lang WHERE name = :name';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':name' => $name
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            return $row;
        }

    }

    /**
     * @param string name
     * @param string new content     
     */
    public function updateLangByField($name, $newField){
        $request = 'UPDATE lang SET content = :content WHERE name = :name';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':name' => $name,
            ':content' => $newField
        ]);

    }

    /**
     * @param string lang code name
     * @param string lang name
     */
    public function addLang($name, $content) {
        $request = 'INSERT INTO `lang`(`name`, `content`) VALUES (:name, :content)';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':name' => $name,
            ':content' => $content
        ]);

    }

}