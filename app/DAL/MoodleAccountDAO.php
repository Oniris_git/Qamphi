<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 21/05/2019
 * Time: 10:07
 */

namespace App\DAL;

use App\BO\User;
use PDO;
use App\DAL\BDDMoodle;
use Vespula\Auth\Adapter\AdapterInterface;
use Vespula\Auth\Exception;

class MoodleAccountDAO extends BDDMoodle implements AdapterInterface
{

    /**
     * validate the username and password.
     *
     * @param array $credentials Array with keys 'username' and 'password'
     * @return boolean
     * @throws Exception
     */
    public function authenticate(array $credentials)
    {
        $request = "SELECT * FROM mdl_user WHERE username = :username";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username'=> $credentials['username']
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){}
        if(!empty($row)){
            $check = password_verify( $credentials['password'], $row['password']);
            if($check == false){
                throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
            }
            return $check;
        }else{
            throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
        }


    }

    /**
     * Find extra userdata. This will be stored in the session
     *
     * @param $username
     * @return User Userdata specific to the adapter
     */
    public function lookupUserData($username)
    {
        $request = "SELECT * FROM mdl_user WHERE username = :username";
        $role = 'student';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $username
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $user = [
                'id' => $row['id'],
                'email' => $row['email'],
                'username' => $row['username'],
                'auth' => 'moodle',
                'role' => $role,
                'password' => $row['password'],
            ];
            return new User($user);
        }
    }

    /**
     * Get the most recent error for debugging purposes
     *
     * @return string Error (should be a constant)
     */
    public function getError()
    {
        return "Error";
    }

    public function selectUserByName($input){

        $request = 'SELECT * FROM mdl_user WHERE username LIKE :input AND deleted="0"';

        $data = [];
        $i=1;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':input' => "%$input%"
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i] = $user = [
                'email' => $row['email'],
                'username' => $row['username'],
                'auth' => 'moodle',
                'role' => 'student',
                'password' => $row['password'],
            ];
            $i++;
        }
        return $data;

    }

}
