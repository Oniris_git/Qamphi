<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 16:27
 */

namespace App\DAL;


use App\BO\User;
use mysql_xdevapi\Exception;
use PDO;
use PDOException;

class UserDAO extends BDD
{

    /**
     * @return array Users
     */
    public function allUsers() {
        $request = 'SELECT * FROM users';
        $results = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $users = [];
        foreach ($results as $row) {
            //if ($row['auth'] != 'lock') {
                $users[] = new User($row);
            //}
        }
        return $users;
    }

    /**
     * @param User
     */
    public function insertUser(User $user){
        $request = 'INSERT INTO users (username, email, auth, role, password, request, open, token) VALUES (:username, :email, :auth, :role, :password, :request, :open, :token)';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $user->getUsername(),
            ':email' => $user->getEmail(),
            ':auth' => $user->getAuth(),
            ':role' => $user->getRole(),
            ':password' => $user->getPassword(),
            ':request' => $user->getRequest(),
            ':open' => $user->getOpen(),
            ':token' => $user->getToken(),
        ]);
    }

    /**
     * @param string role
     * @return array users
     */
    public function selectUsersByType($type){

        if($type == 'admin'){
            $ADD = ' OR auth = "lock"';
        }else{
            $ADD = '';
        }
        $request = 'SELECT * FROM users WHERE role = :usertype'.$ADD.' ORDER BY auth ASC';

        $data = [];
        $i = 1;

        $stmt=$this->getPDO()->prepare($request);
        $stmt->execute([
            ':usertype'=>$type
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i] = new user($row);
            $i++;
        };

        return $data;
    }

    /**
     * @param string username
     * @return bool exists or not
     */
    public function userExistByName($username){
        $request = 'SELECT * FROM users WHERE username = :username';

        $stmt =  $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $username
        ]);
        $chk_pseudo = $stmt->fetch(PDO::FETCH_ASSOC);
        if($chk_pseudo == '1' || $chk_pseudo > '1'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param User
     */
    public function deleteUser(User $user){
        $request = 'UPDATE users SET username = :username, password = NULL, role = "undefined" WHERE id = :id;';
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $user->getUsername().time(),
            ':id' => $user->getId()
        ]);
    }

    /**
     * @param int id
     * @return User
     */
    public function selectUserById($id){
       $request = 'SELECT * from users WHERE id = :id';

       $stmt = $this->getPDO()->prepare($request);
       $stmt->execute([
           ':id' => $id
       ]);
       $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
       foreach ($result as $row){
           return new User($row);
       }
    }

    /**
     * @param string username
     * @return User
     */
    public function selectUserByUsername($search){
       $request = 'SELECT * from users WHERE username = :searchUsername OR email = :searchEmail';

       $stmt = $this->getPDO()->prepare($request);
       $stmt->execute([
           ':searchUsername' => $search,
           ':searchEmail' => $search,
       ]);
       $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
       foreach ($result as $row){
           return new User($row);
       }
    }

    public function searchUserByName($input){

        $request = 'SELECT * FROM users WHERE username LIKE :input';

        $data = [];
        $i=1;
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':input' => "%$input%"
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] =  new User($row);
        }
        return $data;
    }

    /**
     * @param User
     */
    public function updateUser(User $user){

        $request = 'UPDATE users SET username = :username, email= :email, auth = :auth, role = :role, password = :password, request = :request, open = :open, token = :token WHERE id = :id';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $user->getUsername(),
            ':email' => $user->getEmail(),
            ':auth' => $user->getAuth(),
            ':role' => $user->getRole(),
            ':password' => $user->getPassword(),
            ':request' => $user->getRequest(),
            ':open' => $user->getOpen(),
            ':token' => $user->getToken(),
            ':id' => $user->getId()
        ]);
    }

    /**
     * @param string token
     * @return User
     */
    public function selectUserByToken($token){
        $request = 'SELECT * FROM users WHERE token = :token AND open = 1 AND request';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':token' => $token
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            return new User($row);
        }
    }

    /**
     * @param int user id
     * @param string role
     */
    public function changeProfile($userId, $role){
        $request = "UPDATE users SET role = :role WHERE id = :id";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ":role" => $role,
            ":id" => $userId
        ]);

    }

    //Stats
    /**
     * @param user id
     * @return array Sessions
     */
    public function countSessionsByUser($userId) {
        $request = "SELECT COUNT(*) FROM sessions WHERE user_id = :id";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $userId
        ]);
        return $stmt->fetchColumn();
    }

    /**
     * @param int user id
     * @return array Questions
     */
    public function countQuestionsByUser($userId) {
        $request = "SELECT COUNT(*) FROM questions 
        INNER JOIN sessions ON session_id = sessions.id    
        WHERE sessions.user_id = :id";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $userId
        ]);
        return $stmt->fetchColumn();
    }


}
