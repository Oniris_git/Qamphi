<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 13/12/2018
 * Time: 10:13
 */

namespace App\DAL;

use PDO;

abstract class BDD extends PDO{

    private $db_host;
    private $db_user;
    private $db_password;
    private $db_name;
    private $db_type;
    private $pdo;

    public function __construct(array $connector){
        $this->db_name= $connector['db_name'] ;
        $this->db_user= $connector['db_user'];
        $this->db_password= $connector['db_password'];
        $this->db_host= $connector['db_host'];
        $this->db_type= $connector['db_type'];
    }

    public function getPDO(){
        if(!isset($this->pdo)){
            $this->pdo = new PDO($this->db_type.':host=' . $this->db_host .
                ';dbname=' .$this->db_name,
                $this->db_user,
                $this->db_password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $this->pdo;
    }

    public function tableExists($table) {
        try {
            $result = $this->getPDO()->query("SELECT 1 FROM {$table} LIMIT 1");
        } catch (\PDOException $e) {
            // We got an exception (table not found)
            return false;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return true;
    }

    public function truncate($table) {

        $this->getPDO()->query("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE $table; SET FOREIGN_KEY_CHECKS = 1;");

        return true;
    }
}
