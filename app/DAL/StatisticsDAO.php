<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 09:52
 */

namespace App\DAL;

use App\DAL\BDD;

class StatisticsDAO extends BDD
{

    public function notify($action) {
        
        $month = date('m');
        $year = date('Y');

        if ($this->month_exist()) {
            $request = 'UPDATE statistics SET '.$action.' = '.$action.' + 1 WHERE month = "'.$month.'" AND year = '.$year.';';
        } else {
            $request = 'INSERT INTO statistics ('.$action.', month, year) VALUES (1, "'.$month.'", '.$year.');';
        }
        $this->getPDO()->query($request);
        return true;
    }
    
    private function month_exist() {
        $month = date('m');
        $year = date('Y');
        $request = 'SELECT * FROM statistics WHERE month = :month AND year = :year;';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':month' => $month,
            ':year' => $year
        ]);

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return is_array($result);
    }

    public function stats_this_month() {
        $month = date('m');
        $year = date('Y');
        $request = 'SELECT * FROM statistics WHERE month = :month AND year = :year;';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':month' => $month,
            ':year' => $year
        ]);

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function all_stats() {
        $year = date('Y');
        $request = 'SELECT * FROM statistics WHERE year = '.$year.' ORDER BY month ASC;';

        return $this->getPDO()->query($request)->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function stats_all_time() {
        $year = date('Y');
        $request = 'SELECT * FROM statistics;';

        return $this->getPDO()->query($request)->fetchAll(\PDO::FETCH_ASSOC);
    }
}