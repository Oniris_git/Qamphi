<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 13:58
 */

namespace App\DAL;
use App\BO\User;
use PDO;
use Vespula\Auth\Adapter\AdapterInterface;
use App\DAL\BDD;
use PDOException;
use Vespula\Auth\Exception;


class ManualAccountDAO extends BDD implements AdapterInterface
{

    /**
     * validate the username and password.
     *
     * @param array $credentials Array with keys 'username' and 'password'
     * @return boolean
     * @throws Exception
     */
    public function authenticate(array $credentials)
    {
        $request = 'SELECT * FROM users WHERE (auth = "manual" OR auth = "lock") AND username = :username';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $credentials['username'],
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $userObj = [];
        foreach ($result as $user){
            $userObj = new User($user);
        }
        if(!empty($userObj)){
            $check = password_verify( $credentials['password'], $userObj->getPassword());
            if($check == false){
                throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
            }
            return $check;
        }else{
            throw new Exception("false");
        }

    }

    /**
     * Find extra userdata. This will be stored in the session
     *
     * @param $username
     * @return User Userdata specific to the adapter
     */
    public function lookupUserData($username)
    {
        $request = 'SELECT * FROM users WHERE username = :username';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $username
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            return new User($row);
        }

    }

    /**
     * Get the most recent error for debugging purposes
     *
     * @return string Error (should be a constant)
     */
    public function getError()
    {
        return "Error";
    }
}
