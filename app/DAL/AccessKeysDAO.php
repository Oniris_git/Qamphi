<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 16:27
 */

namespace App\DAL;


use App\BO\User;
use mysql_xdevapi\Exception;
use PDO;
use PDOException;
use App\BO\AccessKey;

class AccessKeysDAO extends BDD
{

    /**
     * @return array question types
     */
    public function allKeys() {
        $request = 'SELECT * FROM access_keys;';
        $data = [];
        $result = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] = $row;
        }
        return $data;
    }

    /**
     * @return array all private tokens as string array
     */
    public function selectTokens() {
        $request = 'SELECT token FROM access_keys;';
        $data = [];

        $result = $this->getPDO()->query($request);
        foreach ($result as $row){
            $data[] = $row['token'];
        }
        return $data;

    }

    /**
     * @param int sessionId
     * @return array tokens for given session as a string array
     */
    public function selectTokensBySession($sessionId) {
        $request = "SELECT token FROM access_keys WHERE session_id = :id";
        $data = [];

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $sessionId
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $data[] = $row;
        }

        return $data;
    }

    /**
     * @param int sessionId
     * Delete all private tokens for the given session
     */
    public function deleteKeysBySession($sessionId) {
        $request = 'DELETE FROM access_keys WHERE session_id= :s_id';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':s_id' => $sessionId
        ]);

    }

    /**
     * @param array arrays of randomly generated tokens
     * @param int sessionId
     */
    public function insertKeys($keys, $sessionId) {

        $request = 'INSERT INTO access_keys(token, session_id) VALUES ';

        foreach ($keys as $key => $value) {            
            $request .= "('$value', '$sessionId')";
            if ($key != count($keys)-1 ) {
                $request .= ', ';
            }
        }

        $this->getPDO()->query($request);


    }

    /**
     * @param int sessionId
     * @return int number of keys for given session
     */
    public function countKeysBySession($sessionId) {
        $request = "SELECT COUNT(*) FROM access_keys WHERE session_id = :id";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $sessionId
        ]);
        $result = $stmt->fetchColumn();

        return $result;
    }

    /**
     * @param int sessionId
     * @param int number of keys to delete
     */
    public function deleteSomeBySession($sessionId, $limit) {
        $request = 'DELETE FROM access_keys WHERE session_id= :s_id LIMIT '.$limit;

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':s_id' => $sessionId
        ]);

    }

    /**
     * @param string token
     * @return int session id
     */
    public function selectSessionByToken($token) {
        $requete = 'SELECT session_id FROM access_keys WHERE token = :value';

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':value'=>$token
        ]);
        $result = $stmt->fetchColumn();
        return $result;

    }

    /**
     * @param string token to delete
     */
    public function deleteByToken($token) {
        $request = 'DELETE FROM access_keys WHERE token= :token';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':token' => $token
        ]);

    }
}