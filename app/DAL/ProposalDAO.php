<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:07
 */

namespace app\DAL;

use App\BO\Proposal;
use App\DAL\BDD;
use PDOException;

class ProposalDAO extends BDD
{

    /**
     * @return array Proposals
     */
    public function allProposals() {
        $request = 'SELECT * FROM proposals';
        $results = $this->getPDO()->query($request, \PDO::FETCH_ASSOC);
        $proposals = [];
        foreach ($results as $row) {
            $proposals[] = new Proposal($row);
        }
        return $proposals;
    }

    /**
     * @param $q_id
     * @return false|\PDOStatement|string
     * @throws \Exception
     */
    public function selectProposalByQuestion($q_id){
        $requete = "SELECT * FROM proposals WHERE question_id = :q_id ORDER BY p_order";
        $i = 0;
        $data = [];

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $q_id
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            $data[$i] = new Proposal($item);
            $i++;
        }
        return $data;
    }

    /**
     * @param $q_id
     * @return false|string
     * @throws \Exception
     */
    public function selectProposalNotNullByQuestion($q_id){
        $requete = "SELECT * FROM proposals WHERE question_id = :q_id ORDER BY p_order";
        $i = 0;
        $data = [];

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $q_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            if($item['p_text'] != ""){
                $data[$i] = new Proposal($item);
                $i++;
            }
        }
        return $data;

    }

    /**
     * @param $q_id
     * @param $p_order
     * @param $p_text
     * @param $correct
     * @throws \Exception
     */
    public function insert($q_id, $p_order, $p_text, $correct){

        $requete = 'INSERT INTO proposals(question_id, p_order, p_text, correct) VALUES(:q_id, :p_order, :p_text, :correct)';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $q_id,
            ':p_order' => $p_order,
            ':p_text' => $p_text,
            ':correct' => $correct,
        ]);

    }

    /**
     * @param $q_id
     * @param $proposal
     */
    public function updateProposal($q_id, $proposal){

        $requete = 'UPDATE proposals SET p_text = :p_text, correct = :correct WHERE question_id = :question_id AND p_order= :p_order';
        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':p_text' => $proposal->getContent(),
            ':correct' => $proposal->getCorrect(),
            ':question_id' => $q_id,
            ':p_order' => $proposal->getOrder()
        ]);
    }

    /**
     * @param Proposal to update
     */
    public function update($proposal) {
        $requete = 'UPDATE proposals SET p_text = :p_text, correct = :correct, p_order = :p_order WHERE id = :id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':p_text' => $proposal->getContent(),
            ':correct' => $proposal->getCorrect(),
            ':p_order' => $proposal->getOrder(),
            ':id' => $proposal->getId(),

        ]);

    }

    /**
     * @param $qId
     * @return array
     */
    public function selectCorrectProposalByQuestion($qId){

        $request = "SELECT * FROM proposals WHERE question_id = :q_id AND correct = 1";
        $data = [];
        $i = 0;

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':q_id' => $qId
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[$i] = new Proposal($row);
            $i++;
        }
        return $data;

    }

    /**
     * @param $idQuestion
     * @return mixed
     */
    public function selectProposalNotEmpty($idQuestion){

        $requete = "SELECT * FROM proposals WHERE question_id = :q_id AND p_text IS NOT NULL ORDER BY p_order";
        $i = 0;
        $data= [];

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $idQuestion
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            if($item['p_text'] != ""){
                $data[$i] = new Proposal($item);
                $i++;
            }

        }
        return $data;
    }

    /**
     * @param int question id
     * @param int proposal order
     * @return array Proposal
     */
    public function selectByOrder($idQuestion, $order) {

        $requete = "SELECT * FROM proposals WHERE question_id = :q_id AND p_order = :p_order";
        $i = 0;
        $data= [];

        $stmt=$this->getPDO()->prepare($requete);
        $stmt->execute([
            ':q_id' => $idQuestion,
            ':p_order' => $order
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            if($item['p_text'] != ""){
                $data[$i] = new Proposal($item);
                $i++;
            }
        }
        return $data;
    }

    /**
     * @param int proposal id
     */
    public function deleteProposal($p_id){

        $requete = 'DELETE FROM proposals WHERE id= :p_id';

        $stmt = $this->getPDO()->prepare($requete);
        $stmt->execute([
            ':p_id' => $p_id
        ]);

    }

    public function proposalDeleteUpdate($q_id,$order) {
        $request = "CALL proposalDelete($q_id, $order);";

        $this->getPDO()->query($request);

    }

    public function updateOrder() {
        $request = 'UPDATE proposals
                        SET p_order = p_order-1
                        WHERE p_order > :deleted_p_order
                    AND question_id=:q_id;';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([

        ]);

    }

    /**
     * @param Proposal
     * @return int id
     */
    public function getId($proposal) {
        $request = 'SELECT id FROM proposals WHERE question_id = :qId AND p_order = :pOrder';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':qId' => $proposal->getQuestionId(),
            ':pOrder' => $proposal->getOrder(),
        ]);
        return $stmt->fetchColumn();

    }

}