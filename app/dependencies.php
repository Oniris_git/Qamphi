<?php

require_once 'Autoloader.php';
include __DIR__.'/../public/Controller/ControllerFunctions/langFunctions.php';

use App\Autoloader;
use App\DAL\ManualAccountDAO;
use App\DAL\MoodleAccountDAO;
use App\DAL\ConfigDAO;
use App\DAL\SessionDAO;
use App\DAL\LangDAO;
use App\Session\Session;
use League\Container\Container;
use Vespula\Auth\Auth;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


$qamphi = str_replace($_SERVER['DOCUMENT_ROOT'],'',__DIR__);
$qamphi = str_replace('/app', '', $qamphi);
$URL = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$qamphi;

if (!file_exists(__DIR__.'/../config/Config.php') || !file_exists(__DIR__.'/../config/ConfigSMTP.php')) {
    header('Location: '.$URL.'/public/install/lang.php');
    die;
}


$args = [
    'language' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

Autoloader::register();

//config.php instance
$config= App\Config::getInstance();

//DB connector
$connector=[
    'db_name'=>$config->get('db_name'),
    'db_host'=>$config->get('db_host'),
    'db_password'=>$config->get('db_password'),
    'db_user'=>$config->get('db_user'),
    'db_type'=>$config->get('db_type'),
];

$configDAO = new ConfigDAO($connector);
$langDAO = new LangDAO($connector);

try {//display errors
    if ($configDAO->selectConfigByType('general', 'environment')['value'] == 'dev') {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
    $moodle = $configDAO->selectConfigByType('auth', 'moodle')['value'];
} catch (PDOException $e) {
//    log_error($e, $logger, 'dependencies', $session->getUserdata(), $backLink);
//    dump($e);
    echo $e->getMessage();
    die();
}


if($moodle == 'on'){
    //Moodle DB connector
    $connectorMoodle=[
        'db_name'=>$config->get('db_nameMoodle'),
        'db_host'=>$config->get('db_hostMoodle'),
        'db_password'=>$config->get('db_passwordMoodle'),
        'db_user'=>$config->get('db_userMoodle'),
        'db_type'=>$config->get('db_typeMoodle'),
    ];
}

//we load lang table into $LANG to access everywhere

//$LANG = $langDAO->selectAllLang();


$container = new Container();
$container->add('session', new Session());


$container->add('adapter', function () use ($connector) {
    $manual = new ManualAccountDAO($connector);
    return $manual;
});



$container->add('logger', function () {
    $logger = new Logger('qamphi');
    $logger->pushHandler(new StreamHandler((__DIR__.'/../public/tmp/error.log'), Logger::WARNING));

    return $logger;
});

try {
    $session = $container->get('session');
    $adapter = $container->get('adapter');
    $logger = $container->get('logger');
    $auth = new Auth($adapter, $session);
} catch (Exception $e) {
    $ERROR = [
        'message' => $LANG['parameter'].' : '.$e->getMessage()
    ];
}

//langs settings
try {
    $lang = $configDAO->selectConfigByType('general', 'lang')['value'];
    $allLangs = $langDAO->selectAllLang();
} catch (PDOException $e) {
    $ERROR = [
        'message' => $e->getMessage()
    ];
}
$langCodes = [];
foreach ($allLangs as $key => $value) {
    $langCodes[] = $key;
}

//background link
try {
    $backLink = $configDAO->selectConfigByType('view', 'background')['value'];
} catch (PDOException $e) {
    $ERROR = [
        'message' => $e->getMessage()
    ];
}

$user_prefs = $session->getUserPreferences();

//$prefLang = $session->getLang();


if (array_key_exists('lang', $user_prefs)) {
    $lang = $user_prefs['lang'];
}

if (isset($POST['language']) && in_array($POST['language'], $langCodes)) {
    $lang = $POST['language'];
}
require __DIR__.'/../config/lang/'.$lang.'.php';