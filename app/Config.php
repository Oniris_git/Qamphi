<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 13/12/2018
 * Time: 09:52
 */

namespace App;


class Config{

    private $settings =[];
    private $SMTP=[];
    private $version;
    private static $_instance;

    /**
     * @return self
     */
    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new Config();
        }
        return self::$_instance;
    }

    public function __construct(){
        $this->settings = require dirname(__DIR__) . '/config/Config.php';
        $this->SMTP = require dirname(__DIR__) . '/config/ConfigSMTP.php';
        $this->version = require dirname(__DIR__) . '/config/Version.php';

    }

    public function get($key){
        if(!isset($this->settings[$key])){
            return null;
        }

        return $this->settings[$key];
    }

    public function getSMTP($key){
        if(!isset($this->SMTP[$key])){
            return null;
        }

        return $this->SMTP[$key];
    }

    public function getVersion() {
        return $this->version;
    }

}
