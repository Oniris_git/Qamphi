<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 13/12/2018
 * Time: 09:53
 */

namespace App;


class Autoloader{
    static function register() {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    static function autoload($class){
        $class = str_replace('App\Controller\controller','/Controller/controller',$class);
        $class = str_replace('App\DAL','/DAL',$class);
        $class = str_replace('App\HTML','HTML',$class);
        $class = str_replace('App\Install','Install',$class);
        $class = str_replace('App\Autoloader','/Autoloader',$class);
        $class = str_replace('App\Session\Session','/Session/Session',$class);
        $class = str_replace('App\Helper\Config','/Helper/Config',$class);
        $class = str_replace('App\BO','/BO',$class);
        $class = str_replace('\Questions','/Questions',$class);
        $class = str_replace('App\Config','/Config',$class);
        $lastRefactor = __DIR__ . '/' . $class.'.php';
        $lastRefactor = str_replace("app//DAL\\",'app/DAL/',$lastRefactor);
        $lastRefactor = str_replace("app//BO\\",'app/BO/',$lastRefactor);
        $lastRefactor = str_replace("app/HTML",'HTML',$lastRefactor);
        $lastRefactor = str_replace('\\', '/', $lastRefactor);
        require $lastRefactor;
    }
}
