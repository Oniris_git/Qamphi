<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:56
 */

namespace App\BO;

class Proposal
{

    protected $id;
    protected $question_id;
    protected $p_order;
    protected $p_text;
    protected $correct;
    private $answers;

    public function __construct($data)
    {
        if(isset($data['id'])){
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }

        $this->question_id = $data['question_id'];
        $this->p_order = $data['p_order'];
        $this->p_text = $data['p_text'];
        $this->correct = $data['correct'];        
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * @param mixed $questionId
     */
    public function setQuestionId($questionId)
    {
        $this->question_id = $questionId;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->p_order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->p_order = $order;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->p_text;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->p_text = $content;
    }

    /**
     * @return mixed
     */
    public function getCorrect()
    {
        return $this->correct;
    }

    /**
     * @param mixed $correct
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;
    }
    
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }
    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        $attributes = get_object_vars($this);
        unset($attributes['answers']);
        return $attributes;
    }

    public function display($isStatsPage = false)
    {
        if($this->getContent() === '') {
            return;
        }
        $class = ($isStatsPage === false) ? 'col-9' : 'col-4';
        echo '<div class="row justify-content-center marginLaunch marginLeft" >
            <div class="col-1 sessionFont">
                ' . $this->getOrder() . '-
            </div>
            <div class="'.$class.' sessionFont">
            ' . $this->getContent() . '
            </div>';
        if ($isStatsPage === false) {            
            echo '</div>';
        }
    }

    public function displayStats($total)
    {
        $this->display(true);
        $retour = '<div class="col-2 sessionFont">
        ' . $this->answers . '
        </div>
        <div class="col-3">';
        if ($total != 0) {
            $pourcent = number_format($this->answers / $total * 100, 0);
            if ($pourcent === 'nan'): $pourcent = 1; endif;
            $bgcolor = ($this->correct == 0) ? 'red1' : 'green1';
            $retour .= '<div class="sessionFont" style="width:' . $pourcent . '% ;
                                    background-image: url(\'image/' . $bgcolor . '.jpg\');
                                    background-repeat: no-repeat;background-size:cover;" 
                                    border=0 
                                    cellspacing=0>&nbsp;
                       </div>';
        }
            $retour .= '</div>
        </div>';

    echo $retour;
    }

    /**
     * Proposal edit form
     * @param bool $has_correct_answers. true (default) if teacher wants to specify what proposals are corrects are not. Ex : false for polls
     */    
    public function edit($questionOrder, $has_correct_answers = true)
    {
        $class = $this->getContent() == "" ? "disabled-check" : "";
        $retour = '<div class="row justify-content-end propo align-items-center">
                        <div class="col-1" id="shootMarges" style="text-align: end;font-size:25px">
                            ' . $this->getOrder() . ' - 
                        </div>
                        <div class="col-10">
                            <input size="128"';
                        if($this->getOrder() == 1 || $this->getOrder() == 2 && $this->getContent() == ""){
                            $retour .=  " required";
                        }
                        $retour .=  ' maxlength="128" id="Q' . $questionOrder . 'P' . $this->getOrder() . '" value="' . $this->getContent() . '" class="form-control proposal" type="text" name="Q' . $questionOrder . 'P' . $this->getOrder() . '" oninput="enable()">
                            </div>
                        <div class="col-1">';
        if ($has_correct_answers === true) {
                if ($this->getCorrect() == 0):
                    $retour .= '<input id="check'.$questionOrder.'id'.($this->getOrder()-1).'" type="hidden" name="Q' . $questionOrder . 'C' . $this->getOrder() . '" value="' . $this->getCorrect() . '">
                                <i id="iconeCheck'.$questionOrder.'id'.($this->getOrder()-1).'" class="fas fa-times '.$class.'" style="cursor: pointer;color: #ff5353;font-size:1.5em;" onclick="changeValue('.$questionOrder.','.($this->getOrder()-1).')"></i>';
                else:
                    $retour .= '<input id="check'.$questionOrder.'id'.($this->getOrder()-1).'" type="hidden" name="Q' . $questionOrder . 'C' . $this->getOrder() . '" value="' . $this->getCorrect() . '">
                                <i id="iconeCheck'.$questionOrder.'id'.($this->getOrder()-1).'" class="fas fa-check" style="cursor: pointer;color: #32c032;font-size:1.5em;" onclick="changeValue('.$questionOrder.','.($this->getOrder()-1).')"></i>';
                endif;
        }
        $retour .='</div>
                </div>';
        echo $retour;
    }
}