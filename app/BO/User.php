<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 16:03
 */

namespace App\BO;


class User
{

    private $id;
    private $username;
    private $email;
    private $auth;
    private $role;
    private $password;
    private $request;
    private $open;
    private $token;

    /**
     * User constructor.
     * @param $data
     */
    public function __construct($data)
    {
        if(isset($data['id'])){
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }
        $this->email = $data['email'];
        $this->username = $data['username'];
        $this->auth = $data['auth'];
        $this->role = $data['role'];

        if(isset($data['password'])){
            $this->password = $data['password'];
        }
        if(isset($data['open'])){
            $this->open = $data['open'];
        }
        if(isset($data['request'])){
            $this->request = $data['request'];
        }
        if(isset($data['token'])){
            $this->token = $data['token'];
        }


    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $upn
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param mixed $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @param mixed $open
     */
    public function setOpen($open)
    {
        $this->open = $open;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $role
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        return get_object_vars($this);
    }

}
