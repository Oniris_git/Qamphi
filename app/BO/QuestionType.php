<?php


namespace App\BO;


class QuestionType {

    private $id;
    private $name;


    public function __construct($data) 
    {
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }
        $this->name = $data['name'];        
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        return get_object_vars($this);
    }




}