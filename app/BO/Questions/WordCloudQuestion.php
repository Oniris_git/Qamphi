<?php

namespace App\BO\Questions;
use App\BO\Questions\Question;
use App\BO\Session;
use App\BO\Response;
use Exception;

class WordCloudQuestion extends Question
{

    const ICON = '<i class="fas fa-cloud fa-3x"></i>';

    public function display()
    {
        parent::display();

        $previousQuestion = $this->getOrder() - 1;

        $retour = '<div class="row justify-content-md-center">';

        if ($previousQuestion != 0) {
            $retour .= '	<div class="col-4">
                        <form method="post" action="index.php?page=launch_session">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $previousQuestion . '">
                            <button type="submit" class="btn btn-outline-secondary width100 margin_bottom_btn_submit">'.$this->lang['previous'] .'</button>
                        </form>
					</div>';
        }

        $retour .= '    <div class="col-4">
                        <form action="index.php?page=launch_session" method="post">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $this->getOrder() . '">
                            <button type="submit" class="btn btn-outline-success width100 margin_bottom_btn_submit" name="request" value="response">'.$this->lang['responses'] .'</button>
                        </form>
                    </div>
                </div>';


        $retour .= '<br>';

        echo $retour;
    }

    function formContent()
    {
        return;
    }

    public function constructResponse($postdata) {
        if (!isset($postdata['response']) || $postdata['response'] === '') {
            throw new Exception('noResponse');
        }
        return $postdata['response'];
    }

    public function displayStats(array $responses)
    {
        parent::displayStats([]);
        // tableau : mot => nombre d'occurences
        $words = [];
        foreach ($responses as $response) {
            if (isset($words[$response->getResponse()])) {
                $words[$response->getResponse()]++;
            } else {
                $words[$response->getResponse()] = 1;
            }
        }
        arsort($words);
        echo '<table class="table table-hover">';
        $i = 0;
        foreach ($words as $key => $value) {
            echo '<tr>
                    <td class="sessionFont">'.$key.'</td>
                    <td class="sessionFont">'.$value.'</td>
                </tr>';
            $i++;
            if ($i >= 5) {
                break;
            }
        }
        echo '</table>';
    }

    public function displayDetailedStats($total, array $responses = [])
    {
        $this->displayTitle($total);
        // Word cloud
        $words = [];
        foreach ($responses as $response) {
            if (isset($words[$response->getResponse()])) {
                $words[$response->getResponse()]++;
            } else {
                $words[$response->getResponse()] = 1;
            }
        }
        echo '<div id="word-cloud-container">';
        $i = 0;
        foreach ($words as $key => $value) {
            echo '<span class="randomnumber" style="font-size:'.($value*20).'px">&nbsp'.$key.'&nbsp</span>';
            if ($i % 6 == 0) {
                echo '</div>
                <div id="word-cloud-container">';
            }
            $i++;
        }
        echo '</div>';
    }


    public function displayResponse(Response $response)
    {
        $remote_display = $this->lang['response_exist']."&nbsp;".$response->getResponse();
        $remote_display .= "<div class='row shoot_margin_lateral' id='propoStyle'>";
        $remote_display .= "<div class='col-6'>
                                <button class='propoStyle' disabled>X</button>
                            </div>
                        </div>";

        echo $remote_display;
    }


    public function displayForm($username)
    {
        $nextQuestion = $this->getOrder() + 1;
        $previousQuestion = $this->getOrder() - 1;

        $remote_display = "<div class='row shoot_margin_lateral' id='propoStyle'>";

        $remote_display .= "<input type='text' id='wc-input' name='response' class='zindex5'>";

        $remote_display .= "<input type='hidden' value='".$this->getSessionId()."' name='idSession'>
                                     <input type='hidden' value='".$this->getId()."' name='idQuestion'>
                                     <input type='hidden' value='".$nextQuestion."' name='nextQuestion'>
                                     <input type='hidden' value='".$username."' name='username'>                                     
                            </div>
                            <button class='propoStyle btn-confirm fixed-bottom' type='submit' name='valid' id='bV'>Ok</button>
                            </table>                            
                         </form>";
        echo $remote_display;
    }

    public function displaySummary(Response $response)
    {
        $retour = "<div id='questionTitle'>".$this->lang['question_number']."&nbsp;".$this->getOrder()."</div>
                        <div class='titleFont responseSummary'>                    
                        ".$this->lang['responses_summary']."&nbsp;".$response->getResponse()."
                    </div>";
        echo $retour;
    }

    public function statsFromData(Session $session, array $responses)
    {

    }

    static function needsProposals()
    {
        return false;
    }
}