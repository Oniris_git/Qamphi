<?php

namespace App\BO\Questions;
use App\BO\Questions\Question;
use App\BO\Proposal;
use App\BO\Session;
use App\BO\Response;


use Exception;

class ChoiceQuestion extends Question
{


    protected $proposals = [];
    private $stats = [];
    const ICON = '<i class="fas fa-list-ol fa-3x"></i>';

    public function __construct($data, $proposals = [], $stats = [])
    {
        parent::__construct($data);
        $this->setProposals($proposals);
        $this->setStats($stats);
    }

    public function addProposal(Proposal $proposal)
    {
        if (!in_array($proposal, $this->proposals)) {
            array_push($this->proposals, $proposal);
        }
    }

    public function setProposals(array $proposals = [])
    {
        $this->proposals = [];
        $count = count($proposals);
        if ($count !== 5) {
            for ($i=0; $i < 5-$count ; $i++) {
                $emptyProposalArray = [
                    'question_id' => $this->getId(),
                    'p_order' => $count+$i+1,
                    'p_text' => '',
                    'correct' => 0
                ];
                $emptyProposal = new Proposal($emptyProposalArray);
                $proposals[] = $emptyProposal;
            }
        }
        foreach ($proposals as $proposal) {
            $this->addProposal($proposal);
        }
    }

    public function getProposals()
    {
        return $this->proposals;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        //$attributes = get_object_vars($this);
        $attributes = parent::getAttributes();
        //unset($attributes['proposals']);
        unset($attributes['stats']);
        return $attributes;
    }



    public function constructResponse($postdata) {
        $response = "-";
        $countResponse=0;
        for($i=1;$i<=5;$i++){
            if(isset($postdata['P'.$i])){
                $response .= $postdata['P'.$i]."-";
                if ($postdata['P'.$i] != 0) {
                    $countResponse++;
                }
            }else{
                $response .= "0-";
            }
        }
        if ($this->getType() == 2 && $countResponse > 1) {
            throw new Exception('tooMuch');
        }
        return $response;
    }



    public function addStat($stat,int $value)
    {
        if (in_array($stat, ['correct', 'incorrect', 'partial', 'non_response']))
        {
            $this->stats[$stat] = $value;
        }
    }

    public function setStats(array $stats)
    {
        foreach ($stats as $stat => $value)
        {
            $this->addStat($stat, $value);
        }
    }

    static function needsProposals()
    {
        return true;
    }


    /* DATA TREATMENT FUNCTIONS */


    public function statsFromData(Session $session, array $responses)
    {
        //variables to display stats
        $correct = 0;
        $incorrect = 0;
        $partial = 0;
        // $stringResponse = constructStringResponse($proposals);
        $stringResponse = "";
        $i=1;
        foreach ($this->proposals as $proposal){
            if ($proposal->getContent() !== '') {
                if($proposal->getCorrect() == 1){
                    $stringResponse.="-".$i;
                }else{
                    $stringResponse.="-0";
                }
                $i++;
            }
        }
        $stringResponse.="-";
        for ($j=$i; $j < 6; $j++) {
            $stringResponse .= '0-';
        }
        //if response exist
        if($stringResponse != '-0-0-0-0-0-'){
            // $listResponse = $responseDAO->selectResponseByQuestion($question->getId());
            //foreach response we compare strings and we increment correct or incorrect
            foreach ($responses as $response){
                if ($response->getResponse() != '-0-0-0-0-0-'  ){
                    $trim = traitementPartial($response->getResponse(), $stringResponse);
                    $check = strcmp (  $response->getResponse(), $stringResponse );
                    if($check == 0) {
                        $correct = $correct + 1;
                        //foreach correct we increment user score
                        // for ($f = 0; $f < $length; $f++) {
                        //     if ($response->getUsername() == $listUsers[$f]['username']) {
                        //         $listUsers[$f]['score'] = $listUsers[$f]['score'] + 1;
                        //     }
                        // }
                    }else{
                        $incorrect = $incorrect + 1;
                    }
                    if($trim == true && $check != 0){
                        $partial = $partial + 1;
                    }
                }
            }
        }

        $this->setStats([
            'correct' => $correct,
            'incorrect' => $incorrect - $partial,
            'partial' => $partial,
            'non_response' => $session->getParticipants() - ($partial + $correct +($incorrect - $partial))
        ]);

    }

    /**
     *
     */
    public function setProposalsFromData(array $listResponse,array $listProposition){
        $totalCount = 0;
        if($listResponse != "" || $listResponse != null){
            //$tabCount = responseTreatment($listResponse);
            $tabCount = self::sortResponses($listResponse);
            for ($i=0;$i<5;$i++){
                $totalCount = $totalCount + $tabCount[$i];
            }
        }else{
            $tabCount = [0,0,0,0,0];
        }
        foreach ($tabCount as $index => $corrects) {
            if (isset($listProposition[$index])) {
                $listProposition[$index]->setAnswers($corrects);
            }
        }
        $this->setProposals($listProposition);
    }

    /**
     * Sort responses
     * @return array : choice => nmber of users who made that choice
     */
    protected static function sortResponses(array $responses){
        //to stock response numbers by proposal
        $tabCount = [0,0,0,0,0];
        foreach ($responses as $responseObject){
            $response = $responseObject->getResponse();
            //we explode responses in table
            $tabResponse = explode("-", $response);
            $i = 0;
            foreach ($tabResponse as $row){
                if ($row != ""){
                    $tableau[$i] = [
                        'response' => $row,
                        'correct' => ""
                    ];
                    $i++;
                }
            }
            $tabLenght = count($tableau);
            $tabLenght = intval($tabLenght);
            //we increment table for each response valid
            for ($f=0;$f<$tabLenght;$f++){
                if($tableau[$f]['response'] != 0){
                    $tabCount[$f] = $tabCount[$f] +1;
                }
            }
        }
        return $tabCount;
    }



    /* DISPLAY FUNCTIONS */



    /* Teacher display functions */

    protected function formContent() {
        $checked = $this->getType() == 1 ? ' checked ' : '';
        echo '<div class="row justify-content-center">
                <div class="form-check">
            <input onclick="changetype();" type="checkbox" id="allow_multiple_responses'.$this->getOrder().'"'.$checked.'>
            <label for="allow_multiple_responses'.$this->getOrder().'">'.$this->lang['allow_multiple_responses'].'</label>
            </div>
        </div>';
        foreach ($this->proposals as $proposal) {
            $proposal->edit($this->getOrder());
        }
    }

    public function display(){
        parent::display();

        foreach ($this->proposals as $proposal) {
            if ($proposal->getContent() !== '') {
                $proposal->display();
            }
        }


        $previousQuestion = $this->getOrder() - 1;

        $retour = '<div class="row justify-content-md-center">';

        if ($previousQuestion != 0) {
            $retour .= '	<div class="col-4">
                        <form method="post" action="index.php?page=launch_session">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $previousQuestion . '">
                            <button type="submit" class="btn btn-outline-secondary width100 margin_bottom_btn_submit">'.$this->lang['previous'] .'</button>
                        </form>
					</div>';
        }

        $retour .= '    <div class="col-4">
                        <form action="index.php?page=launch_session" method="post">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $this->getOrder() . '">
                            <button type="submit" class="btn btn-outline-success width100 margin_bottom_btn_submit" name="request" value="response">'.$this->lang['responses'] .'</button>
                        </form>
                    </div>
                </div>';


    $retour .= '<br>';

    echo $retour;
    }

    public function displayStats(array $responses){
        $total = count($responses);
        parent::displayStats([]);


                if($total != 0){
                    $perCentCorrect = ($this->stats['correct'] * 60) / $total;
                    $perCentIncorrect = ($this->stats['incorrect'] * 60) / $total;
                    $perCentPartial = ($this->stats['partial'] * 60) / $total;
                    $perCentNonResponse = 60 - ($perCentCorrect + $perCentPartial + $perCentIncorrect);
                    $totalNonResponse = $total - ($this->stats['correct'] + $this->stats['incorrect'] + $this->stats['partial']);
                }else{
                    $perCentCorrect = 0;
                    $perCentIncorrect = 0;
                    $perCentPartial = 0;
                    $perCentNonResponse = 0;
                    $totalNonResponse = 0;
                }

                $result = '<div class="row" style="text-align:center;margin:1rem">';
                                if($perCentCorrect != 0){
                $result .=      '<div style="background-color: #51e295;width:'.$perCentCorrect.'%;color:white;" class="resultStatBar">'.$this->stats['correct'].'
                                </div>';}
                                if($perCentPartial != 0){
                $result .=      '<div style="background-color: #fad046;width:'.$perCentPartial.'%;color:white" class="resultStatBar" cellspacing=0>'.$this->stats['partial'].'
                                </div>';}
                                if($perCentIncorrect != 0){
                $result .=      '<div style="background-color: #fa4557;width:'.$perCentIncorrect.'%;color:white" class="resultStatBar" cellspacing:0>'.$this->stats['incorrect'].'
                                </div>';}
                                if($totalNonResponse != 0){
                $result .=          '<div style="background-color: #f0f0f0;width:'.$perCentNonResponse.'%;" class="resultStatBar" cellspacing:0>'.$totalNonResponse.'
                                </div>';}
                $result .= '</div>';


        echo $result;
    }

    public function displayDetailedStats($total, array $responses = []){
        $this->displayTitle($total);
        foreach ($this->proposals as $proposal) {
            $proposal->displayStats($this->participants);
        }

        echo '</table>';
    }


    /* Public display functions */

    public function displayForm($username)
    {
        $nextQuestion = $this->getOrder() + 1;
        $previousQuestion = $this->getOrder() - 1;
        $i=0;
        foreach ($this->proposals as $proposal){
            if ($proposal->getContent() === '') {
                unset($this->proposals[$i]);
            }
            $i++;
        }
        $countProposal = count($this->proposals);

        $remote_display = "<div class='row shoot_margin_lateral' id='propoStyle'>";
        for($i=0;$i<$countProposal;$i++){
            $remote_display .= "<div class='col-6'>
                                    <input id='P".$this->proposals[$i]->getOrder()."' type='hidden' name='P".$this->proposals[$i]->getOrder()."' value='0'>
                                    <button class='propoStyle' id='P".$this->proposals[$i]->getOrder()."Btn' type='button' onClick='validResponse(".$this->proposals[$i]->getOrder().",".$this->getType().")'>".$this->proposals[$i]->getOrder()."</button>
                                </div>";
        }
        while($i < 5){
            $remote_display .= "<div class='col-6'>
                                    <button class='propoStyle' style='cursor:no-drop;height:100%;'></button>
                                </div>";
                                $i++;
        }
        $remote_display .= "<div class='col-6'>
                                    <input type='hidden' value='".$this->getSessionId()."' name='idSession'>
                                     <input type='hidden' value='".$this->getId()."' name='idQuestion'>
                                     <input type='hidden' value='".$nextQuestion."' name='nextQuestion'>
                                     <input type='hidden' value='".$username."' name='username'>
                                     <button class='propoStyle' type='submit' name='valid' id='bV'>Ok</button>
                                </div> 
                            </div></table>                            
                         </form>";
        echo $remote_display;
    }

    public function displayResponse(Response $response)
    {
        $responseArray = explode("-", $response->getResponse());
        $remote_display = "<div class='row shoot_margin_lateral' id='propoStyle'>";

        for($i=1;$i<6;$i++){
            if($responseArray[$i] != 0){
                $style = "style='color: white;background-color: #444'";
            }else{
                $style = "";
            }
            $remote_display .= "<div class='col-6'>
                                    <button class='propoStyle' ".$style." type='button' disabled>".$i."</button>
                                </div>";
        }
        $remote_display .= "<div class='col-6'>
                                <button class='propoStyle' disabled>X</button>
                            </div>
                        </div>";

        echo $remote_display;
    }

    public function displaySummary(Response $response)
    {
        $retour = "<div id='questionTitle'>".$this->lang['question_number']."&nbsp;".$this->q_order."</div>
        ";
        if ($response->getResponse() === "-0-0-0-0-0-") {
            $retour .="<div class='titleFont responseSummary'>".$this->lang['no_responses']."</div>";
        } else {
            $responseList = explode("-", $response->getResponse());
            $retour .= "<div class='titleFont responseSummary'>".$this->lang['responses_summary'];
            $retour .="<ul>";
            foreach ($responseList as $responseString) {
                if ($responseString !=="0" && $responseString !=="") {
                    $retour .= "<li>$responseString</li>";
                }
            }
            $retour .="</ul></div>";
        }
        echo $retour;
    }

}