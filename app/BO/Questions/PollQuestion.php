<?php

namespace App\BO\Questions;


use App\BO\Session;


class PollQuestion extends ChoiceQuestion
{
    const ICON = '<i class="fas fa-chart-pie fa-3x"></i>';

    public function statsFromData(Session $session, array $responses){}

    public function setProposalsFromData(array $listResponse, array $listProposition)
    {
        $this->setProposals($listProposition);
    }

    protected function formContent() {
        foreach ($this->proposals as $proposal) {
            $proposal->edit($this->getOrder(), false);
        }
    }

    public function displayDetailedStats($total, array $responses = [])
    {
        $this->displayTitle($total);
        $tab = self::sortResponses($responses);
        $graph = [];
        foreach ($this->proposals as $index => $proposal) {
            if ($proposal->getContent() !== '') {
                $graph[$proposal->getContent()] = $tab[$index];
            }
        }
        echo '<div><canvas id="question'.$this->getId().'-chart" height="500px"></canvas></div>';
        echo '<div id="question'.$this->getId().'-data" style="visibility: hidden">';
        echo json_encode($graph);
        echo '</div>';
    }

    public function displayStats(array $responses)
    {
        parent::displayStats([]);
         $tab = self::sortResponses($responses);
         $graph = [];
         foreach ($this->proposals as $index => $proposal) {
             if ($proposal->getContent() !== '') {
                 $graph[$proposal->getContent()] = $tab[$index];
             }
         }
         arsort($graph);
         foreach ($graph as $prop => $nber) {
             $percent = ($nber/count($responses))*100;
             $percent = is_nan($percent) ? 0 : $percent;
             $graph[$prop] = round($percent, 1);
         }
         echo '<table class="table table-hover">';
         foreach ($graph as $key => $value) {
             echo '<tr>
                     <td class="sessionFont">'.$key.'</td>
                     <td class="sessionFont">'.$value.'%</td>
                 </tr>';
         }
         echo '</table>';
    }
}