<?php

namespace App\BO\Questions;
use App\BO\Questions\Question;
use App\BO\Proposal;
use App\BO\Session;
use App\BO\Response;
use Exception;

class ClickMapQuestion extends Question
{
    const ICON = '<i class="far fa-image fa-3x"></i>';

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function formContent() {
        $required = 'required';
        if (is_file(__DIR__.'/../../../public/tmp/question'.$this->getId().'.png')) {
            $required = '';
            echo '<div class="row justify-content-center propo">
                    <img class="img-size" src="../public/tmp/question'.$this->getId().'.png">
                    </div>';
        }
        echo '<div class="row justify-content-center propo">
                <input type="file" name="upload_'.$this->getOrder().'" '.$required.'>
                </div>';
    }

    public function display()
    {
        parent::display();
        echo '<div class="row justify-content-center propo">
        <img class="responsive-img" src="../public/tmp/question'.$this->getId().'.png">
        </div>';


        $previousQuestion = $this->getOrder() - 1;

        $retour = '<div class="row justify-content-md-center">';

        if ($previousQuestion != 0) {
            $retour .= '	<div class="col-4">
                        <form method="post" action="index.php?page=launch_session">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $previousQuestion . '">
                            <button type="submit" class="btn btn-outline-secondary width100 margin_bottom_btn_submit">'.$this->lang['previous'] .'</button>
                        </form>
					</div>';
        }

        $retour .= '    <div class="col-4">
                        <form action="index.php?page=launch_session" method="post">
                            <input type="hidden" name="idSession" value="' . $this->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $this->getOrder() . '">
                            <button type="submit" class="btn btn-outline-success width100 margin_bottom_btn_submit" name="request" value="response">'.$this->lang['responses'] .'</button>
                        </form>
                    </div>
                </div>';


        $retour .= '<br>';

        echo $retour;
    }

    public function constructResponse($postdata) {
        if ($postdata['left'] === false ) {
            throw new Exception('noResponse');
        }
        return json_encode([
            'left' => $postdata['left'],
            'top' => $postdata['top']
        ]);
    }

    public function displayStats(array $responses)
    {
        parent::displayStats([]);
        echo '<div class="row propo  justify-content-center img-container relative">
                <img class="responsive-img" src="../public/tmp/question'.$this->getId().'.png">';
        foreach ($responses as $response) {
            $position = json_decode($response->getResponse(), true);

            echo '<div style="width: 1px; height: 1px; position: absolute; left: '.$position['left'].'%; top: '.$position['top'].'%;">
                <i class="fas fa-2x fa-map-marker-alt icon-pointer"></i>
            </div>';
        }
        echo '</div>';
    }

    public function displayDetailedStats($total, array $responses = [])
    {
        $this->displayTitle($total);
        echo '<div class="row propo img-container justify-content-center relative">
                    <img class="responsive-img" src="../public/tmp/question'.$this->getId().'.png">';
        foreach ($responses as $response) {
            $position = json_decode($response->getResponse(), true);

            echo '<div style="width: 1px; height: 1px; position: absolute; left: '.$position['left'].'%; top: '.$position['top'].'%;">
                <i class="fas fa-2x fa-map-marker-alt icon-pointer"></i>
            </div>';
        }
        echo '</div>';
    }


    public function displayResponse(Response $response)
    {

    }


    public function displayForm($username)
    {
        $nextQuestion = $this->getOrder() + 1;
        $previousQuestion = $this->getOrder() - 1;

        $remote_display = "<div class='row shoot_margin_lateral' id='propoStyle '>";

        $remote_display .= '<div>
                        <img class="responsive-img img-size width100 zindex5" id="img_question" onclick="get_click_position(event);" src="../public/tmp/question'.$this->getId().'.png" style="position: relative">            
                        </div>

                            <input type="hidden" name="left" id="pos_x">
                            <input type="hidden" name="top" id="pos_y">';

        $remote_display .= "<input type='hidden' value='".$this->getSessionId()."' name='idSession'>
                                     <input type='hidden' value='".$this->getId()."' name='idQuestion'>
                                     <input type='hidden' value='".$nextQuestion."' name='nextQuestion'>
                                     <input type='hidden' value='".$username."' name='username'>
                                     <button class='propoStyle btn-confirm' type='submit' name='valid' id='bV'>Ok</button>
                            </div>
                            </table>                           
                         </form>";
        echo $remote_display;
    }

    public function displaySummary(Response $response)
    {
        $retour = "<div id='questionTitle'>".$this->lang['question_number']."&nbsp;".$this->q_order."</div>
                        <div class='titleFont responseSummary'>                    
                        ".$this->lang['response_received']."
                        </div>
                    ";
        echo $retour;
    }

    public function statsFromData(Session $session, array $responses)
    {

    }

    static function needsFile()
    {
        return true;
    }

    static function needsProposals() {
        return false;
    }

}