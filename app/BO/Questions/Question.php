<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:53
 */

namespace App\BO\Questions;
use App\BO\Proposal;
use App\BO\Session;
use App\BO\Response;
//use ChoiceQuestion;

abstract class Question
{

    protected $id;
    protected $session_id;
    protected $q_order;
    protected $q_text;
    protected $type_id;
    protected $participants;
    protected $lang;

    const ICON = '';

    /**
     * Return question object depending of it's type
     * When creating a new question type you need to :
     *  - Create a new Class extending this one and implements methods
     *  - Add a new question type in database (question_types) and it's id in following switch. It will return new instance of your question.
     *  - In each lang file (folder config/lang) add a line like the following :
     *      "btn_[your_new_question_type_name_row_in_db] => "The translation you wants",
 *          "ttl_add_[your_new_question_type_name_row_in_db]_question" => "The translation you wants",
     */
    static function instanciate($data) {
        switch ($data['type_id']) {
            case 1 :
            case 2 : return new ChoiceQuestion($data);
                break;
            case 3 : return new WordCloudQuestion($data);
                break;
            case 4 : return new ClickMapQuestion($data);
                break;
            case 5 : return new PollQuestion($data);
                break;
            default : throw new QuestionTypeException('Question type unknown');
        }
    }

    public function __construct($data)
    {
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }
        if(isset($data['session_id'])) {
            $this->session_id = $data['session_id'];
        }
        if(isset($data['q_order'])) {
            $this->q_order = $data['q_order'];
        }
        if(isset($data['q_text'])) {
            $this->q_text = $data['q_text'];
        }
        if(isset($data['type_id'])) {
            $this->type_id = $data['type_id'];
        }

        global $LANG;
        $this->lang = $LANG;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->session_id = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->q_order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->q_order = $order;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->q_text;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->q_text = $content;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type_id;
    }

    /**
     * @param mixed
     */
    public function setType($type)
    {
        $this->type_id = $type;
    }

    public function setParticipants($participants)
    {
        $this->participants = $participants;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        $attributes = get_object_vars($this);
        unset($attributes['participants']);
        if ($this::needsProposals()) {
            unset($attributes['proposals']);
        }
        return $attributes;
    }

    /**
     * Construct response string for DB persistance from $_POST
     * Optionnaly throw exception (for example if empty response), corresponding to message displayed in confirm.php controller
     */
    abstract function constructResponse($postdata);



    // ====================
    // = DISPLAY FUNCTION =
    // ====================

    /**
     * @param $sessionId
     * @param $count
     *
     * DO NOT IMPLEMENT IN CHILDREN. Only implement formContent()
     */
    public function teacherForm($sessionId, $count) {
        $this->formTitle();
        $this->formContent();
        $this->navigation($sessionId, $count);
    }

    /**
     * @return mixed
     * HTML Form
     */
    protected abstract function formContent();

    public function displayHeader($total, $username)
    {
        $nextQuestion = $this->getOrder() + 1;
        $previousQuestion = $this->getOrder() - 1;
        $remote_display = "<form method='post' action='index.php?page=participate' onsubmit='return checkResponses()'>
                             <table class='remote width100' cellspacing='10'> 
                                <div class='row shoot_margin_lateral'>
                                <input type=hidden id=warning_no_response value='".$this->lang['warning_no_response']."'>";

        if($this->getOrder() != 1) {
            $remote_display .= "<div class='col-2 shoot_padding' id='questionTitle'>
                                    <a href='index.php?page=participate&idSession=" . $this->getSessionId() . "&qOrder=" . $previousQuestion . "&username=".$username."'><i class='fas fa-arrow-left'></i></a>
                                </div>";
        }else{
            $remote_display .="<div class='col-2' id='questionTitle'></div>";
        }
        $remote_display .="<div class='col-8 shoot_padding'>
                                <div id='questionTitle'>".$this->lang['question_number']."&nbsp;".$this->getOrder()."</div>
                            </div>";
        if($this->getOrder() != $total) {
            $remote_display .= "<div class='col-2 shoot_padding' id='questionTitle'>
                                <a href='index.php?page=participate&idSession=".$this->getSessionId()."&qOrder=".$nextQuestion. "&username=".$username."'><i class='fas fa-arrow-right'></i></a>
                             </div></div>";
        }else{
            $remote_display .="<div class='col-2' id='questionTitle'></div></div>";
        }
        echo $remote_display;
    }

    public function displayTitle($total) {
        $separator = $this->lang['question_number'] == '' ? '. ' : ' : ';
        echo '<div class="row">
						<div class="col-10 sessionFontTitle">
						    <h4 class="marginLeft text-center title_question">
                                '. $this->getContent() . '
                            </h4>
                        </div>
                        <div class="col-2 fontSizeLaunch marginLaunch2" >
							<i class="fas fa-users">&nbsp'.$this->participants.'&nbsp/&nbsp'.$total.'</i> 
						</div> 
                    </div>';
    }

    protected function formTitle(){
        $retour='<div class="question">
        <div class="row" id="questionBlock" name="'.$this->getOrder().'">
                <input type="hidden" name="Q'.$this->getOrder().'type" value="'.$this->getType().'">

                            ';

        $retour .='
                    <div class="question-icon">
                        '.$this::ICON.'
                    </div>
					<div class="col-2 col-xl-1 QStyle" id="shootMarges" style="padding-right:1rem;">
						Q '.$this->getOrder().'.
					</div>
                    <div class="col-8 col-xl-9" id="shootMarges">';
        $retour .= '<input size="128" maxlength="128" id="QID'.$this->getOrder().'" class="form-control inputQuestion" placeholder="'.$this->lang['new_question_title'].'" type="text" name="Q'.$this->getOrder().'" value="'.$this->getContent().'" required>';
        $retour.='<input type="hidden" name="QID'.$this->getOrder().'" value="'.$this->getId().'"  >
					    </div>
				    </div>
				    <div class="formNavBlock">
				        <div class="formContent">';
        echo $retour;
    }

    public function navigation($sessionId, $count) {
        $retour = '</div>
            <div class="navigation">
            <div class="col-12">';
        if($this->getOrder() == 1 || $this->getContent()==""){
            $retour .= '<a>
                                        <i class="fas fa-caret-up size order-arrow iconSize colorArrow" style="color: transparent"></i>
                                        </a>';
        }else if ($this->getContent() == ""){}else{
            $retour .= '<a onclick="requestUp('.$sessionId.','.$this->getOrder().', readData)">
                                    <i class="fas fa-caret-up size order-arrow iconSize colorArrow"></i>
                                </a>';
        }

        // DELETE
        $retour .= '</div>
                        <div class="col-12">
                                    <form method="post" action="index.php?page=edit_session&id='.$sessionId.'">
                                        <input type="hidden" name="idQuestion" value="'.$this->getId().'">
                                        <button type="submit" onclick="return confirm(\''.$this->lang['delete_question'].'\')" name="action" value="deleteQuestion" class="unstyleBtn trash delete-question">
                                            <i class="fas fa-trash-alt iconSize" ></i>
                                        </button>
                                    </form>
                                </div>';

        $retour .='<div class="col-12">';
        if($this->getOrder() == $count || $this->getContent()==""){
            $retour .= '<a>
                                            <i class="fas fa-caret-down size order-arrow iconSize colorArrow" style="color: transparent"></i>
                                            </a>';
        }else{
            $retour .='<a onclick="requestDown('.$sessionId.','.$this->getOrder().',  readData)">
                                    <i class="fas fa-caret-down size order-arrow iconSize colorArrow"></i>
                                </a>';
        }
        $retour .= '</div>
        </div>
        </div>
        </div>';

        echo $retour;
    }


    /* TEACHER DISPLAY FUNCTIONS */

    /**
     * Teacher displaying question to public
     * Display question : Order title (and proposals if ChoiceQuestion)
     * Code below display question title and participants count. Call parent::display() at the begining of children implementations
     */
    protected function display() {
        echo '	<div class="row marginTop marginLeft">
        <div class="col-10 sessionFontTitle text-center">
        '. $this->getContent() . '
            </div>
        <div class="col-2 fontSizeLaunch marginLaunch2" >
            <i class="fas fa-users">&nbsp<span id="nbr-responses"></span>&nbsp/&nbsp<span id="nbr-participants"></span></i> 
        </div>  
        </div>
        <input type="hidden" id="question_order" value="'.$this->getOrder().'">';
    }


    /**
     * Display generals stats for this question
     * Code below display question title and participants count. Call parent::display() at the begining of children implementations
     */
    protected function displayStats(array $responses) {
        echo '<div>
               <div class="row justify-content-center" id="titleSession" style="margin: 1.5rem 0 0 0;">
                    <div class="col-12">
                        <span class="text-center">' . $this->getContent() . '</span>
                   </div>
                </div>';
    }

    /**
     * Display detailled stats for this question
     */
    abstract function displayDetailedStats($total, array $responses = []);




    /* PUBLIC DISPLAY FUNCTIONS */

    /**
     * Display previous response of user for this question
     */
    abstract function displayResponse(Response $response);

    /**
     * Display public input
     */
    abstract function displayForm($username);

    /**
     * Display response summary on confirm screen
     */
    abstract function displaySummary(Response $response);

    abstract function statsFromData(Session $session, array $responses);

    /**
     * @return either true or false if the question type needs proposals from teacher
     * If true need to implement following methods :
     *      - addProposal(Proposal $proposal)
     *      - setProposals(array $proposals)
     *      - getProposals() return array Proposal
     *      - private static sortResponses(array $responses) return array : choice => nmber of users who made that choice
     *      - public function setProposalsFromData(array[Response] $listResponse,array[Proposal] $listProposition)
     */
    abstract static function needsProposals();

    /**
     * If question requires file upload, set this to true (cf ClickMapQuestion)
     */
    static function needsFile() {
        return false;
    }
}