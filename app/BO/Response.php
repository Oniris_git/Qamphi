<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:59
 */

namespace App\BO;

class Response
{

    protected $id;
    protected $question_id;
    protected $response;
    protected $username;

    public function __construct($data)
    {
        if(isset($data['id'])){
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }
        $this->question_id = $data['question_id'];
        $this->response = $data['response'];
        $this->username = $data['username'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * @param mixed $questionId
     */
    public function setQuestionId($questionId)
    {
        $this->question_id = $questionId;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->username;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->username = $userName;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        return get_object_vars($this);
    }

}