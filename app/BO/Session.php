<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 11/03/2019
 * Time: 10:47
 */

namespace App\BO;

class Session
{

    protected $id;
    protected $user_id;
    protected $title;
    protected $active;
    protected $participants;
    private $current;
    private $token;
    private $auth;
    private $access;

    public function __construct($data)
    {
        if(isset($data['id'])){
            $this->id = $data['id'];
        }else{
            $this->id = null;
        }
        if(isset($data['current'])){
            $this->current = $data['current'];
        }else{
            $this->current = 1;
        }
        if(isset($data['token'])){
            $this->token = $data['token'];
        }
        $this->user_id = $data['user_id'];
        $this->title = $data['title'];
        if(isset($data['active'])){
            $this->active = $data['active'];
        }else{
            $this->active = 0;
        }
        $this->participants = $data['participants'];
        if(isset($data['auth'])){
            $this->auth=$data['auth'];
        }
        if(isset($data['access'])) {
            $this->access=$data['access'];
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param int $counter
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    public function getParticipants() {
        return $this->participants;
    }

    public function setParticipants($participants) {
        $this->participants = $participants;
    }




    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param mixed $auth
     *
     * @return self
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param mixed $access
     * 
     * @return self
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * @return array attributes
     * Used for data dumping
     */
    public function getAttributes() {
        return get_object_vars($this);
    }
}
