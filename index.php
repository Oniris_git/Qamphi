<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 26/03/2019
 * Time: 16:39
 */


if (!file_exists(__DIR__.'/config/Config.php') || !file_exists(__DIR__.'/config/ConfigSMTP.php')) {
    header('Location: '.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'public/install/lang.php');
    die;
}

require 'vendor/autoload.php';
require "app/dependencies.php";

use App\DAL\ConfigDAO;

//DAL instantiation
$configDAO = new ConfigDAO($connector);

//background link
$domain = $configDAO->selectConfigByType('general', 'domain')['value'];

header("Location: ".$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."public/");
exit;
