# Qamphi

Qamphi is a web-application developed by the team of the national school of veterinary medicine, food sciences and engineering Oniris, Nantes.

Qamphi is an animation tool in amphi. Whether it's to punctuate a class or a conference, the only prerequisite is that the public has mobile phones or tablets, and access to the internet.

#### Features
* Session management
* User management (create, import from Moodle)
* Questions management
* Communication with Moodle

## Installation

### Prerequisites

This web-application requiress :
* a web server with writing permissions
* php ≥ 7.2
* php-mysql
* php-pdo
* [composer](https://getcomposer.org) (coupling management)

### Installation by CLI

Clone or decompress archive
`git clone https://gitlab.com/Oniris_git/Qamphi`  

Go into QAmphi directory
  
Execute `composer install && composer dump-autoload --optimize` to install depedencies

Go to yoursite.com/qamphi to launch install

### Manual installation

https://connect.oniris-nantes.fr/adds_oniris/Qamphi/qamphi_install.zip

## Versioning

Qamphi is using Semantic versioning (http://semver.org)

## Developpers
Félicien Arnault,
Rémi André,
Arthur Valvert,
Quentin Debray,
Frédéric Auffray

## License
This project is licensed under the terms of the GNU General Public License as published by the Free Software Foundation version 3


## Support
If you have any question, issue or bug to report please contact support.qamphi@oniris-nantes.fr
