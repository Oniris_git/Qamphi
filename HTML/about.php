<?php


namespace App\HTML;

class about extends html
{

	public function aboutTitle($from, $version){
        $result = '<div class="row">
                        <div class="col-2 col-sm-1 homeIcon">
                            <a href="'.$from.'">
                                <i class="fas fa-arrow-left leftArrow"  style="margin-top: 4vh"></i>
                            </a>
                        </div>
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-bookmark" style="color: #625a5a"></i> '.$this->lang['about'].' - Qamphi '.$version.'
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function contentAbout(){

    	$result = '<div class="row justify-content-center" id="aboutContent">
    					<div class="col-10">
                            <p>
                                '.$this->lang['description'].'<strong>'.$this->lang['UN'].'</strong>. '.$this->lang['usage'].'
                            </p>
                            <img src="../public/image/qamphi_process.JPG" style="width:100%;margin-bottom:1rem"/>
                            <p>'.$this->lang['prerequistes'].'</p>
                            <p>'.$this->lang['download'].' Git: <i><a target="_blank" href="https://gitlab.com/Oniris_git/Qamphi" t>'.$this->lang['gitlab'].'</a></i></p>
                            <p>'.$this->lang['download'].' zip: <i><a href="https://connect.oniris-nantes.fr/adds_oniris/Qamphi/qamphi_install.zip" t>'.$this->lang['download'].'</a></i></p>
                            <hr>
   					    </div>
    				</div>
                    
                    <div class="row justify-content-center" style="margin-top:2rem;margin-bottom:1rem">
                        <div class="col-10">
                            <h3><strong>'.$this->lang['team'].'</strong></h3>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <p>'.$this->lang['designer'].' :</p>
                            <ul>
                                <li>Frédéric Auffray, Marc Gogny</li>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <p>'.$this->lang['lead_dev'].' :</p>
                            <ul>
                                <li>Félicien Arnault, Rémi André</li>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <p>'.$this->lang['previous_devs'].' :</p>
                            <ul>
                                <li>Arthur Valvert</li>
                                <li>Quentin Debray</li>
                                <li>Frédéric Auffray</li>
                        </div>
                        <span class="env-info">'.$this->lang['support_contact'].'&nbsp;<strong>support.qamphi(at)oniris-nantes.fr</strong></span>
                    </div>
                    <div class="row justify-content-center">
                        
                        <hr>
                        </div>
                    </div>';

    	echo $result;

    }

    public function licenceAbout(){

        $result = '<div class="row justify-content-center">
                        <div class="col-10">
                            <h3><strong>'.$this->lang['licence'].'</strong></h3>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin-bottom: 3rem">
                        <div class="col-10">
                            <p>'.$this->lang['licence_description'].'</p>
                        </div>
                    </div>';
        echo $result;
    }

}
