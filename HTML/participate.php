<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 18/03/2019
 * Time: 15:52
 */

namespace App\HTML;


class participate extends html
{

    public function showProposal($question, $listProposal, $username){
        $nextQuestion = $question->getOrder() + 1;
        $previousQuestion = $question->getOrder() - 1;
        $countProposal = count($listProposal);

        $remote_display = "<div class='row' id='propoStyle' style='margin-right:0;margin-left:0'>";
        for($i=0;$i<$countProposal;$i++){
            $remote_display .= "<div class='col-6'>
                                    <input id='P".$listProposal[$i]->getOrder()."' type='hidden' name='P".$listProposal[$i]->getOrder()."' value='0'>
                                    <button class='propoStyle' id='P".$listProposal[$i]->getOrder()."Btn' type='button' onClick='validResponse(".$listProposal[$i]->getOrder().",".$question->getType().")'>".$listProposal[$i]->getOrder()."</button>
                                </div>";
        }
        while($i < 5){
            $remote_display .= "<div class='col-6'>
                                    <button class='propoStyle' style='cursor:no-drop;height:100%;min-height:30vh'></button>
                                </div>";
                                $i++;
        }
        $remote_display .= "<div class='col-6'>
                                    <input type='hidden' value='".$question->getSessionId()."' name='idSession'>
                                     <input type='hidden' value='".$question->getId()."' name='idQuestion'>
                                     <input type='hidden' value='".$nextQuestion."' name='nextQuestion'>
                                     <input type='hidden' value='".$username."' name='username'>
                                     <button class='propoStyle' type='submit' name='valid' id='bV'>V</button>
                                </div> 
                            </div>
                         </form>";
        echo $remote_display;
    }

    public function headerProposal($question, $countQuestion, $username){
        $nextQuestion = $question->getOrder() + 1;
        $previousQuestion = $question->getOrder() - 1;

        $remote_display = "<form method='post' action='index.php?page=participate' onsubmit='return checkResponses()'>
                             <table class='remote width100' cellspacing='10'> 
                                <div class='row' style='margin-right:0;margin-left:0'>
                                <input type=hidden id=warning_no_response value='".$this->lang['warning_no_response']."'>";

        if($question->getOrder() != 1) {
            $remote_display .= "<div class='col-2' id='questionTitle'>
                                    <a href='index.php?page=participate&idSession=" . $question->getSessionId() . "&qOrder=" . $previousQuestion . "&username=".$username."'><i class='fas fa-arrow-left'></i></a>
                                </div>";
        }else{
            $remote_display .="<div class='col-2' id='questionTitle'></div>";
        }
        $remote_display .="<div class='col-8' style='padding: 0;'>
                                <div id='questionTitle'>".$this->lang['question_number']."&nbsp;".$question->getOrder()."</div>
                            </div>";
        if($question->getOrder() != $countQuestion) {
            $remote_display .= "<div class='col-2' id='questionTitle'>
                                <a href='index.php?page=participate&idSession=".$question->getSessionId()."&qOrder=".$nextQuestion. "&username=".$username."'><i class='fas fa-arrow-right'></i></a>
                             </div></div>";
        }else{
            $remote_display .="<div class='col-2' id='questionTitle'></div></div>";
        }

        echo $remote_display;
    }

    public function showResponse($question, $responseArray){

        $remote_display = "<div class='row' id='propoStyle'>";

        for($i=1;$i<6;$i++){
            if($responseArray[$i] != 0){
                $style = "style='color: white;background-color: #444'";
            }else{
                $style = "";
            }
            $remote_display .= "<div class='col-6'>
                                    <button class='propoStyle' ".$style." type='button' disabled>".$i."</button>
                                </div>";
        }
        $remote_display .= "<div class='col-6'>
                                <button class='propoStyle' disabled>X</button>
                            </div>
                        </div>";

        echo $remote_display;
    }

    public function panelBetween(){

        echo '<div class="container width100 height100">
                    <div class="row justify-content-center">
                        <div class="col-6 col-sm-12 col-md-11" id="menuContent">';
                    echo '  <div class="row justify-content-md-center" style="padding: 1rem 1rem 2rem 1rem">
                                <div class="col-12 col-sm-6 col-md-6 center">
                                    <a href="index.php?page=list_session" class="iconMenu">
                                        <i class="fas fa-edit btnHover"></i>
                                    </a>
                                    <div class="col-12 center">
                                        '.$this->lang['previous'].'
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 center">
                                    <a href="index.php?page=play" class="iconMenu">
                                        <i class="fas fa-play btnHover"></i>
                                    </a>
                                    <div class="col-12 center">
                                        '.$this->lang['next'].'
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>';
    }

    public function middleScreen() {
        $screen =''
            .'OK'
            .'  <button type="submit" name="next">Next</button>';
        echo $screen;
    }


}
