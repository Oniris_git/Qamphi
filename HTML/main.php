<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:45
 */

namespace App\HTML;

class main extends html
{
    //display main menu
    public function menu($backLink)
    {
        echo '<body style="background-image:url('.$backLink.')">
                <div class="container-fluid d-flex h-100 moz-margin">
                <style type="text/css" rel="stylesheet">
                @-moz-document url-prefix() {
                    .moz-margin {
                        margin-top: 15%                    
                    }
                }                
                </style>
                    <div class="row align-self-center w-100" style="padding:0;margin:0">
                        <div class="col-8 col-sm-10 col-md-8 col-lg-6 mx-auto d-flex justify-content-around" id="menuContent" style="padding:2rem">
                            <div class="row justify-content-center w-100">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="text-align:center">
                                    <a href="index.php?page=login" class="iconMenu">
                                        <i class="fas fa-chalkboard-teacher btnHover"></i>
                                    </a>
                                    <div class="col-12 center">
                                        '.$this->lang['restrict_access'].'
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="text-align:center">
                                    <a href="index.php?page=sessions" class="iconMenu">
                                        <i class="fas fa-mobile btnHover"></i>
                                    </a>
                                    <div class="col-12 center">
                                        '.$this->lang['public_access'].'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    }

    public function returnBtn($from, $idSession = null, $order = null)
    {
        if(substr($from, -14) == 'launch_session'){
            echo '<div class="row">
                    <div class="col-2 col-sm-1 homeIcon">
                        <form actio="launch_session" method="post">
                            <button href="'.$from.'" class="unstyle">
                                <input type="hidden" name="idSession" value="'.$idSession.'">
                                <input type="hidden" name="order" value="'.$order.'">
                                <i class="fas fa-arrow-left leftArrow" style="margin-top: 4vh"></i>
                            </button>
                        </form>
                    </div>
                </div>';
        }else{
            echo       '<div class="row">
                        <div class="col-12 homeIcon">
                            <a href="'.$from.'">
                                <i class="fas fa-arrow-left leftArrow" style="margin-top: 4vh"></i>
                            </a>
                        </div>
                    </div>';
        }

    }

}
