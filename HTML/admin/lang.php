<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 10/05/2019
 * Time: 16:38
 */

namespace App\HTML;


class lang extends html
{

    public function langTitle(){
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="index.php?page=admin">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                        <div class="col-10 col-sm-10 adminTitle">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-flag" style="color: #625a5a"></i> '.$this->lang['lang_management'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>
                    <form method="post" action="?page=lang">';
        echo $result;
    }

    public function newLangForm() {

        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="index.php?page=lang">
                                <i class="fas fa-arrow-left"></i>
                            </a>
                        </div>
                    <div class="col-10 col-sm-10 adminTitle">
                        <h1 style="margin-top: 3vh">
                            '.$this->lang['new_lang'].'
                        </h1>
                    </div>
                </div> 
                <hr>
                <form method="post" action="?page=add_lang" enctype="multipart/form-data">
                <div class="installContainer">
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                '.$this->lang['code'].' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="" name="langName" class="form-control"  required maxlength=5>
                                <small class ="form-text text-muted">'.$this->lang['lang_name'].'</small>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                '.$this->lang['entitled'].' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="" name="langContent" class="form-control"  required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                '.$this->lang['flag'].' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="flagFile" required>
                            <label class="custom-file-label" for="inputGroupFile01">'.$this->lang['file_choice'].'</label>
                            <small class ="form-text text-muted">'.$this->lang['file_name'].'</small>
                            </div>
                        </div>
                </div>
                
                ';
        echo $result;
    }

    public function langLine($value, $attribute, $POST){
        $prefill = $POST[$value] === null ? $this->lang[$value] : $POST[$value];
        $result = ' <div class="col-12 col-sm-6">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">'.$value.'</span>
                            </div>
                            <input type="text" class="form-control" name="'.$value.'" aria-describedby="inputGroup-sizing-default" '.$attribute.'="'.$prefill.'" required>
                        </div>
                    </div>';
        return $result;
    }

    public function mainTitles($tab, $attribute, $POST = null){
        $result = '<div class="row" >
                        <div class="col-12 col-sm-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["main_titles"].' 
                            </h3>
                            <i class="fas fa-caret-down" id="arrowTitles" data-toggle="collapse" href="#collapseTitles" role="button" aria-expanded="false" aria-controls="collapseTitles" onclick="hideArrow(\'Titles\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseTitles" class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang" >';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>
                    <hr>';
        echo $result;
    }

    public function buttons($tab, $attribute, $POST = null){
        $result = '<div class="row">
                        <div class="col-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["buttons"].' 
                            </h3>
                            <i class="fas fa-caret-down" id="arrowButtons" data-toggle="collapse" href="#collapseButtons" role="button" aria-expanded="false"  aria-controls="collapseButtons" onclick="hideArrow(\'Buttons\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseButtons"  class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang">';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>               
                    <hr>';
        echo $result;
    }


    public function general($tab, $attribute, $POST = null){
        $result = '<div class="row">
                        <div class="col-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["general"].' 
                            </h3>
                            <i class="fas fa-caret-down" id="arrowButtons" data-toggle="collapse" href="#collapseGeneral" role="button" aria-expanded="false"  aria-controls="collapseButtons" onclick="hideArrow(\'Buttons\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseGeneral"  class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang">';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>               
                    <hr>';
        echo $result;
    }

    public function tab($tab, $attribute, $POST = null){
        $result = '<div class="row">
                        <div class="col-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["tables"].' 
                            </h3>
                            <i class="fas fa-caret-down" id="arrowTab" data-toggle="collapse" href="#collapseTab" role="button" aria-expanded="false" aria-controls="collapseTab" onclick="hideArrow(\'Tab\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseTab" class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang">';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>                  
                    <hr>';
        echo $result;
    }

    public function about($tab, $attribute, $POST = null){
        $result = '<div class="row">
                        <div class="col-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["about"].' 
                            </h3>
                            <i class="fas fa-caret-down" id="arrowAbout" data-toggle="collapse" href="#collapseAbout" role="button" aria-expanded="false" aria-controls="collapseAbout" onclick="hideArrow(\'About\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseAbout" class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang">';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>                  
                    <hr>';
        echo $result;
    }

    public function errors($tab, $attribute, $POST = null, $disabled){

        $result = '<div class="row">
                        <div class="col-12 col-sm-8" style="margin: 2rem 0">
                            <h3 style="display: inline">
                                 '.$this->lang["error_messages"].'
                            </h3>
                            <i class="fas fa-caret-down" id="arrowError" data-toggle="collapse" href="#collapseError" role="button" aria-expanded="false" aria-controls="collapseError" onclick="hideArrow(\'Error\')" style="cursor:pointer"></i>
                        </div>
                    </div> 
                    <div id="collapseError" class="collapse">
                        <div class="card card-body">
                            <div class="row inputLang" >';
                                foreach ($tab as $value) {
                                    $result .= $this->langLine($value, $attribute, $POST);
                                }
        $result .=           '</div>
                        </div>
                    </div>
                    <hr>
                    <div class="row inputLang justify-content-center">
                        <div class="col-12 col-sm-3">
                            <button class="btn btn-outline-success width100" '.$disabled.'>'.$this->lang['record'].'</button>
                        </div>
                    </div>        
                </form>';

        echo $result;
    }


    public function langChoice($currentLang, $langs) {
        $retour = ' <div class="centerLang">
        <form id="formLang" method="POST" action="?page=lang">
        <h2>
        <strong>'.$this->lang['lang_choice'].'</strong>
        </h2>
        <div style="display: block ruby;">
        <select name="language" id="language" class="form-control col-2" onchange="changeLang()">';
        foreach ($langs as $name => $content) {
            if ($name === $currentLang) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $retour .= '<option value="'.$name.'"'.$selected.'>'.$content.'</option>';
        }
        $retour .= '</select>
            <a href="?page=add_lang">
            <i class="fas fa-plus-circle hvr-pulse fa-lg" onclick="displayTypeButtons()" style="margin: 0 0 0.5rem 2rem" title="'.$this->lang['new_lang'].'"></i>
            </a>
        </div>
        </form>
        </div>';

        echo $retour;
    }
}
