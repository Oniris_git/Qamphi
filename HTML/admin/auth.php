<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 14:19
 */

namespace App\HTML;


class auth extends html
{

    //display page title with return button
    public function authTitle()
    {
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="?page=admin">
                                <i class="fas fa-arrow-left leftArrow"></i>
                            </a>
                        </div>
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-users" style="color: #625a5a"></i> '.$this->lang['account_management'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    //display admin acounts with admins list in parameters
    public function adminAccounts($listAdminUsers, $countSessionsAdmin, $questionsByAdmin, $moodle)
    {
        $result = $this->modalManual('Admin');
        $result .= $this->modalMoodle($data = [],'Admin');

        $result .= '<div class="container-fluid" style="margin: 4rem 0 1rem 0">
                        <h2 style="display: inline">'.$this->lang['admin_account'].'</h2>
                        <i class="fas fa-plus-circle hvr-pulse" onclick="displayButtons(\'Admin\')" style="margin: 0 0 0.5rem 2rem"></i>
                        <button onclick="addAutofocus()" type="button" style="visibility: hidden;margin: 0 0.5rem 0.5rem 1rem" class="btn btn-outline-success accountTypeAdmin" data-toggle="modal" data-target="#modalManualAdmin">'.$this->lang['manual'].'</button>';
                        if($moodle == "on"){
        $result .= '                <button onclick="addAutofocus()" style="visibility: hidden;margin: 0 0.5rem 0.5rem 0.5rem" class="btn btn-outline-success accountTypeAdmin" data-toggle="modal" data-target="#modalMoodleAdmin">'.$this->lang['moodle'].'</button></div>';
      }else{
        $result .= '<div class="accountTypeAdmin"></div>';
      }
        $result .= '<div id="targetListadmin"></div>
                        <table class="table table-hover" id="tableListadmin">
                              <thead>
                                    <tr>
                                          <th scope="col" style="min-width: 30%">'.$this->lang['username'].'</th>
                                          <th scope="col" style="text-align: center">'.$this->lang['account'].'</th>
                                          <th scope="col" style="text-align: center">'.$this->lang['user_stats'].'</th>
                                          <th scope="col" style="text-align: center">'.$this->lang['delete'].'</th>
                                    </tr>
                              </thead>
                              <tbody>';
                                    foreach ($listAdminUsers as $key => $user) {
                                        $result .= '<tr>
                                                        <td>' . $user->getUsername() . '</td>
                                                        <td style="text-align: center">';
                                                       if($user->getAuth() == 'manual'){
                                                           $result .= '<i class="fas fa-rainbow"></i></td>';
                                                       }elseif ($user->getAuth() == 'lock'){
                                                           $result .= '<i class="fas fa-lock"></i></td>';
                                                       }else{
                                                           $result .= '<i class="fas fa-graduation-cap" ></i></td>';
                                                       }

                                                       $result .= '<td style="text-align: center">'.$countSessionsAdmin[$user->getId()].' ('.$questionsByAdmin[$user->getId()].')</td>';

                                        $result .= '   <td style="text-align: center"> 
                                                            <form action="?page=auth" method="post" onsubmit="return confirmDelete()">
                                                            <input type=hidden id=confirm_delete_user value="'.$this->lang['delete_user_confirm'].'">
                                                                <button type="submit" name="deleteUser" value="'.$user->getId().'" class="unstyleBtn trash">
                                                                     <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </form>   
                                                        </td>
                                                     </tr>';
                                    }
        $result .= '</tbody>
                         </table>
                     ';
        echo $result;
    }

    public function teacherAccounts($listTeacherUsers, $countSessions, $questionsByUser, $moodle)
    {
        $result = $this->modalManual('Teacher');
        $result .= $this->modalMoodle($data = [],'Teacher');

        $result .= '<div class="container-fluid" style="margin: 4rem 0 1rem 0">
                        <h2 style="display: inline"> '.$this->lang['teacher_account'].'</h2>
                        <i class="fas fa-plus-circle hvr-pulse" onclick="displayButtons(\'Teacher\')" style="margin: 0 0 0.5rem 2rem"></i>
                        <button onclick="addAutofocus()" type="button" style="visibility: hidden;margin: 0 0.5rem 0.5rem 1rem" class="btn btn-outline-success accountTypeTeacher" data-toggle="modal" data-target="#modalManualTeacher">Manuel</button>';
                          if($moodle == "on"){
        $result .= '                <button onclick="addAutofocus()" style="visibility: hidden;margin: 0 0.5rem 0.5rem 0.5rem" class="btn btn-outline-success accountTypeTeacher" data-toggle="modal" data-target="#modalMoodleTeacher">Moodle</button></div>';
      }else{
        $result .= '<div class="accountTypeTeacher"></div>';
      }
        $result .= ' <div id="targetListteacher"></div>
                        <table class="table table-hover" id="tableListteacher" style="margin-bottom: 2rem">
                              <thead>
                                    <tr>
                                          <th scope="col" style="min-width: 30%">'.$this->lang['username'].'</th>
                                          <th scope="col" style="text-align: center">'.$this->lang['account'].'</th>
                                          <th scope="col" style="text-align: center">'.$this->lang['user_stats'].'</th>                                          
                                          <th scope="col" style="text-align: center">'.$this->lang['delete'].'</th>
                                    </tr>
                              </thead>
                              <tbody>';
        foreach ($listTeacherUsers as $key => $user) {
            $result .= '<tr>
                                                        <td>' . $user->getUsername() . '</td>
                                                        <td style="text-align: center">';
            if($user->getAuth() == 'manual'){
                $result .= '<i class="fas fa-rainbow"></i></td>';
            }elseif ($user->getAuth() == 'lock'){
                $result .= '<i class="fas fa-lock"></i>';
            }else{
                $result .= '<i class="fas fa-graduation-cap" ></i></td>';
            }


            $result .= '<td style="text-align: center">'.$countSessions[$user->getId()].' ('.$questionsByUser[$user->getId()].')</td>';



            $result .= '   <td style="text-align: center"> 
                                                            <form action="?page=auth" method="post" onsubmit="return confirmDelete()">
                                                            <input type=hidden id=confirm_delete_user value="'.$this->lang['delete_user_confirm'].'">
                                                                <button type="submit" name="deleteUser" value="'.$user->getId().'" class="unstyleBtn trash">
                                                                     <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </form>   
                                                        </td>
                                                     </tr>';
        }
        $result .= '</tbody>
                         </table>';
        echo $result;
    }

    //display modal for manual account add / account type in params
    public function modalManual($type)
    {
        $result = '<div class="modal fade" aria-labelledby="manualModalLabel" id="modalManual'.$type.'" style="margin-bottom: 1rem"  tabindex="-1" role="dialog" aria-hidden="true">
                        <div  class="modal-dialog modal-lg" role="document" id="formManual">
                            <div class="modal-content" style="margin-top: 33%">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="manualModalLabel"><i class="fas fa-user"></i>'." ".$this->lang['add_manual_account'].'</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="?page=auth" method="post">
                                        <div class="row">
                                            <div class="col-6">
                                                '.$this->lang['id'].'
                                            </div>
                                            <div class="col-6">
                                                <input type="text" class="form-control" name="username">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                Email
                                            </div>
                                            <div class="col-6">
                                                <input type="email" class="form-control" name="email" >
                                            </div>
                                        </div>
                                         <input type="hidden" class="form-control" name="password">
                                        <input type="hidden" class="form-control" name="confirmPassword">
                                        <div class="row justify-content-center">
                                            <div class="col-4">
                                                <button type="submit" class="btn btn-outline-success width100" name="typeAdd" value="'.$type.'">'.$this->lang['create'].'</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>';

        return $result;
    }

    public function modalMoodle($data = null, $type)
    {

        $result = '<div class="modal fade" id="modalMoodle'.$type.'" style="margin-bottom: 2rem" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl" id="formManual"  role="document">
                            <div class="modal-content" style="margin-top: 33%">
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fas fa-user"></i>'.$this->lang['moodle_search'].'</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="margin-top: 1rem">
                                    <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                          </div>
                                          <input type="text" class="form-control" oninput="userSearch(readUsers'.$type.', \''.$type.'\')" id="inputUsername'.$type.'">
                                     </div>';
        $result .= $this->tabMoodle($data, $type);
        $result .= '</div></div>';
        return $result;
    }

    public function tabAjax($data, $type, $countSessions, $questionsByUser)
    {
        $result = '<table class="table table-hover" id="tableList'.$type.'">
                      <thead>
                          <tr>
                              <th scope="col">'.$this->lang['username'].'</th>
                              <th scope="col" style="text-align:center">'.$this->lang['account'].'</th>';
        //if ($type != "Admin") {
            $result .='             <th scope="col" style="text-align:center">'.$this->lang['user_stats'].'</th>';
        //}
        $result .='             <th scope="col" style="text-align:center">'.$this->lang['delete'].'</th>
                          </tr>
                      </thead>
                      <tbody>';
                            foreach ($data as $key => $user) {
                                $result .= '<tr>
                                               <td>' . $user->getUsername() . '</td>
                                               <td style="text-align: center">';
                                if($user->getAuth() == 'manual'){
                                     $result .= '<i class="fas fa-rainbow"></i></td>';
                                }elseif ($user->getAuth() == 'lock'){
                                     $result .= '<i class="fas fa-lock"></i></td>';
                                }else{
                                     $result .= '<i class="fas fa-graduation-cap" ></i></td>';
                                }
        //if ($type != "Admin") {
            $result .= '<td style="text-align: center">'.$countSessions[$user->getId()].' ('.$questionsByUser[$user->getId()].')</td>';
        //}

        $result .= '                            <td style="text-align:center">    
                                                   <form action="?page=auth" method="post" onsubmit="return confirmDelete()">
                                                   <input type=hidden id=confirm_delete_user value="'.$this->lang['delete_user_confirm'].'">
                                                        <button type="submit" name="deleteUser" value="'.$user->getId().'" class="unstyleBtn trash">
                                                             <i class="fas fa-trash-alt"></i>
                                                        </button>
                                                    </form> 
                                                </td>
                                            </tr>';
                            }
        $result .= '</tbody>
                         </table>';
        echo $result;
    }

    public function tabMoodle($data, $type)
    {
        $result = '<div id="targetUsers'.$type.'"></div>';
        if ($data != null && !empty($data)) {
            $result .= '<table class="table table-hover" id="moodle'.$type.'Users">
                              <thead>
                                    <tr>
                                          <th scope="col">Username</th>
                                          <th scope="col"></th>
                                    </tr>
                              </thead>
                              <tbody>';
            if ($data != null) {
                foreach ($data as $key => $user) {
                    $result .= '<tr>
                                     <td>' . $user['username']. '</td>
                                     <td>
                                         <button type="submit" name="newAdd" onclick="newAdd(read'.$type.', \''.$user['username'].'\', \''.$type.'\')" class="unstyleBtn">
                                            <i class="fas fa-plus-circle hvr-pulse" ></i>
                                         </button>
                                     </td>
                                </tr>';
                }
            }

            $result .= '</tbody>
                          </table></div></div>';
        } else {
            $result .= '<div style="text-align: center" id="moodle'.$type.'Users"></div></div></div>';
        }
        return $result;
    }

    public function footerAuth()
    {
        echo ' <script src="../public/JS/auth.js"></script>';
    }

}
