<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:44
 */

namespace App\HTML\admin;

class update extends html
{
    public function updateTitle(){
        $result = '<div class="row">
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-cogs" style="color: #625a5a"></i> '.$this->lang['update'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>
                    <div class="text-center env-info">
                        '.$this->lang['update_needed_admin'].'
                    </div>
                    <br>';

        echo $result;
    }

    public function export($disabled) {
        $link = $disabled == 'disabled' ? 'title="'.$this->lang['permissions'].'"' : 'href="?page=update&dump"';
        $result = '<div class="installContainer">
                        <h2>
                        <strong>'.$this->lang['datas_management'].'</strong>
                        </h2>
                            <div class="row justify-content-end" style="margin: 2rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                <a class="btn btn-outline-success" '.$link.'>
                                    '.$this->lang['export_db'].'
                                </a>
                            </div>
                            <div class="col-12 col-md-12 col-lg-6 env-info">
                                '.$this->lang['export_infos'].'
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin: 2rem">
                            '.$this->lang['update_datas_infos'].'
                        </div>                                                                     
                        </div>';
        echo $result;
    }

    public function update($disabled) {
        $link = $disabled == 'disabled' ? 'title="'.$this->lang['permissions'].'"' : 'href="?page=update&update"';
        $result = '<div class="installContainer">
                        <h2>
                        <strong>'.$this->lang['update'].'</strong>
                        </h2>
                        <div class="row justify-content-center" style="margin: 2rem">
                            '.$this->lang['update_infos'].'
                        </div>  
                        <div class="justify-content-center text-center">
                            <a class="btn btn-outline-success" '.$link.'">
                                '.$this->lang['launch_update'].'
                            </a>
                        </div>                                    
                        </div>';
        echo $result;
    }
}
