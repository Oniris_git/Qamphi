<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 07/05/2019
 * Time: 16:53
 */

namespace App\HTML;


class theme extends html
{

    public function footerTheme(){
        echo ' <script src="https://cdn.jsdelivr.net/npm/@jaames/iro/dist/iro.min.js"></script>
               <script src="../public/JS/colorPicker.js"></script>';
    }

    public function colorPicker($color){
        $result = '   <div class="col-4 ">
                        <div class="row">
                            <div class="row" style="margin: 3rem 1rem 0 1rem">
                                <div class="col-12">
                                    <h3><i class="fas fa-pencil-alt"></i> Modifier la couleur principale</h3>
                                </div>
                            </div>
                            <div class="row colorContainer" style="background-color: '.$color.';margin-top: 3rem">
                                <div class="col-4 ">
                                    <form action="?page=theme" method="post">
                                        <button type="submit" class="btn btn-outline-secondary width100" style="margin-top: 19rem" name="defaultColor">Défault</button>
                                    </form>
                                </div>
                                <div class="col-8">
                                     <div id="color-picker-container"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>';
        echo $result;
    }

    public function themeTitle(){
        $result = '<div class="row">
                        <div class="col-3 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="index.php?page=admin">
                                <i class="fas fa-arrow-left leftArrow" ></i>
                            </a>
                        </div>
                        <div class="col-9 col-sm-10 adminTitle">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-palette" style="color: #625a5a"></i> '.$this->lang['theme'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>';
        echo $result;
    }

    public function backgroundForm($backLink, $disabled){

        $formats = ['jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF'];
        foreach ($formats as $format) {
            if (file_exists("../public/image/error.".$format)) {
                $fileFormat = $format;
            }
        }

        $result = '<div class="row">
                        <div class="col-12">
                            <div class="row" style="margin-top: 2.5rem">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                   <img src="'.$backLink.'" style="max-height: 20vh; padding-left: 15em;">
                                   <img src="../public/image/error.'.$fileFormat.'" style="max-height: 20vh; padding-left: 15em; margin-top: 5vh;">
                                </div>
                             
                                <div class="col-12 col-sm-12 col-lg-6">

                                
                                    <div class="row justify-content-center titleIcon">
                                        '.$this->lang['update_background'].'
                                    </div>
                                    <form action="?page=theme" method="post" enctype="multipart/form-data">
                                        <div class="input-group mb-3 width100">
                                            
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="backgroundSelected" required>
                                                <label class="custom-file-label" for="inputGroupFile01">'.$this->lang['file_choice'].'</label>
                                            </div>
                                        </div>
                                        <div class="col-12" style="text-align: center" >
                                            <button type="submit" class="btn btn-outline-success" style="margin: 1rem 0;" '.$disabled.'>
                                                  '.$this->lang['alter'].'
                                            </button>
                                        </div>
                                    </form>


                                    <div class="row justify-content-center titleIcon">
                                        '.$this->lang['update_error'].'
                                    </div>
                                    <form action="?page=theme" method="post" enctype="multipart/form-data">
                                        <div class="input-group mb-3 width100">
                                            
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="errorSelected" required>
                                                <label class="custom-file-label" for="inputGroupFile01">'.$this->lang['file_choice'].'</label>
                                            </div>
                                        </div>
                                        <div class="col-12" style="text-align: center" >
                                            <button type="submit" class="btn btn-outline-success" style="margin: 1rem 0;" '.$disabled.'>
                                                  '.$this->lang['alter'].'
                                            </button>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>';

        echo $result;

    }

}
