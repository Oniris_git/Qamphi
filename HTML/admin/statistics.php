<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 06/05/2019
 * Time: 15:51
 */

namespace App\HTML;


class statistics extends html
{
    public function statsTitle()
    {
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="?page=admin">
                                <i class="fas fa-arrow-left leftArrow"></i>
                            </a>
                        </div>
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-chart-bar" style="color: #625a5a"></i> '.$this->lang['statistics'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function month_summary($stats) {
        if ($stats !== false) {
            echo 'Sessions créées ce mois : '.$stats['session_create'];
            echo '<br>';
            echo 'Nombre de participations ce mois : '.$stats['session_join'];
        } else {
            echo 'Pas encore d\'activité ce mois';
        }
    }

    public function all_stats($actions) {
        foreach ($actions as $key => $value) {
            echo '<h5 class="text-center">'.$this->lang[$key].'</h5>
                     <div><canvas id="'.$key.'-chart" height="500px"></canvas></div>';
        }
    }


}