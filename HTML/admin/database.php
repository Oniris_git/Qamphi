<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 10/05/2019
 * Time: 16:38
 */

namespace App\HTML;


class database extends html
{

    public function dbTitle()
    {
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="index.php?page=admin">
                                <i class="fas fa-arrow-left leftArrow"></i>
                            </a>
                        </div>
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-cogs" style="color: #625a5a"></i> ' . $this->lang['configuration'] . '
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function moodleDB($active, $configFile, $install)
    {
        $disable = '';
        $result = '<form action="index.php?page=database" method="post">  
                    <div class="installContainer">
                        <div class="row switchAlign">
                            <div class="col-12 col-sm-8 col-md-7 col-lg-5 col-xl-3" style="display: contents">
                            <h2 style="display:inline"><strong>Moodle connection</strong></h2>
                            <a type="button" data-toggle="modal" data-target="#modalHelpMoodle">
                                <i class="far fa-question-circle" style="font-size:1.2rem;"></i>
                            </a>                         
                            <label class="switch">
                                <input id="radioMoodle" type="radio"';
        if ($active == "off") {
            $result .= ' value="off" checked ';
            $disable = ' disabled ';
        }
        $result .= 'name="radioMoodle" onclick="disableMoodle()">
                                <span class="slider round"></span>
                            </label>
                            </div>
                            
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                ' . $this->lang['server_name'] . ' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="' . $configFile['db_hostMoodle'] . '" name="serverNameMoodle" class="form-control disableMoodle" ' . $disable . ' required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                Login :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="' . $configFile['db_userMoodle'] . '" name="loginBDDMoodle" class="form-control disableMoodle" ' . $disable . '  required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                ' . $this->lang['password'] . ' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="password" value="' . $configFile['db_passwordMoodle'] . '" name="passwordBDDMoodle" class="form-control disableMoodle" ' . $disable . '>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                ' . $this->lang['database'] . ' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="' . $configFile['db_nameMoodle'] . '" name="bddNameMoodle" class="form-control disableMoodle" ' . $disable . '  required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-5">
                                Type :
                            </div>
                            <div class="col-6">
                                <select name="typeBDDMoodle" class="form-control">
                                <option value="mysql"' . self::isselected($configFile, 'mysql') . '>MySQL/MariaDB</option>
                                <option value="oci"' . self::isselected($configFile, 'oci') . '>Oracle</option>
                                <option value="pgsql"' . self::isselected($configFile, 'pgsql') . '>PostgreSQL</option>
                                </select>
                            </div>
                        </div>
                    </div>';
        $result .= $install->modalHelpMoodle();
        echo $result;
    }

    public function SMTPAccess($install, $configSMTP, $domain)
    {
        $checked = $configSMTP['certs'] == 'on' ? ' checked' : '';
        $result = '<div class="installContainer">
                        <div class="row">
                            <div class="col-12">
                                <h2 style="display:inline"><strong>' . $this->lang['smtp'] . '</strong></h2>
                                <a type="button" data-toggle="modal" data-target="#modalHelpSMTP">
                                    <i class="far fa-question-circle" style="font-size:1.2rem;"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                Host :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="' . $configSMTP['host'] . '" name="hostSMTP" class="form-control"  required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                Port :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="number" value="' . $configSMTP['port'] . '" name="portSMTP" class="form-control"  required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                Username :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="text" value="' . $configSMTP['username'] . '" name="usernameSMTP" class="form-control" required>
                            </div>
                        </div>
                        <div class="row justify-content-end" style="margin: 1rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                ' . $this->lang['password'] . ' :
                            </div>
                            <div class="col-12 col-md-12 col-lg-6">
                                <input type="password" value="' . $configSMTP['password'] . '" name="passwordSMTP" class="form-control"  required>
                            </div>
                        </div>

                        <div class="row justify-content-end" style="margin: 1rem">
                        <div class="col-12 col-md-12 col-lg-5">
                            ' . $this->lang['certs'] . ' :
                        </div>
                        <div class="col-12 col-md-12 col-lg-6">
                            <input type="checkbox" name="certs" class=""' . $checked . '>
                        </div>
                    </div>
                    </div>';
        $result .= $install->modalHelpSMTP();
        echo $result;
    }

    public function environmentChoice($environment)
    {
        $result = '<form action="index.php?page=database" method="post">
                    <div class="installContainer">
                        <h2>
                        <strong>' . $this->lang['environment'] . '</strong>
                        </h2>
                        <input type="radio" id="prod" name="environment" value="prod"';
        if ($environment == 'prod') {
            $result .= 'checked >
            ';
        } else {
            $result .= '>
            ';
        }
        $result .= '<label for="prod">' . $this->lang['prod'] . '<span class="env-info">&nbsp(' . $this->lang['prod_info'] . ')</span></label><br>
                        <input type="radio" id="dev" name="environment" value="dev" ';
        if ($environment == 'dev') {
            $result .= 'checked >
            ';
        } else {
            $result .= '>
            ';
        }
        $result .= '<label for="dev">' . $this->lang['dev'] . '<span class="env-info">&nbsp(' . $this->lang['dev_info'] . ')</span></label>
                    </div>';

        echo $result;

    }

    public function export($disabled)
    {
        $link = $disabled == 'disabled' ? 'title="' . $this->lang['permissions'] . '"' : 'href="?page=database&dump"';
        $result = '<div class="installContainer">
                        <h2>
                        <strong>' . $this->lang['datas_management'] . '</strong>
                        </h2>
                            <div class="row justify-content-end" style="margin: 2rem">
                            <div class="col-12 col-md-12 col-lg-5">
                                <a class="btn btn-outline-success" ' . $link . '>
                                    ' . $this->lang['export_db'] . '
                                </a>
                            </div>
                            <div class="col-12 col-md-12 col-lg-6 env-info">
                                ' . $this->lang['export_infos'] . '
                            </div>
                        </div>
                        
                        
                        
                        </div>';
        echo $result;
    }

    static function isselected($data, $dbtype)
    {
        return $data['db_typeMoodle'] == $dbtype ? ' selected' : '';
    }

    // public function langChoice($currentLang, $this->langs) {
    //     $retour = '<div class="installContainer">
    //     <h2>
    //     <strong>'.$this->lang['lang_choice'].'</strong>
    //     </h2>
    //     <select name="language" id="language" class="form-control col-2">';
    //     foreach ($this->langs as $name => $content) {
    //         if ($name === $currentLang) {
    //             $selected = ' selected="selected"';
    //         } else {
    //             $selected = '';
    //         }
    //         $retour .= '<option value="'.$name.'"'.$selected.'>'.$content.'</option>';
    //     }
    //     $retour .= '</select>
    //     </div>';

    //     echo $retour;
    // }
}
