<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 06/05/2019
 * Time: 15:51
 */

namespace App\HTML;


class legal extends html
{

    public function legalTitle($from){
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="'.$from.'">
                                <i class="fas fa-arrow-left leftArrow"></i>
                            </a>
                        </div>
                        <div class="col-10 col-sm-10">
                            <h1 style="margin-top: 3vh;" id="legalSize">
                                <i class="fas fa-gavel" style="color: #625a5a;"></i> '.$this->lang['legal'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function legalEditor(){

        $result = ' <div  class="row" style="">
                        <div class="col-12" id="editorContainer">
                            <div class="row" style="padding: 10px">
                                <div class="col-12">
                                    <input type="button"  class="btn btn-light height100" value="G" style="font-weight: bold;" onclick="commande(\'bold\');" />
                                    <input type="button"  class="btn btn-light height100" value="I" style="font-style: italic;" onclick="commande(\'italic\');" />
                                    <input type="button" value="S"  class="btn btn-light height100" style="text-decoration: underline;" onclick="commande(\'underline\');" />
                                    <button type="button" value="JustifyLeft"  class="btn btn-light height100" onclick="commande(\'justifyleft\');" ><i class="fa fa-align-left" aria-hidden="true"></i></button>
                                    <button type="button" value="JC"  class="btn btn-light height100" onclick="commande(\'justifycenter\');" ><i class="fa fa-align-center" aria-hidden="true"></i></button>
                                    <button type="button" value="JR"  class="btn btn-light height100" onclick="commande(\'justifyright\');" ><i class="fa fa-align-right" aria-hidden="true"></i></button>
                                    <button type="button" value="J"  class="btn btn-light height100" onclick="commande(\'justifyright\');" ><i class="fa fa-align-justify" aria-hidden="true"></i></button>
                                    <input type="button" value="Lien"   class="btn btn-light height100" onclick="commande(\'createLink\');" />
                                    <div class="dropup btn-default" style="display: inline">
                                        <select class="btn btn-default dropdown-toggle" onchange="commande(\'heading\', this.value); this.selectedIndex = 0;">
                                            <option value="">Titre</option>
                                            <option value="h1">'.$this->lang['title'].' 1</option>
                                            <option value="h2">'.$this->lang['title'].' 2</option>
                                            <option value="h3">'.$this->lang['title'].' 3</option>
                                            <option value="h4">'.$this->lang['title'].' 4</option>
                                            <option value="h5">'.$this->lang['title'].' 5</option>
                                            <option value="h6">'.$this->lang['title'].' 6</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';

        echo $result;
    }

    public function contenEditor($legalContent){
        $result = '<div class="row">
                        <div class="col-12">
                            <div id="editor" contentEditable><p>'.
                                $legalContent['value']
                            .'</p></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" style="text-align: center;margin: 1rem 0;">
                            <button id="myBtn" type="button" class="btn btn-success" onclick="result(sendHTML);">
                                '.$this->lang['record'].'
                            </button>
                        </div>
                    </div>';
        echo $result;
    }

    public function displayLegal($legalContent){

        $result = '<div class="row">
                        <div class="col-12">
                            <div id="display"><p>'.
                                $legalContent['value']
                            .'</p></div>
                        </div>
                  </div>';

        echo $result;
    }


}
