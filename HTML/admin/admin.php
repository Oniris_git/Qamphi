<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 15/04/2019
 * Time: 17:19
 */

namespace App\HTML;


class admin extends html
{

    public function adminMenu(){

        echo '<div class="col-12 col-sm-12 col-md-12 col-lg-12">';
            echo '	<div class="row justify-content-start">

                            </div>
	                        <div class="row justify-content_between padding_container">
                                <div class="admin_icon" id="shootMarges">
                                    <a href="index.php?page=list_session" class="btn btn-default">
                                        <i class="fas fa-edit iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['create'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=lang" class="btn btn-default">
                                        <i class="fas fa-flag iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['languages'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=theme" class="btn btn-default">
                                        <i class="fas fa-palette iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['theme'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=legal"  class="btn btn-default">
                                        <i class="fas fa-gavel iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['legal'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=auth" class="btn btn-default">
                                        <i class="fas fa-sign-in-alt iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['authorization'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=database" class="btn btn-default">
                                        <i class="fas fa-cogs iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['configuration'].'
                                    </div>
                                </div>
                                <div class="admin_icon" id="shootMarges">
                                    <a href="?page=statistics" class="btn btn-default">
                                        <i class="fas fa-chart-bar iconMenu btnHover"></i>
                                    </a>
                                    <div class="col-12 center margin_text">
                                        '.$this->lang['statistics'].'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div>';
    }

}
