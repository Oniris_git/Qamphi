<?php

namespace App\HTML;

abstract class html
{
    protected $lang;

    public function __construct() {
        global $LANG;
        $this->lang = $LANG;
    }
}