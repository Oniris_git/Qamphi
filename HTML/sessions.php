<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/03/2019
 * Time: 16:50
 */

namespace App\HTML;

use Endroid\QrCode\QrCode;

class sessions extends html
{

    public function sessionAccess(){
        $retour = ' <form action="index.php?page=authentication" method="post">
                             <h3 class="center titleFont">'.$this->lang['access_key'].'</h3>
                        <div class="row justify-content-center" style="margin-top:2rem">
                            <div class="col-7 col-sm-4 col-md-3" style="margin-bottom:1rem">
                                <input type="text" class="form-control width100 height100" name="tokenSession" style="font-size:inherit">
                            </div>
                            <div class="col-7 col-sm-4 col-md-3" style="margin-bottom:1rem">
                                <button type="submit" class="btn btn-outline-success width100" style="font-size:inherit">'.$this->lang['next'].'</button>
                            </div>
                        </div>
                    </form>';

        echo $retour;

    }

    //display session title with return button
    public function sessionTitle($from, $message)
    {
        $result = '<div class="row">
                        <div class="col-2 col-sm-1 homeIcon">
                            <a href="'.$from.'">
                                <i class="fas fa-arrow-left leftArrow"  style="margin-top: 4vh"></i>
                            </a>
                        </div>
                    </div>
                    <div class="justify-content-center">
                    </div>';

        echo $result;
    }

    public function sessionToken($session){
        $dirname = str_replace($_SERVER['DOCUMENT_ROOT'],'',dirname(__DIR__));
        $qamphi = str_replace('/public','',$dirname);
        $domain = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$qamphi;

        $page = $session->getAccess() == 'public'
            ? 'authentication&idSession='.$session->getId()
            : 'sessions';

        $link = $domain.'/public/index.php?page='.$page;

        $qrcode = new QrCode($link);

        if ($session->getAccess() == 'public') {
            $retour = ' 
            <form action="index.php?page=launch_session" method="post">
                    <div class="d-flex">
                        <div class="col-6 d-flex align-items-center justify-content-center" >
                            <div>
                                <h3 class="center titleFont" style="margin-top: 4vh">'.$this->lang['following_access_key'].'</h3>
                                <p class="d-flex justify-content-center" style="font-size:9rem;font-weight: 700">'.$session->getToken().'</p>
                                <p class="modal-title center font-italic">'.$this->lang['use_qrcode'].'</p>
                            </div> 
                            <input type="hidden" value="'.$session->getId().'" name="idSession">
                            <input type="hidden" value="1" name="order">
                        </div>
                        <div class="d-flex col-6 justify-content-center">
                            <img src="'.$qrcode->writeDataUri().'" class="qrcode">
                        </div>
                    </div>

                        <div class="row justify-content-center" >
                            <div class="col-8 col-sm-2">
                                <button type="submit" class="btn btn-outline-success width100" style="font-size:inherit">'.$this->lang['use'].'</button>
                            </div>
                        </div>
                    </form>';
        } else {
            $retour = '<div>
                            <h3 class="center titleFont" style="margin-top: 4vh">'.$this->lang['private_access_key'].'</h3>
                        </div> 
                <form action="index.php?page=launch_session" method="post">
                             <div class="modal-title align-items-center d-flex justify-content-center font-italic">'.$this->lang['flash_qrcode'].'</div>
                    <div class="d-flex justify-content-center">
                            <input type="hidden" value="'.$session->getId().'" name="idSession">
                            <input type="hidden" value="1" name="order">
                        <div class="justify-content-center">
                            <img src="'.$qrcode->writeDataUri().'" class="qrcode">
                        </div>
                    </div>

                        <div class="row justify-content-center" >
                            <div class="col-8 col-sm-2">
                                <button type="submit" class="btn btn-outline-success width100" style="font-size:inherit">'.$this->lang['use'].'</button>
                            </div>
                        </div>
                    </form>';
        }

        echo $retour;

    }

}
