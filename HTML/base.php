<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 08/01/2019
 * Time: 10:44
 */

namespace App\HTML;

class base extends html
{

    //display base header
    public function base_header($css_files = [])
    {
        echo '<!DOCTYPE html>
                <html lang="fr" id="html">
                    <head>
                        <link rel="stylesheet" href="css/lib/bootstrap.min.css">
                        <link rel="stylesheet" href="css/css/media_queries.css">
                        <link rel="stylesheet" href="css/css/app_global.css">';
            foreach($css_files as $link){
                echo '<link rel="stylesheet" href="css/css/'.$link.'.css">';
            }
            echo' <meta name="viewport" http-equiv="Content-Type" content="width=device-width, initial-scale=1.0, text/html; charset=utf-8"/> 
        				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
        				<title>Qamphi</title>
                        <link rel="shortcut icon" href="image/favicon.ico">
                        <script src="../public/css/lib/Chart.min.js"></script>
                    </head>
                    ';
    }

    //display base footer
    public function base_footer($optional_scripts = []){

        echo '	
                    <footer>
                        <script src="../public/css/lib/jquery-3.4.1.min.js"></script>
                        <script src="../public/css/lib/bootstrap.bundle.js"></script>
                        <script src="../public/JS/notification.js"></script>
                        <script src="../public/JS/toast.js"></script>
                        <script src="../public/JS/global.js"></script>';
        // Load specific js files for current page
        if ($optional_scripts !== []) {
            foreach ($optional_scripts as $script) {
                echo '<script src="../public/JS/'.$script.'.js"></script>';
            }
        }
    	echo			'</footer>';
    }

    public function close_body(){
        echo '                  </div>
                            </div>
                        </div>
                ';
    }

    public function open_body($backLink){
        echo'<body style="background-image:url('.$backLink.')">
                <div id="cible">
                    <div id="global">';
    }


    /**
     * @param $backLink
     * @param $width
     * @param string $firefox_height Firefox does not implement "display: table" like a real browser would do. We need to cheat to get through that with margin-top.
     * Remove following style balise and $firefox_height (and it's implementations) when display: table is working in firefox
     */
    public function openFlexBody($backLink, $width, $firefox_height = '30'){
        switch($width){
            case "large":
                $class="offset-1 col-10 offset-sm-2 col-sm-8 offset-md-1 col-md-10 offset-lg-0 col-lg-12";
                break;
            case "XL":
                $class="col-lg-12";
                break;
            case "middle":
                $class="col-12 col-sm-12 offset-md-1 col-md-10 offset-lg-2 col-lg-8";
                break;
            case "little":
                $class="col-12 offset-sm-1 col-sm-10 offset-md-1 col-md-10 offset-lg-3 col-lg-6";
                break;
            default:
                $class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-10";
                break;
        }
        echo '<body style="background-image:url('.$backLink.')" id="body">
                <div class="container-fluid h-100 w-100 d-flex align-items-center moz-margin">
                <style type="text/css" rel="stylesheet">
                @-moz-document url-prefix() {
                    .moz-margin {
                        margin-top: '.$firefox_height.'vh                    
                    }
                }                
                </style>
                        <div class="'.$class.' d-flex flex-row justify-content-around" id="menuContent">';
    }

    //display start table or end / $type
    public function  table($type, $from = null)
    {
        switch ($type){
            case 'start':
                $class = 'container';
                $id = 'authContent';
                $exclude = false;
                break;
            case 'start-fluid':
                $class = 'container-fluid';
                $id = 'authContent3';
                $exclude = false;
                break;
            case 'start-without-btn':
                $class = 'container-fluid';
                $id = 'authContent3';
                $exclude = true;
                break;
            case 'end':
                echo '</div></div></div></div></div>';
                break;
            case 'without-btn':
                $class = 'container-fluid';
                $id = 'authContent3';
                $exclude = true;
                break;
            case 'flex':
                $class='';
                $id='';
                $exclude = true;
                break;
            case 'flex-btn':
                $class='';
                $id='';
                $exclude = false;
                break;
            default:
                $class = 'container';
                $id = 'authContent';
                $exclude = false;
                break;
        }
        if ($type == 'flex' || $type == 'flex-btn'):
             echo '<div class="d-flex flex-column w-100 h-100">
						<div class="homeIcon">';
                        if($exclude != true):
						echo '<a href="'.$from.'">
								<i class="fas fa-arrow-left leftArrow" ></i>
							</a>';
						endif;
				  echo '</div>
					<div class="w-100">
						<table class="table table-hover">
							<thead class="center titleFont">
								<tr>';
        elseif ($type == 'end') :
            echo '</div></div></div></div></div></div>';
        elseif($type != 'end'):
            echo '
                    <div class="'.$class.' width100 height100">
                        <div class="row justify-content-center ">
                            <div class="col-11" id="'.$id.'">
                                <div class="recContent">
                                    <div class="row justify-content-start">
                                        <div class="col-2 homeIcon">';
                                        if($exclude != true){
                                            echo'<a href="'.$from.'">
                                                    <i class="fas fa-arrow-left leftArrow"></i>
                                                </a>';
                                        }
                                        echo'</div></div>';
        endif;
    }

    //display errors
    public function displayError($ERROR = null){
        if ($ERROR != null) :
            foreach ($ERROR as $e):
                echo '<div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="text-align: center"><strong>'.$e.'</strong></p>
                     </div>';
            endforeach;
        endif;
    }

    //display valid alert
    public function displayValid($VALID = null){
        if ($VALID != null) :
            foreach ($VALID as $e):
                echo '<div class="alert alert-success" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="text-align: center"><strong>'.$e.'</strong></p>
                     </div>';
            endforeach;
        endif;
    }

    //display errors
    public function displayWarning($WARNING = null){
        if ($WARNING != null) :
            foreach ($WARNING as $e):
                echo '<div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="text-align: center"><strong>'.$e.'</strong></p>
                     </div>';
            endforeach;
        endif;
    }

    public function footer($role){
        echo   '</div></div>
                    <div class="footerLink center">';
                        if($role != "ANON"){
                            echo '<a href="?page=logout" class="footer_margin">
                                   <i class="fas fa-sign-out-alt"></i>&nbsp; '.$this->lang['disconnect'].'
                                </a>
                                <a href="?page=profile" class="footer_margin">
                                   <i class="fas fa-user"></i>&nbsp; '.$this->lang['profile'].'
                                </a>';
                             }
                        echo ' <a href="?page=about" class="footer_margin"><i class="fas fa-bookmark">
                                    </i> '.$this->lang['about'].'
                                 </a>
                                <a href="?page=legal" class="footer_margin"><i class="fas fa-gavel">
                                    </i>&nbsp; '.$this->lang['legal'].'
                                </a>
                                <a href="?page=lang_choice" class="footer_margin"><i class="fas fa-language">
                                    </i>&nbsp; '.$this->lang['languages'].'
                                </a>
                    </div>';

    }


    public function open_form_container() {
        echo '<div class="form-bg">
      <div class="phone">
        <div class="phone_mic"></div>
        <div class="phone_screen">
';
    }

    public function close_form_container() {
        echo '</div>
        <div class="phone_button"></div>
      </div>
    </div>';
    }
}
