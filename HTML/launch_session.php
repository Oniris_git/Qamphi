<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 21/03/2019
 * Time: 10:20
 */

namespace App\HTML;


class launch_session extends html
{

    //display question and its proposals
    public function launchSession($question, $listProposition)
    {
        $separator = $this->lang['question_number'] == '' ? '. ' : ' : ';
        $retour = '<div class="row">
						<div class="col-10 sessionFontTitle">
						    <h4 style="margin-top: 5vh; font-size: 4vh" class="marginLeft">
                                '.$this->lang['question_number'] . $question->getOrder() .$separator. $question->getContent() . '
                            </h4>
                        </div>
                        <div class="col-2 fontSizeLaunch marginLaunch2" >
							<i class="fas fa-users"> <span id="nbr-responses"></span> / <span id="nbr-participants"></span></i> 
						</div> 
                    </div>';

        foreach ($listProposition as $proposition) {
            $retour .= '<div class="row justify-content-center marginLaunch marginLeft" >
						<div class="col-1 sessionFont">
							' . $proposition->getOrder() . '-
						</div>
					<div class="col-9 sessionFont">
						' . $proposition->getContent() . '
					</div>
				</div>';
        }

        $retour .= '<br>';

        echo $retour;
    }

    //display and order responses
    public function sessionReview($question, $listProposition, $tabCount, $totalCount, $session, $countResponse)
    {
        $separator = $this->lang['question_number'] == '' ? '. ' : ' : ';
        $retour = '<div class="row">
						<div class="col-10 sessionFontTitle">
						    <h4 style="margin-top: 5vh; font-size: 4vh" class="marginLeft">
                                '.$this->lang['question_number'] . $question->getOrder() .$separator. $question->getContent() . '
                            </h4>
                        </div>
                        <div class="col-2 fontSizeLaunch marginLaunch2" >
							<i class="fas fa-users">'.' '.$countResponse.'/'.$session->getParticipants().'</i> 
						</div> 
                    </div>';
        $i = 0;
        foreach ($listProposition as $proposition) {
            $retour .= '<div class="row" style="margin: 3vh">
						<div class="col-1 sessionFont marginLeft" style="text-align: end">
							' . $proposition->getOrder() . '- 
						</div>
						<div class="col-6 sessionFont">
							' . $proposition->getContent() . '
						</div> 
						<div class="col-1 sessionFont">
							' . $tabCount[$i] . '
						</div>
						<div class="col-3">
							' . $this->statBar($proposition->getCorrect(), $totalCount, $tabCount[$i]) . '
						</div>
					</div>';
            $i++;
        }

        $retour .= '</table><br><br>';

        echo $retour;
    }

    //answers percentage calculation and display a proportional-sized bar
    public function statBar($correct, $nbtot, $nb)
    {
        if ($nbtot != 0) {
            $pourcent = number_format($nb / $nbtot * 100, 0);
            if ($pourcent === 'nan'): $pourcent = 1; endif;
            $bgcolor = ($correct == 0) ? 'red1' : 'green1';
            $retour = '<div class="sessionFont" style="width:' . $pourcent . '% ;
                                    background-image: url(\'image/' . $bgcolor . '.jpg\');
                                    background-repeat: no-repeat;background-size:cover;" 
                                    border=0 
                                    cellspacing=0>&nbsp;
                       </div>';
            return $retour;
        }

    }

    //display next question button
    public function navigationReview($question, $countQuestion, $page)
    {
        $nextQuestion = $question->getOrder() + 1;
        $previousQuestion = $question->getOrder();

        echo '<div class="row justify-content-md-center">';

        if ($previousQuestion != 0) {
            echo '	<div class="col-4">
                        <form method="post" action="index.php?page=launch_session">
                            <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $previousQuestion . '">
                            <button type="submit" class="btn btn-outline-secondary width100" style="margin-bottom: 5vh;">'.$this->lang['previous'] .'</button>
                        </form>
					</div>';
        }

        if ($nextQuestion <= $countQuestion) {
            echo '    <div class="col-4">
                            <form method="post" action="index.php?page=launch_session">
                                <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                                <input type="hidden" name="order" value="' . $nextQuestion . '">
                                <button type="submit" class="btn btn-outline-secondary width100" style="margin-bottom: 5vh;">'.$this->lang['next'] .'</button>
                            </form>
                        </div>';
        }
        if($page == "question"){
            echo '    <div class="col-4">
                            <form action="index.php?page=launch_session" method="post">
                                <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                                <input type="hidden" name="order" value="' . $question->getOrder() . '">
                                <button type="submit" class="btn btn-outline-success width100" name="request" value="response" style="margin-bottom: 5vh;">'.$this->lang['response'] .'</button>
                            </form>
                        </div>
                    </div>';
        }else if (($nextQuestion > $countQuestion) && $page == "stats"){
            echo '    <div class="col-4">
                           <form method="post" action="index.php?page=launch_session">
                                <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                                <button type="submit" class="btn btn-info width100" style="margin-bottom: 5vh;" name="request" value="stats">'.$this->lang['statistics'] .'</button>
                           </form>                           
                       </div> 
                       <div class="col-4">
                            <a type="button" class="btn btn-outline-dark width100" href="index.php?page=list_session">'.$this->lang['home'].'</a>
                       </div>';
        }

    }

    public function session_id($sId) {
        echo '<input type="hidden" id="session_id" value="'.$sId.'">';
    }

    //previous question button
    public function navigationQuestion($question)
    {
        $previousQuestion = $question->getOrder() - 1;

        echo '<div class="row justify-content-md-center">
        <input type="hidden" id="session_id" value="' . $question->getSessionId() . '">
        <input type="hidden" id="question_order" value="' . $question->getOrder() . '">';

        if ($previousQuestion != 0) {
            echo '	<div class="col-4">
                        <form method="post" action="index.php?page=launch_session">
                            <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $previousQuestion . '">
                            <button type="submit" class="btn btn-outline-secondary width100" style="margin-bottom: 5vh;">'.$this->lang['previous'] .'</button>
                        </form>
					</div>';
        }

        echo '    <div class="col-4">
                        <form action="index.php?page=launch_session" method="post">
                            <input type="hidden" name="idSession" value="' . $question->getSessionId() . '">
                            <input type="hidden" name="order" value="' . $question->getOrder() . '">
                            <button type="submit" class="btn btn-outline-success width100" name="request" value="response" style="margin-bottom: 5vh;">'.$this->lang['responses'] .'</button>
                        </form>
                    </div>
                </div>';
    }

    public function statPage($users, $from, $actualSession)
    {
        if($users != null && !empty($users)) {
            $retour = '</div></div></div><div class="row justify-content-center" style="margin: 3rem 0 0 0">
                            <div class="col-12" id="user-scores" >
                            <div class="row justify-content-center" id="titleSession">';
            if ($actualSession->getAccess()==='public') {
                $retour .=   		'
                                        <span>'.$this->lang['score'].'</span>
                                    </div>
                                <div class="row justify-content-start">';
                foreach ($users as $key => $user) {

                $retour .= '<div class="col-12 score">
                            <div class="row justify-content-center">
                                    <div class="col-3" style="text-align:left">';
                                    if(strlen($key) > 3 && substr($key, 0, 4) == "Anon"){
                                        $retour .= '<span>'.$this->lang['anonymous'].'</span>';
                                    }else{
                                        $retour .= '<span>' . $key . '</span>';
                                    }
                        $retour .= '</div>
                                    <div class="col-2">
                                        <span>' . $user . '</span>
                                    </div>
                            </div>
                            </div>';

                    }
                }
            } else {
                $retour = "";
            }
            $retour .= '</div></div></div>
                            <div class="row justify-content-center marginLaunch">
                                <div class="col-4 center">
                                    <a type="button" class="btn btn-outline-dark" href="index.php?page=list_session">'.$this->lang['home'].'</a>
                                </div>
                            </div>';

        echo $retour;
    }
    function statSwitch($list) {
        $retour = "<div class = 'statSwitch'>
                    <label class='switch'>
                    <input type='radio' value='on' onclick='showAdvancedStats(".$list.");' checked>
                    <span class='slider round'></span>
                </label>
                <p>&nbsp&nbsp".$this->lang['advanced_stats']."</p>
                </div>";
        echo $retour;
    }

    function openstats() {
        echo '<div id="statsBlock">
                <div id="generalStats">';
    }

    function closestats() {
        echo '<div>
            </div>';
    }

    function resultBar($tabCount){
        if($tabCount['total'] != 0){
            $perCentCorrect = ($tabCount['correct'] * 60) / intval($tabCount['total']);
            $perCentIncorrect = ($tabCount['incorrect'] * 60) / intval($tabCount['total']);
            $perCentPartial = ($tabCount['partial'] * 60) / intval($tabCount['total']);
            $perCentNonResponse = 60 - ($perCentCorrect + $perCentPartial + $perCentIncorrect);
            $totalNonResponse = $tabCount['total'] - ($tabCount['correct'] + $tabCount['incorrect'] + $tabCount['partial']);
        }else{
            $perCentCorrect = 0;
            $perCentIncorrect = 0;
            $perCentPartial = 0;
            $perCentNonResponse = 0;
            $totalNonResponse = 0;
        }

        $result = '<div class="row" style="text-align:center;margin:1rem">';
                        if($perCentCorrect != 0){
        $result .=      '<div style="background-color: #51e295;width:'.$perCentCorrect.'%;color:white;" class="resultStatBar">'.$tabCount['correct'].'
                        </div>';}
                        if($perCentPartial != 0){
        $result .=      '<div style="background-color: #fad046;width:'.$perCentPartial.'%;color:white" class="resultStatBar" cellspacing=0>'.$tabCount['partial'].'
                        </div>';}
                        if($perCentIncorrect != 0){
        $result .=      '<div style="background-color: #fa4557;width:'.$perCentIncorrect.'%;color:white" class="resultStatBar" cellspacing:0>'.$tabCount['incorrect'].'
                        </div>';}
                        if($perCentNonResponse != 0){
        $result .=          '<div style="background-color: #f0f0f0;width:'.$perCentNonResponse.'%;" class="resultStatBar" cellspacing:0>'.$totalNonResponse.'
                        </div>';}
         $result .= '</div>';
        return $result;

        //same thing with bootstrap
        /*$result = '<div class="progress" style="height:2rem;margin:1rem">
                        <div class="progress-bar bg-success" role="progressbar" style="width: '.$perCentCorrect.'%" aria-valuenow="'.$perCentCorrect.'" aria-valuemin="0" aria-valuemax="100">'.$tabCount['correct'].'</div>
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">'.$tabCount['partial'].'</div>
                        <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">'.$tabCount['incorrect'].'</div>
                    </div>';
        return $result;*/
    }

}
