<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 24/05/2019
 * Time: 16:10
 */

namespace App\HTML;


class success extends html
{
    function success($title = null){
        $result = '';
        if($title != null){
            $result = '<h2 style="text-align:center; margin:1rem">'.$this->lang['end_session'].'</h2>';
        }

        $result .= '<div class="row justify-content-center">
                <div class="col-4">
                    <img src="../public/image/success2.png" class="width100">
                </div>
             </div>
             <div class="row justify-content-center" style="margin: 1rem">
                <div class="col-12 col-sm-6 col-lg-6 col-xl-4">
                    <a type="button" href="?page=main" class="btn btn-outline-success width100">'.$this->lang['home'].'</a>
                </div>
             </div>';
        echo $result;
    }

}
