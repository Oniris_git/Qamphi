<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/03/2019
 * Time: 16:16
 */

namespace App\HTML;


class authentification extends html
{
    public function form_auth($user_name, $session, $moodle){
		$auths = explode(',',$session->getAuth());
        $retour = '<div class="row ">
						<div class="col-12">
							<div class="contentAuth">
								<b>'.$this->lang['connection'].'</b>
							</div>
							<div class="row justify-content-md-center">';
		if (in_array('anon', $auths)) {
		$retour .=					'<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 heightBtn" id="shootMarges" >
									<form action="index.php?page=participate" method="POST">
										<input type="hidden" name="username" value="anon">
										<button type="submit" class="btn btn-light formAnon" value="Anonyme">
											<img src="image/auth_avatar.png" class="imgAnon">
									        <input type="hidden" name="order" value="'.$session->getCurrent().'">
									        <input type="hidden" name="idSession" value="'.$session->getId().'">
											<div class="btnAnon">'.$this->lang['anonymous'].'</div>
										</button>
									</form>
								</div>';
		}
		if (in_array('pseudo', $auths)) {
		$retour .=				'<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 heightBtn" id="shootMarges">
									<form action="index.php?page=participate" method="POST">
										<div class="row">
											<div class="col-sm-12">
												<button type="submit" class="btn btn-light formAnon">
													<img src="image/auth_avatar.png" class="imgAnon">
													<input type="hidden" name="order" value="'.$session->getCurrent().'">
													<input type="hidden" name="idSession" value="'.$session->getId().'">
													<input placeholder="Pseudo" id="inputPseudo" type="text" class="form-control" name="username" required>
												</button>
											</div>
										</div>
									</form>
								</div>';
		}

        if($user_name != "ANON" && $moodle == "on" ){
            $retour .='<div class="col-12 col-sm-12 col-md-12  col-lg-12 col-xl-4 heightBtn" id="shootMarges">
            			<form action="index.php?page=participate" method="POST"> 
							<input type="hidden" name="username" value="'.$user_name.'"> 
							<input type="hidden" name="order" value="'.$session->getCurrent().'">
							<input type="hidden" name="idSession" value="'.$session->getId().'">
							<button type="submit" class="btn btn-light formAnon"> 
								<img src="image/auth_moodle2.png" class="imgAnon">
							</button>
						</form>';
        }else if ($moodle == "on" && in_array('moodle', $auths)){
            $retour .='<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 heightBtn" id="shootMarges">
            			<center><a type="button" class="formAnon" data-toggle="modal" data-target="#modalLoginMoodle">
                               <img src="image/auth_moodle2.png" class="imgAnon">
                            </a></center>';
        }
        $retour .='</div>
							</div>
						</div>
					</div>';
        echo $retour;
    }
}
