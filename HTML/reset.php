<?php

namespace App\HTML;


class reset extends html
{

    public function resetForm($user){

        $result = '<form method="post" action="?page=resetPw&token='.$user->getToken().'">
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                '.$this->lang['new_password'].'
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="password" name="newPassword" required>
                            </div>
                        </div>
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                '.$this->lang['confirm_password'].'
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="password" name="confirmNewPassword" required>
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin: 2rem 0">
                            <div class="col-4">
                                <button type="submit" class="btn btn-outline-success width100">'.$this->lang['alter'].'</button>
                            </div>
                        </div> 
                    </form>';

        echo $result;
    }
}
