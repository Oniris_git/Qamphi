<?php

namespace App\HTML;

use App\BO\User;

class profile extends html
{
    public function profile_title() {
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">
                            <a href="index.php?page=main">
                                <i class="fas fa-arrow-left leftArrow"></i>
                            </a>
                        </div>
                        <div class="col-10">
                            <h1 style="margin-top: 3vh">
                                <i class="fas fa-user" style="color: #625a5a"></i> '.$this->lang['profile'].'
                            </h1>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function profile_form(User $user) {
        $disabled = '';
        if ($user->getAuth() == 'moodle') {
            $disabled = ' disabled';
        }

        $result = '<form method="post" action="?page=profile">
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                Email
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="email" name="email" value="'.$user->getEmail().'"'.$disabled.'>
                            </div>
                        </div>
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                '.$this->lang['password_modify'].'
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="password" name="newPassword"'.$disabled.'>
                            </div>
                        </div>
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                '.$this->lang['confirm_password'].'
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="password" name="confirmNewPassword"'.$disabled.'>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin: 2rem 0">
                            <div class="col-12 col-sm-6">
                                '.$this->lang['current_password'].'
                            </div>
                            <div class="col-12 col-sm-6">
                                <input class="form-control" type="password" name="currentPassword" required'.$disabled.'>
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin: 2rem 0">
                            <div class="col-4">
                                <button type="submit" class="btn btn-outline-success width100"'.$disabled.'>'.$this->lang['alter'].'</button>
                            </div>
                        </div> 
                    </form>';

        echo $result;

    }
}