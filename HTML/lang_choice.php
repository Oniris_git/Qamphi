<?php


namespace App\HTML;

class lang_choice  extends html
{

    public function langMenu($allLangs, $from) {

        $retour = '<div class="container-fluid h-100 w-100 d-flex align-items-center" style="padding:1rem;" id="">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    	<div class="row justify-content-center">
                            <h1 class="titleFont" style="">'.$this->lang['languages'].'</h1>
                            </div>
                            <form method="POST" action="?page=lang_choice">
                            <input type="hidden" name="from" value="'.$from.'">
                            <div class="row justify-content-center" style="padding: 1rem 1rem 0 1rem; margin-bottom:1rem; margin-top:2rem">';

        foreach ($allLangs as $name => $content) {

            $formats = ['jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF'];
            foreach ($formats as $format) {
                if (file_exists("./../public/image/".$name.".".$format)) {
                    $fileFormat = $format;
                }
            }

        $retour .=    '<div class="col-12 col-sm-12 col-md-12 col-lg-2 center" id="shootMarges">
                        <button name="language" value="'.$name.'" class="unstyleBtn trash delete-question" style="text-align: center;">
                            <img class="imgLang" src="./../public/image/'.$name.'.'.$fileFormat.'" alt="'.$content.'" height="72" width="128" style="margin-bottom: 10%;">
                        </button>
                    </div>' ;
        }

        $retour .=           '</div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>';

                echo $retour;
    }

}