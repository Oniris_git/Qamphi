<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 14/05/2019
 * Time: 11:22
 */

namespace App\HTML;


class login extends html
{

    public function loginMoodleForm($token, $idSession){

        $result = ' <div class="modal fade" id="modalLoginMoodle" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                             <div class="modal-content">
                                <div class="modal-header">
                                    <h5 style="font-family:Indie Flower">'.$this->lang['connection'].' Moodle</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>';
        $result .= $this->basicForm($token, 'moodle', $idSession);
        return $result;
    }

    public function basicForm($token, $type, $idSession){

        $result ='<form action="?page=participate" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            '.$this->lang['id'].'
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <input type="text" class="form-control width100" name="username">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            '.$this->lang['password'].'
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <input type="password" class="form-control width100" name="password">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-4">
                            <input type="hidden" name="token" value="'.$token.'">
                            <input type="hidden" name="typeAuth" value="'.$type.'">
                            <input type="hidden" name="idSession" value="'.$idSession.'">
                            <button type="submit" class="btn btn-outline-success width100"><i class="fas fa-sign-in-alt"></i> '.$this->lang['enter'].'</button>
                        </div>
                    </div>';
            $result .=            $this->passwordReset();
            $result .='                </form>
                                    </div>
                                </div>
                             </div>
                         </div>
                     </div>';
                     return $result;
    }

    public function titleLogin(){

      echo '<div class="d-flex flex-column w-100">
                <div class="d-flex flex-row w-100">
                    <div class="homeIcon justify-content-start">
                        <a href="index.php?page=main">
                            <i class="fas fa-arrow-left leftArrow"></i>
                        </a>
                    </div>
                    <div class="h-100 w-100 margin_left">
                        <div class="d-flex justify-content-center" ><h1 class="titleFont">
                            '.$this->lang['connection'].'
                        </h1></div>
                    </div>
                </div>';
    }


    public function passwordReset(){
        $result = '<div class="row" id="passwordForm">
                        <div class="col-12">
                            <a data-toggle="collapse" href="#collapsePassword" role="button" aria-expanded="false" aria-controls="collapsePassword">
                                    '.$this->lang['password_reset'].'
                            </a>
                        </div>
                    </div>
                    <div class="w-100">
                        <div class="collapse col-12" id="collapsePassword">
                            <div class="row">
                                <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Email" name="emailReset">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-success" type="submit">
                                                Envoyer
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
        return $result;
    }

    public function login($token, $type, $idSession){
        $result ='<div class="d-flex justify-content-center">
                    <div id="formLogin"  class="w-100">
                        <form action="?page=login" method="post">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6">
                                    '.$this->lang['id'].'
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" class="form-control width100" name="username" autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6">
                                    '.$this->lang['password'].'
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="password" class="form-control width100" name="password">
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12 col-sm-10 col-md-8 col-lg-6">
                                    <input type="hidden" name="token" value="'.$token.'">
                                    <input type="hidden" name="idSession" value="'.$idSession.'">
                                    <button type="submit" class="btn btn-outline-success width100">
                                        <i class="fas fa-sign-in-alt"></i> '.$this->lang['enter'].'
                                    </button>
                                </div>
                            </div>';
                            $result .=$this->passwordReset();
                            $result .='                </form>
                                        </div>
                                    </div>
                                 </div>
                             </div>
                         </div>';
                         echo $result;
    }

}
