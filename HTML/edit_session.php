<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 13/03/2019
 * Time: 13:56
 */

namespace App\HTML;


use App\BO\Questions\Question;
use App\BO\User;

class edit_session extends html
{

    //change session title form
    public function sessionEdit($session, $keys)
    {
        $show = $session->getAccess() == 'private' ? 'onclick="createVisibilityPreferences(\''.$session->getId().'\',\''.$keys.'\')"' : '';
        $retour='<div class="row justify-content-md-center" id="session_edit">
					<div class="col-12"> ';
        $retour.='<div class="row justify-content-md-center marginTop" id="session">
                    
						<div class="col-sm-2 col-md-2 col-xl-1">
							'.$this->lang['session'].'
						</div>
						<div class="col-sm-9 col-md-9">
							<input type="text" class="form-control" name="title" value="'.$session->getTitle().'" 
							    required id="inputSession" size="128" maxlength="128" placeholder="'.$this->lang['session_title'].'">
                        </div>
                        <button type="button" class="unstyleBtn colorArrow" data-toggle="modal" data-target="#sessionPreferences" '.$show.'>
                        <i class="fas fa-wrench fa-2x"></i>
                        </button>
                        </i>
					</div>';
        $retour.='</row>
                    <div id="allBlock">';
        echo $retour;
    }

    public function openForm($id){
        echo '<form id="formSession" action="index.php?page=edit_session&id='.$id.'#ink" method="POST" enctype="multipart/form-data">';
    }

    //save and quit button
    public function buttonEdit_save_quit($idSession)
    {
    echo '<button onmouseover="question_info(0,'.$idSession.')" onmouseleave="empty_question_info()" name="action" value="save_quit" id="btnSave" type="submit" style="margin: 0 0.5rem 0.5rem 1rem;" class="btn btn-outline-success accountTypeTeacher">
            <i class="fas fa-sign-out-alt fa-3x"></i>
        </button>
        </div>
        <div id="question-info-container" style="height: 5vh; text-align: center;">

                </div>';
    }

    public function buttonAddQuestion($idSession, $types) {
        $result =  '</div>

                 <div style="text-align : center;"><br>';
        foreach ($types as $type) {
            $question = Question::instanciate([
                'type_id' => $type->getId()
            ]);
            if ($type->getId() != 1) {
                $result .=  '<button onmouseover="question_info('.$type->getId().','.$idSession.')" 
                                        onmouseleave="empty_question_info()" 
                                        title="'.$this->lang[$type->getName()].'" 
                                        id="questionTypeBtn'.$type->getId().'" 
                                        onclick="saveAndAdd('.$idSession.','.$type->getId().')" 
                                        type="button" 
                                        style="margin: 0 0.5rem 0.5rem 1rem; color: #d1c9cf; border-color: #d1c9cf;" 
                                        class="btn btn-outline-success accountTypeTeacher add_question-button">
                                        '.$question::ICON.'
                                        </button>';
            }
        }

        echo $result;
    }

    public function options_modal($session, $moodle, $keys, $guests) {
        $s_id = $session->getId();
        $auth = $session->getAuth();
        $display = $session->getAccess() == "public" ? '' : 'style="display: none;"';
        //$keys = 0;
        $authArray = explode(',',$auth);
        if ($moodle==="off") {
            $disabled=" disabled";
        } else {
            $disabled="";
        }
        $result = '
        <div class="right">

        </div>
        <div class="modal fade" id="sessionPreferences" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onshow="initModal('.$session->getId().')" onhide="deleteCSV('.$session->getId().')">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fas fa-wrench"></i>&nbsp;&nbsp;'.$this->lang['sessions_options'].'</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="sessions_options_form" method="post" action="index.php?page=edit_session&id='.$s_id.'" onsubmit="return checkAuths(event)">        
                <div id="optionsModal">       

                    <input type="hidden" id="session_access_keys" value="'.$keys.'">

                    <div class="installContainer" id="visibility-container">
                    <p>'.$this->lang['session_visibility'].' : </p>
                    
                    <div class="env-info">
                        '.$this->lang['session_visibility_infos'].'
                    </div><br> 
                    <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="public" name="visibility" value="public" '.$this->check_visibility('public', $session->getAccess()).' onchange="changeModal('.$session->getId().')" onclick="return warn()">
                    <label class="custom-control-label" for="public">'.$this->lang['public_visibility'].'</label>
                    </div>

                    <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="private" name="visibility" value="private" '.$this->check_visibility('private', $session->getAccess()).' onchange="changeModal('.$session->getId().')">
                    <label class="custom-control-label" for="private">'.$this->lang['private_visibility'].'</label>
                    </div>
                    
                    <input type="hidden" name="keyTrad" value="'.$this->lang['keys_number'].'">
                    <input type="hidden" name="csvTrad" value="'.$this->lang['csv_export'].'">
                    <input type="hidden" name="authTrad" value="'.$this->lang['auth_required'].'">
                    <input type="hidden" name="confirmMsgKeys" value="'.$this->lang['confirm_delete_keys'].'">
                    <input type="hidden" name="showKeysTrad" value="'.$this->lang['show_keys'].'">

                    </div>
                    
                                    
                    <div class="installContainer" id="auths"'.$display.'>
                    <p>'.$this->lang['authentications'].' : </p>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="anon" id="anon"'.$this->is_allowed('anon', $authArray).'>
                        <label class="form-check-label" for="anon">'.$this->lang['anonymous'].'</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="pseudo" id="pseudo"'.$this->is_allowed('pseudo', $authArray).'>
                        <label class="form-check-label" for="pseudo">Pseudo</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="moodle" id="moodle"'.$this->is_allowed('moodle', $authArray).$disabled.'>
                        <label class="form-check-label" for="moodle">Moodle</label>
                    </div>

                    </div>
                    </div>                                                          
                </form> 
                
                    <div class="installContainer">
                    <p>'.$this->lang['session_share'].' : </p>                   
                    <div id="guests_container">';
                    $result .= $this->guests_list($guests, $session->getId());
                    $result .= '</div>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                      </div>
                    <input id="search_user_input" type="text" class="form-control" name="search" oninput="search_user('.$s_id.')" placeholder="'.$this->lang['session_add_guest'].'">
                    </div>
                    <div id="search-container">                    
                    
                    </div>   
                    </div>
                <div class="row justify-content-center">
                <div class="col-4">
                    <button id="optionsBtn" type="submit" name="action" value="options" class="btn btn-outline-success width100" onclick="saveOptions('.$s_id.')">'.$this->lang['record'].'</button>
                </div>
            </div>               
              </div>
            </div>
          </div>
        </div>';

        echo $result;
    }

    public function add_guest(User $guest, $sessionid) {

        $result = '<tr id="tr-'.$guest->getId().'">
                    <td>'.$guest->getUsername().'</td>
                    <td>
                        <labe for="read">'.$this->lang['session_read'].'</labe>
                        <input type="radio" name="right-'.$guest->getId().'" value="read" id="'.$guest->getId().'-read" checked>
                    </td>
                    <td>
                        <label for="write">'.$this->lang['session_write'].'</label>
                        <input type="radio" name="right-'.$guest->getId().'" value="write" id="'.$guest->getId().'-write">
                    </td>
                    <td>
                        <button type="button" class="unstyleBtn" onclick="add_guest(\''.$guest->getId().'\',\''.$sessionid.'\')">
                            <i class="fas fa-plus-circle hvr-pulse"></i>
                        </button>
                    </td>
                </tr>';

        return $result;
    }

    public function guests_list($guests, $idSession) {
        $output = '<table class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    '.$this->lang['username'].'
                                </th>
                                <th colspan="2" style="text-align: center">
                                </th>
                                <th>
                                </th>
                                
                            </tr>
                        </thead>';
        foreach ($guests as $guest) {
            $read_check = $guest['auth'] === 'read' ? ' checked' : '';
            $write_check = $guest['auth'] === 'write' ? ' checked' : '';
            $output .= '<tr>
                    <td>'.$guest['user']->getUsername().'</td>
                    <td>
                        <labe for="read">'.$this->lang['session_read'].'</labe>
                        <input type="radio" onchange="update_rights(\''.$guest['user']->getId().'\',\''.$idSession.'\')" name="right-'.$guest['user']->getId().'" value="read" id="'.$guest['user']->getId().'-read"'.$read_check.'>
                    </td>
                    <td>
                        <label for="write">'.$this->lang['session_write'].'</label>
                        <input type="radio" onchange="update_rights(\''.$guest['user']->getId().'\',\''.$idSession.'\')" name="right-'.$guest['user']->getId().'" value="write" id="'.$guest['user']->getId().'-write"'.$write_check.'>
                    </td>
                    <td>
                        <button type="button" name="action" value="" class="unstyleBtn trash delete-question" onclick="delete_guest(\''.$guest['user']->getId().'\',\''.$idSession.'\')">
                            <i class="fas fa-trash-alt iconSize" ></i>
                        </button>
                    </td>
                </tr>';
        }
        $output .= '</table>';
        return $output;
    }

    public function open_guests_table() {
        $output = '<table class="table table-hover">
                        <thead>
                            <tr>
                                <th>
                                    '.$this->lang['username'].'
                                </th>
                                <th colspan="2" style="text-align: center">
                                </th>
                                <th>
                                </th>
                                
                            </tr>
                        </thead>';

        return $output;
    }

    public function close_guest_table() {
        return '</table>';
    }


    private function check_visibility($visibility, $access) {
        if ($visibility === $access) {
            return ' checked';
        }
        return '';
    }

    private function is_allowed($auth, $authArray) {
        if (in_array($auth, $authArray)) {
            return 'checked';
        }
        return '';
    }

}
