<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 25/03/2019
 * Time: 15:27
 */

namespace App\HTML;


class error extends html
{

    public function showError($messsage, $error = null){

        $formats = ['jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF'];
        foreach ($formats as $format) {
            if (file_exists("../public/image/error.".$format)) {
                $fileFormat = $format;
            }
        }

        echo "  
                    <div class='col-10 errorBloc' id='error'>
                        <div class='row justify-content-center' id='shootRow'>
                            <div class='errorMsg'>
                                <strong>".$messsage."</strong>
                            </div>
                        </div>
                        <br>";
        if ($error!= null) {
            echo '<div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="text-align: center"><strong>'.$error.'</strong></p>
                     </div>';
        }

          echo  "<div class='row justify-content-start' id='shootRow'>
                            <div class='col-12 col-lg-4'>
                                <img style='float:left' class='imgHomer' src='../public/image/error.$fileFormat'/>
                            </div>
                            <div class='col-12 col-lg-4 center'>
                                <a href='index.php?page=main' type='button' class='btn btn-outline-warning errorBackHome' >".$this->lang['home']."</a>
                            </div>
                    </div>
                </div>";

    }

}
