<?php

namespace App\HTML;

class confirm extends html
{

    public function showSummary($recap, $order)
    {
        $retour = "<div id='questionTitle'>" . $this->lang['question_number'] . "&nbsp;" . $order . "</div>
        ";
        if ($recap === "-0-0-0-0-0-") {
            $retour .= "<div class='titleFont responseSummary'>" . $this->lang['no_responses'] . "</div>";
        } else {
            $responseList = explode("-", $recap);
            $retour .= "<div class='titleFont responseSummary'>" . $this->lang['responses_summary'];
            $retour .= "<ul>";
            foreach ($responseList as $response) {
                if ($response !== "0" && $response !== "") {
                    $retour .= "<li>$response</li>";
                }
            }
            $retour .= "</ul></div>";
        }
        echo $retour;
    }

    public function nextQuestion($waiting)
    {
        $content = $waiting ? '<i class="fas fa-redo-alt"></i>' : '>>';
        $retour = "
            <form method='post' action='index.php?page=participate'>";

        $retour .= "<input type='hidden' value='" . $_GET['idSession'] . "' name='idSession'>
        <input type='hidden' value='" . $_GET['username'] . "' name='username'>
                <button class='propoStyle btn-confirm' type='submit' value= '>>' id='next'>" . $content . "</button>
                </form>";

        echo $retour;
    }

    public function display($message)
    {
        $class = $message === $this->lang['wait'] ? 'waiting' : 'responseSummary';
        echo '<div class="titleFont ' . $class . '">' . $message . '</div>';
    }

    public function endSession()
    {
        $retour = "
        <form method='post' action='index.php?page=success&state=end'>
        <input class='propoStyle btn-confirm' type='submit' value=" . $this->lang['end'] . " id='end'>
        </form>
        ";

        echo $retour;
    }

    public function inResponse($session, $username)
    {
        $retour = '<form action="index.php?page=participate" method="POST">
        <input type="hidden" name="username" value="' . $username . '">
        <input type="hidden" name="order" value="' . $session->getCurrent() . '">
        <input type="hidden" name="idSession" value="' . $session->getId() . '">
        <input class="propoStyle fixed-bottom btn-confirm" type="submit" value=">>" id="">
        </form>';
        echo $retour;
    }

}