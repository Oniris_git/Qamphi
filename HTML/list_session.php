<?php
/**
 * Created by PhpStorm.
 * User: remi.andre
 * Date: 12/03/2019
 * Time: 16:59
 */

namespace App\HTML;


class list_session extends html
{


    public function editTitle($from){
        $result = '<div class="row">
                        <div class="col-2 col-sm-2 col-md-2 col-xl-1 homeIcon">';
        if ($from !="") {
            $result .=          '<a href="'.$from.'">
                                    <i class="fas fa-arrow-left leftArrow"></i>
                                </a>';
        }
        $result .=     '</div>
                        <div class="col-10 col-sm-10 d-flex">
                            <h1 id="legalSize">
                                <i class="fas fa-edit colorEditFa"></i> '.$this->lang['edit_session'].'
                            </h1>
                            <form method="post" action="index.php?page=edit_session">
                                <input type="hidden" name="newSession">
                                <button class="unstyleBtn">
                                    <i class="fas fa-plus-circle hvr-pulse" style="margin: 0 0 0 2rem; transform: scale(1.3)"></i>
                                </button>
                            </form>
                            <button title="'.$this->lang['presentation_options'].'" type="button" class="unstyleBtn colorArrow" data-toggle="modal" data-target="#sessionsfilters" style="margin: 1.5% 0 0 1.5%; transform: scale(1.5)">
                                <i class="fas fa-wrench"></i>
                            </button>
                        </div>
                    </div> 
                    <hr>';

        echo $result;
    }

    public function filtersModal($filters) {

        $public_check = in_array('public', $filters['visibility']) ? ' checked' : '';
        $private_check = in_array('private', $filters['visibility']) ? ' checked' : '';
        $mine_check = in_array('mine', $filters['owner']) ? ' checked' : '';
        $shared_check = in_array('shared', $filters['owner']) ? ' checked' : '';


        $result = '<div class="modal fade" id="sessionsfilters" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fas fa-wrench"></i>&nbsp;&nbsp;'.$this->lang['presentation_options'].'</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="preferences_form">
                <div class="installContainer">
                    <h4>'.$this->lang['filters'].'&nbsp;:</h4>
                    <br>
                    <div class="row">
                        <div class="col">
                            <p class="sessions-list-option">&nbsp;&nbsp;&nbsp;'.$this->lang['visibility'].'&nbsp;:&nbsp;</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="public" id="public"'.$public_check.'>
                                <label class="form-check-label" for="public">'.$this->lang['public_access'].'</label>
                            </div>
                              
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="private" id="private"'.$private_check.'>
                                <label class="form-check-label" for="private">'.$this->lang['private_access'].'</label>
                            </div>
                        </div>
                        
                        <div class="col">
                            <p class="sessions-list-option">&nbsp;&nbsp;&nbsp;'.$this->lang['session_owner'].'&nbsp;:&nbsp;</p>
                             
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="mine" id="mine"'.$mine_check.'>
                                <label class="form-check-label" for="mine">'.$this->lang['my_sessions'].'</label>
                            </div>
                            
                            <div class="form-check ">
                                <input class="form-check-input" type="checkbox" name="shared" id="shared"'.$shared_check.'>
                                <label class="form-check-label" for="shared">'.$this->lang['shared_sessions'].'</label>
                            </div>
                       </div>
                   </div>
                </div>
                
                <div class="installContainer">
                    <h4>'.$this->lang['display'].'&nbsp;:</h4>
                    <br>
                    <div class="form-inline">
                        &nbsp;&nbsp;&nbsp;<input class="form-control" type="number" min="1" max="100" name="byPage" value="'.$filters['by_page'].'">&nbsp;&nbsp;
                        <label for="byPage">'.$this->lang['results_number'].'</label>
                    </div>
                </div>
                                
                <div class="row justify-content-center">
                    <div class="col-4">
                        <button data-dismiss="modal" id="optionsBtn" type="button" name="action" value="options" class="btn btn-outline-success width100" onclick="savePreferences()">
                            '.$this->lang['record'].'
                        </button>
                    </div>
                </div>        
                           
                </form> 
              </div>
            </div>
          </div>
        </div>';

        echo $result;
    }

    public function sessionsSwitch($shared = false) {
        $mine = $shared === false ? ' active-btn' : '';
        $shared = $shared === true ? ' active-btn' : '';
        $retour = '<div class="row justify-content-center">            
                <a class="session-link" href="index.php?page=list_session">
                    <button type="button" class="btn btn-light session-btn'.$mine.'"><span>'.$this->lang['my_sessions'].'</span></button>
                </a>
                <a class="session-link" href="index.php?page=list_session&shared">
                    <button type="button" class="btn btn-light session-btn'.$shared.'"><span>'.$this->lang['shared_sessions'].'</span></button>
                </a>
        </div>';

        echo $retour;
    }

    //display session edition
    public function sessionList($sessionByUser, $count, $current_page, $total_pages, $authorisations, $connected, $sort, $filters)
    {
        $retour = "";
        $shared = '';
        $i = 1;

        $sort_icon = $sort['order'] == 'asc' ? 'fa-caret-up' : 'fa-caret-down';


        $dateselected = $sort['field'] == 'id' ? '<i class="fas '.$sort_icon.'" style="color:lightgray;"></i>' : '';
        $titleselected = $sort['field'] == 'title' ? '<i class="fas '.$sort_icon.'" style="color:lightgray;"></i>' : '';

        $inverse_order = $sort['order'] == 'asc' ? 'desc' : 'asc';

        $idorder = $sort['field'] == 'id' ? $inverse_order : 'asc';
        $titleorder = $sort['field'] == 'title' ? $inverse_order : 'asc';

        $retour .= '<div id="displayDeviceSize" style="visibility:collapse;font-size:0.5em;text-align:center">The device is too small to display content</div>';
        if(sizeof($sessionByUser) > 0){
            $retour .= '<div class="marginTop">
                                <th title="'.$this->lang['sort_date'].$this->lang['sort_'.$idorder].'">
                                    <a href="?page=list_session&spage='.$current_page.'&sort=id&order='.$idorder.'" class="sort-link">
                                        #&nbsp;'.$dateselected.'
                                    </a>
                                </th>
    							<th title="'.$this->lang['sort_title'].$this->lang['sort_'.$titleorder].'">
    							    <a href="?page=list_session&spage='.$current_page.'&sort=title&order='.$titleorder.'" class="sort-link">
                                        '.$this->lang['name'].'&nbsp;'.$titleselected.'
    							    </a>
                                </th>
    							<th>'.$this->lang['activate'].'</th>
    							<th>'.$this->lang['drain'].'</th>
    							<th>'.$this->lang['delete'].'</th>
    							<th>'.$this->lang['answer_count'].'</th>
                                <th>'.$this->lang['use'].'</th>
    						</div>';

            foreach ($sessionByUser as $session){
                $link = $authorisations[$session->getId()]['rights'] === 'write' ? 'href="index.php?page=edit_session&id='.$session->getId().'"' : '';
                $disabled = $authorisations[$session->getId()]['rights'] === 'write' ? '' : ' disabled title="'.$this->lang['session_cannot_write'].'"';
                $icon = $session->getAccess() == 'private' ? '&nbsp;&nbsp;<i class="fas fa-key" title="'.$this->lang['private_session'].'" style="color: #8a8a8abd;"></i>' : '';
                if ($session->getUserId() !== $connected) {
                    $icon .= '&nbsp;&nbsp;<i class="fas fa-share-alt grey_btn" 
                                    title="'.$this->lang['session_owner'].'&nbsp;:&nbsp;'.$authorisations[$session->getId()]['owner'].'">                                
                                </i>';
                }
                $retour .='	<tbody>
								<tr>
								    <td class="text-center col-3 col-lg-2 col-md-2 col-xl-2">'.(($current_page-1)*$filters['by_page']+$i).'</td>
									<td>
									    <a '.$link.'class="link" style="color:#116699">';
                                        if(strlen($session->getTitle()) > 40){
                                            $name = substr($session->getTitle(),0, 40).'...';
                                        }else{
                                            $name= $session->getTitle();
                                        }
                                $retour .= $name.'</a>'.$icon.'
									</td>';

                $retour.= $this->button_active_session($session->getActive(), $session->getId(), $disabled, $shared).
                            $this->button_delete_question($session->getId(), $disabled, $shared).
                            $this->button_delete_session($session->getId(), $disabled, $shared).'
                            <td class="center">
                                <form action="index.php?page=launch_session" method="post">
                                    <input type="hidden" name="idSession" value="'.$session->getId().'">
                                    <input type="hidden" name="request" value="stats">
                                    <button type="submit" class="link unstyleBtn" style="color: #116699">
                                        '.$session->getParticipants().'
                                    </button>
                                </form>
                            </td>';

                        if ($count[$session->getId()] != 0) {
                            $retour .= $this->button_play_session($session->getId(), $this->lang);
                        } else {
                            $retour .= $this->cannot_play();
                        }

                        $retour .= '</tr>';
                $i++;
            }
            $back = $current_page == 1 ? 'pagination-disabled' : 'pagination';
            $previous = $current_page == 1 ? 1 : $current_page-1;

            $next = $current_page == $total_pages ? 'pagination-disabled' : 'pagination';
            $nextP = $current_page == $total_pages ? $total_pages : $current_page+1;

            $retour .='	</tbody>
	                        </table>
                                </div>';
            if ($total_pages > 1) {
                $retour .= '<!-- Pagination -->
                                        <div class="row justify-content-center align-items-center">';
                if ($current_page != 1) {
                    $retour .= '<a href="index.php?page=list_session&spage='.$previous.$shared.'&sort='.$sort['field'].'">
                                                <i class="fas fa-caret-left '.$back.' leftArrow"></i>
                                            </a>';
                }
                for ($i = 1; $i <= $total_pages; $i++) {
                    $current = $i == $current_page ? 'pagination-current' : '';
                    $retour .= '<a href="index.php?page=list_session&spage='.$i.$shared.'&sort='.$sort['field'].'" class="'.$current.' pagination pagination-nmbr">&nbsp&nbsp'.$i.'</a>';
                }

                if ($current_page != $total_pages) {
                    $retour .= '<a href="index.php?page=list_session&spage='.$nextP.$shared.'&sort='.$sort['field'].'">
                                <i class="fas fa-caret-right leftArrow '.$next.'"></i>
                            </a>';

                }
                       $retour .= '</div>';
            }
            echo $retour;
        }else{
            echo "    <div class='row justify-content-center'>
							<div class='col-10 col-sm-4 center'>
								".$this->lang['no_session']."
							</div>
						</div>
					</table>";
        }
        echo '<br>';
    }

    public function button_active_session($active, $s_id, $disabled, $shared)
    {
        if($active == 1){
            //$icon = $disabled === '' ? '<i class="fas fa-toggle-on"></i>' : '<i class="fas fa-ban"></i>';
            $retour = '	<td>
							<div id="icon-size">
							    <form method="post" action="index.php?page=list_session'.$shared.'">
							        <input type="hidden" name="idSwitchOff" value="'.$s_id.'">
							        <button name="action" value="switch-off" class="unstyleBtn active">
							            <i class="fas fa-toggle-on"></i>
							        </button>
                                </form>
							</div>
						</td>';
            return $retour;
        }else if($active == 0){
            $icon = $disabled === '' ? '<i class="fas fa-toggle-off"></i>' : '<i class="fas fa-ban"></i>';
            $retour = '	<td>
							<div id="icon-size" class="activeOff">
								<form method="post" action="index.php?page=list_session'.$shared.'">
							        <input type="hidden" name="idSwitchOn" value="'.$s_id.'">
							        <button type="submit" name="action" value="switch-on" class="unstyleBtn activeOff">
							           <i class="fas fa-toggle-off"></i>
							        </button>
                                </form>
							</div>
						</td>';
            return $retour;
        }
    }

    public function button_delete_session($s_id, $disabled, $shared)
    {
        $icon = $disabled === '' ? '<i class="fas fa-trash-alt"></i>' : '<i class="fas fa-ban"></i>';
        $retour = '	<td>
						<div id="icon-size">
						    <form method="post" action="index.php?page=list_session'.$shared.'">
							    <input type="hidden" name="deleteSession" value="'.$s_id.'">
							    <button type="submit" '.$disabled.' name="action" value="deleteSession" class="unstyleBtn trash" onclick="return confirm('."'".$this->lang["delete_session_confirm"]."'".');">
							        '.$icon.'
							    </button>
                            </form>
						</div>
					</td>';
        return  $retour;
    }

    public function button_play_session($s_id)
    {
        $retour = ' <td>
                        <div id="icon-size">
                            <form method="post" action="index.php?page=sessions">
                                <input type="hidden" name="idSession" value="'.$s_id.'">
                                <input type="hidden" name="order" value="1">
                                <button type="submit" class="unstyleBtn play">
                                    <i class="fas fa-play"></i>
                                </button>
                            </form>
                        </div>
                    </td>';
        return  $retour;
    }

    //if session has no question
    public function cannot_play() {
        $retour = ' <td>
        <div id="icon-size">
                <button type="submit" class="unstyleBtn disabledButton" disabled title="'.$this->lang['no_questions'].'">
                    <i class="fas fa-ban"></i>
                </button>
        </div>
    </td>';
return  $retour;
    }

    public function button_delete_question($s_id, $disabled, $shared)
    {
        $icon = $disabled === '' ? '<i class="fas fa-sync-alt"></i>' : '<i class="fas fa-ban"></i>';
        $retour = '	<td>
						<div id="icon-size">
						    <form method="post" action="index.php?page=list_session'.$shared.'">
							    <input type="hidden" name="deleteResponse" value="'.$s_id.'">
							    <button type="submit" name="action" value="deleteResponse" class="unstyleBtn recycle" onclick="return confirm('."'".$this->lang["delete_question_confirm"]."'".');">
							        <i class="fas fa-sync-alt"></i>
							    </button>
                            </form>
						</div>
					</td>';
        return $retour;
    }

}
